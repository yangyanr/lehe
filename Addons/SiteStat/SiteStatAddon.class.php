<?php
// +----------------------------------------------------------------------
// | 乐呵网 [ 有节骨乃坚，无心品自端 ]
// +----------------------------------------------------------------------
// | 独在异乡为异客， 每逢佳节倍思亲。
// +----------------------------------------------------------------------
// | Author: 二　阳°<707069100@qq.com>   <http://weibo.com/513778937>
// +----------------------------------------------------------------------


// ------------------------------------------------------------------------
namespace Addons\SiteStat;
use Common\Controller\Addon;
// ------------------------------------------------------------------------

/**
 * 系统环境信息插件
 * @category addon
 */
class SiteStatAddon extends Addon{

    public $info = array(
        'name'=>'SiteStat',
        'title'=>'站点统计信息',
        'description'=>'统计站点的基础信息',
        'status'=>1,
        'author'=>'thinkphp',
        'version'=>'0.1'
    );


// ------------------------------------------------------------------------

    public function install(){
        return true;
    }


// ------------------------------------------------------------------------

    public function uninstall(){
        return true;
    }

// ------------------------------------------------------------------------

    //实现的AdminIndex钩子方法
    public function AdminIndex($param){
        $config = $this->getConfig();
        $this->assign('addons_config', $config);
        if($config['display']){
            $info['user']		=	M('Member')->count();
            $info['action']		=	M('ActionLog')->count();
            $info['category']	=	M('Category')->count();
            $info['document']	=	M('Document')->count();
            $this->assign('info',$info);
            $this->display('info');
        }
    }

// ------------------------------------------------------------------------

}


// ------------------------------------------------------------------------

// End SiteStatAddon Class
/* End of file SiteStatAddon.class.php */
/* Location: ./Addons/SiteStat/SiteStatAddon.class.php */