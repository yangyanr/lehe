<?php
// +----------------------------------------------------------------------
// | 乐呵网 [ 有节骨乃坚，无心品自端 ]
// +----------------------------------------------------------------------
// | 独在异乡为异客， 每逢佳节倍思亲。
// +----------------------------------------------------------------------
// | Author: 二　阳°<707069100@qq.com>   <http://weibo.com/513778937>
// +----------------------------------------------------------------------

// ------------------------------------------------------------------------
namespace Addons\DevTeam;
use Common\Controller\Addon;
// ------------------------------------------------------------------------

/**
 * 开发团队信息插件
 * @category addon
 */

class DevTeamAddon extends Addon{

    public $info = array(
        'name'=>'DevTeam',
        'title'=>'开发团队信息',
        'description'=>'开发团队成员信息',
        'status'=>1,
        'author'=>'thinkphp',
        'version'=>'0.1'
    );

    // ------------------------------------------------------------------------
    public function install(){
        return true;
    }

    // ------------------------------------------------------------------------

    public function uninstall(){
        return true;
    }

    // ------------------------------------------------------------------------
    //实现的AdminIndex钩子方法
    public function AdminIndex($param){
        $config = $this->getConfig();
        $this->assign('addons_config', $config);
        if($config['display'])
            $this->display('widget');
    }
    // ------------------------------------------------------------------------
}


// ------------------------------------------------------------------------

// End DevTeamAddon Class
/* End of file DevTeamAddon.class.php */
/* Location: ./Addons/SystemInfo/DevTeamAddon.class.php */