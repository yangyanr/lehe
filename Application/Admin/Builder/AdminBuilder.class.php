<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

namespace Admin\Builder;

use Admin\AdminController;

// ---------------------------------------------------------------------------------------------------------------------


/**
 * AdminBuilder：快速建立管理页面
 *
 * 为什么要继承AdminController
 * 因为 AdminController 的初始化函数中读取了顶部导航栏和左侧的菜单
 * 如果不继承的话，只能复制 AdminController 中的代码来读取导航栏和左侧的菜单
 * 这样做会导致一个问题就是当 AdminController 被官方修改后 AdminBuilder 不会同步更新，从而导致错误
 *
 */
abstract class   AdminBuilder extends AdminController
{

    /**
     * 模板
     * @param string $templateFile
     * @param string $charset
     * @param string $contentType
     * @param string $content
     * @param string $prefix
     */
    public function display($templateFile = '', $charset = '', $contentType = '', $content = '', $prefix = '')
    {
        //获取模版的名称
        $template = dirname(__FILE__) . '/../View/default/Builder/' . $templateFile . '.html';

        //显示页面
        parent::display($template);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 生成html
     * @param $attr
     * @return array|string
     */
    protected function compileHtmlAttr($attr)
    {
        $result = array();
        foreach ($attr as $key => $value) {
            $value = htmlspecialchars($value);
            $result[] = "$key=\"$value\"";
        }
        $result = implode(' ', $result);
        return $result;
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End AdminBuilder Class

/* End of file AdminBuilder.class.php */
/* Location: ./Application/Admin/Builder/AdminBuilder.class.php */
