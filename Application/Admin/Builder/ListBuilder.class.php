<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

namespace Admin\Builder;

// ---------------------------------------------------------------------------------------------------------------------


/**
 * list表格页面
 *
 */
class ListBuilder extends AdminBuilder
{

    //标题
    private $_title;

    //页面标题边上的提示信息
    private $_suggest;

    //筛选功能
    private $_select = array();

    //筛选下拉选择url
    private $_selectPostUrl;

    //搜索
    private $_search = array();

    //设置搜索提交表单的URL
    private $_searchPostUrl;

    //data数据
    private $_data = array();

    //已被U函数解析的地址
    private $_setStatusUrl;

    //设置回收站根据ids彻底删除的URL
    private $_setDeleteTrueUrl;

    //分页
    private $_pagination = array();

    //表格列
    private $_keyList = array();

    //按钮
    private $_buttonList = array();

    /**
     * 设置页面标题
     * @param $title 标题文本
     * @return $this
     */
    public function title($title)
    {
        $this->_title = $title;
        $this->meta_title = $title;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * suggest  页面标题边上的提示信息
     * @param $suggest
     * @return $this
     */
    public function suggest($suggest)
    {
        $this->_suggest = $suggest;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 添加筛选功能
     * @param string $title 标题
     * @param string $name 键名
     * @param string $type 类型，默认文本
     * @param string $des 描述
     * @param        $attr  标签文本
     * @param string $arrDb 筛选项数据来源
     * @param null $arrValue 筛选数据
     * （包含ID 和 value 的数组:array(array('id'=>1,'value'=>'系统'),array('id'=>2,'value'=>'项目'),
     *  array('id'=>3,'value'=>'机构'));）
     * @return $this
     */
    public function select($title = '筛选', $name = 'key', $type = 'select', $des = '', $attr, $arrDb = '', $arrValue = null)
    {
        if (empty($arrDb)) {
            $this->_select[] = array('title' => $title, 'name' => $name, 'type' => $type, 'des' => $des,
                'attr' => $attr, '$arrValue' => $arrValue);
        } else {
            //TODO:待完善 如果$arrDb存在的就把当前数据表的$name字段的信息全部查询出来供筛选
        }
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 筛选下拉选择url
     * @param $url string 已被U函数解析的地址
     * @return $this
     */
    public function setSelectPostUrl($url)
    {
        $this->_selectPostUrl = $url;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

//    /**
//     * 搜索
//     * @param string $title 标题
//     * @param string $name 键名
//     * @param string $type 类型，默认文本
//     * @param string $des 描述
//     * @param        $attr 标签文本
//     * @return $this
//     */
//    public function search($title = '搜索', $name = 'key', $type = 'text', $des = '', $attr)
//    {
//        $this->_search[] = array('title' => $title, 'name' => $name, 'type' => $type, 'des' => $des, 'attr' => $attr);
//        return $this;
//    }
    /**
     * 更新筛选搜索功能 ，修正连续提交多出N+个GET参数的BUG
     * @param string $title 标题
     * @param string $name 键名
     * @param string $type 类型，默认文本
     * @param string $des 描述
     * @param        $attr  标签文本
     * @param string $arrDb 择筛选项数据来源
     * @param string $arrValue 筛选数据
     * （包含ID 和value的数组:array(array('id'=>1,'value'=>'系统'),array('id'=>2,'value'=>'项目'),array('id'=>3,'value'=>'机构'));）
     * @return $this
     */
    public function search($title = '搜索', $name = 'key', $type = 'text', $des = '', $attr, $arrDb = '', $arrValue = null)
    {

        if (empty($type) && $type = 'text') {
            $this->_search[] = array('title' => $title, 'name' => $name, 'type' => $type, 'des' => $des, 'attr' => $attr);
//            $this->setSearchPostUrl('');
        } else {
            if (empty($arrDb)) {
//                $this->_search[] = array('title' => $title, 'name' => $name, 'type' => $type, 'des' => $des, 'attr' => $attr,
//                    'field' => $field, 'table' => $table, '$arrValue' => $arrValue);
                $this->_search[] = array('title' => $title, 'name' => $name, 'type' => $type, 'des' => $des,
                    'attr' => $attr, '$arrValue' => $arrValue);
                $this->setSearchPostUrl('');
            } else {
                //TODO:待完善 如果 $arrDb 存在的就把当前数据表的 $name 字段的信息全部查询出来供筛选
            }
        }
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------


//    /**
//     * 设置搜索提交表单的URL
//     * @param $url
//     * @return $this
//     */
//    public function setSearchPostUrl($url)
//    {
//        $this->_searchPostUrl = $url;
//        return $this;
//    }
    /**
     * 更新筛选搜索功能 ，修正连续提交多出N+个GET参数的BUG
     * @param $url   提交的getURL
     * @return $this
     */
    public function setSearchPostUrl($url)
    {
        $this->_searchPostUrl = $url;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     *列表数据
     * @param $list
     * @return $this
     */
    public function data($list)
    {
        $this->_data = $list;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @param $url string 已被U函数解析的地址
     * @return $this
     */
    public function setStatusUrl($url)
    {
        $this->_setStatusUrl = $url;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 设置回收站根据ids彻底删除的URL
     * @param $url
     * @return $this
     */
    public function setDeleteTrueUrl($url)
    {
        $this->_setDeleteTrueUrl = $url;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 分页
     * 不要给listRows默认值，因为开发人员很可能忘记填写listRows导致翻页不正确
     * @param $totalCount
     * @param $listRows
     * @return $this
     */
    public function pagination($totalCount, $listRows)
    {
        $this->_pagination = array('totalCount' => $totalCount, 'listRows' => $listRows);
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 加入一个按钮
     * @param $title
     * @param $attr
     * @return $this
     */
    public function button($title, $attr)
    {
        $this->_buttonList[] = array('title' => $title, 'attr' => $attr);
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     *设置表格列
     * @param $name
     * @param $title
     * @param $type
     * @param null $opt
     * @return $this
     */
    public function key($name, $title, $type, $opt = null)
    {
        $key = array('name' => $name, 'title' => $title, 'type' => $type, 'opt' => $opt, 'width' => $width);
        $this->_keyList[] = $key;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 加入新增按钮
     * @param        $href
     * @param string $title
     * @param array $attr
     * @return ListBuilder
     */
    public function buttonNew($href, $title = '新增', $attr = array())
    {
        $attr['href'] = $href;
        return $this->button($title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * ajax 请求按钮
     * @param $url
     * @param $params
     * @param $title
     * @param array $attr
     * @return ListBuilder
     */
    public function ajaxButton($url, $params, $title, $attr = array())
    {
        $attr['class'] = 'btn ajax-post';
        $attr['url'] = $this->addUrlParam($url, $params);
        $attr['target-form'] = 'ids';
        return $this->button($title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 加入模态弹窗按钮
     * @param $url
     * @param $params
     * @param $title
     * @param array $attr
     * @return $this
     */
    public function buttonModalPopup($url, $params, $title, $attr = array())
    {
        //$attr中可选参数，data-title：模态框标题，target-form：要传输的数据
        $attr['modal-url'] = $this->addUrlParam($url, $params);
        $attr['data-role'] = 'modal_popup';
        return $this->button($title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 设置状态按钮
     * @param $url
     * @param $status
     * @param $title
     * @param $attr
     * @return ListBuilder
     */
    public function buttonSetStatus($url, $status, $title, $attr)
    {
        $attr['class'] = 'btn ajax-post';
        $attr['url'] = $this->addUrlParam($url, array('status' => $status));
        $attr['target-form'] = 'ids';
        return $this->button($title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 禁用按钮
     * @param null $url
     * @param string $title
     * @param array $attr
     * @return ListBuilder
     */
    public function buttonDisable($url = null, $title = '禁用', $attr = array())
    {
        if (!$url) $url = $this->_setStatusUrl;
        return $this->buttonSetStatus($url, 0, $title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 启用按钮
     * @param null $url
     * @param string $title
     * @param array $attr
     * @return ListBuilder
     */
    public function buttonEnable($url = null, $title = '启用', $attr = array())
    {
        if (!$url) $url = $this->_setStatusUrl;
        return $this->buttonSetStatus($url, 1, $title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 删除到回收站
     * @param null $url
     * @param string $title
     * @param array $attr
     * @return \Admin\Builder\ListBuilder
     */
    public function buttonDelete($url = null, $title = '删除', $attr = array())
    {
        if (!$url) $url = $this->_setStatusUrl;
        return $this->buttonSetStatus($url, -1, $title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 还原按钮
     * @param null $url
     * @param string $title
     * @param array $attr
     * @return ListBuilder
     */
    public function buttonRestore($url = null, $title = '还原', $attr = array())
    {
        if (!$url) $url = $this->_setStatusUrl;
        return $this->buttonSetStatus($url, 1, $title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 清空回收站
     * @param null $model
     * @return $this
     */
    public function buttonClear($model = null)
    {
        return $this->button(L('_CLEAR_OUT_'), array('class' => 'btn ajax-post tox-confirm',
            'data-confirm' => L('_CONFIRM_CLEAR_OUT_'), 'url' => U('', array('model' => $model)),
            'target-form' => 'ids', 'hide-data' => 'true'));
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 彻底删除
     * @param null $url
     * @return $this
     */
    public function buttonDeleteTrue($url = null)
    {
        if (!$url) $url = $this->_setDeleteTrueUrl;
        $attr['class'] = 'btn ajax-post tox-confirm';
        $attr['data-confirm'] = L('_CONFIRM_DELETE_COMPLETELY_');
        $attr['url'] = $url;
        $attr['target-form'] = 'ids';
        return $this->button(L('_DELETE_COMPLETELY_'), $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 排序按钮
     * @param $href
     * @param string $title
     * @param array $attr
     * @return ListBuilder
     */
    public function buttonSort($href, $title = '排序', $attr = array())
    {
        $attr['href'] = $href;
        return $this->button($title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 显示纯文本
     * @param $name 键名
     * @param $title 标题
     * @return ListBuilder
     */
    public function keyText($name, $title)
    {
        return $this->key($name, text($title), 'text');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 显示html
     * @param $name 键名
     * @param $title 标题
     * @return ListBuilder
     */
    public function keyHtml($name, $title)
    {
        return $this->key($name, op_h($title), 'html');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     *map
     * @param $name
     * @param $title
     * @param $map
     * @return ListBuilder
     */
    public function keyMap($name, $title, $map)
    {
        return $this->key($name, $title, 'map', $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * id
     * @param string $name
     * @param string $title
     * @return ListBuilder
     */
    public function keyId($name = 'id', $title = 'ID')
    {
        return $this->keyText($name, $title);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 图标展示
     * @param string $name
     * @param string $title
     * @return $this
     */
    public function keyIcon($name = 'icon', $title = '图标')
    {
        return $this->key($name, $title, 'icon');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 链接
     * 可以是函数或U函数解析的字符串
     * 如果是字符串，该函数将附带一个id参数
     * @param $name
     * @param $title
     * @param $getUrl Closure|string
     * @return $this
     */
    public function keyLink($name, $title, $getUrl)
    {
        //如果getUrl是一个字符串，则表示getUrl是一个U函数解析的字符串
        if (is_string($getUrl)) {
            $getUrl = $this->createDefaultGetUrlFunction($getUrl);
        }

        //修整添加多个空字段时显示不正常的BUG
        if (empty($name)) {
            $name = $title;
        }

        //添加key
        return $this->key($name, $title, 'link', $getUrl);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 状态
     * @param string $name
     * @param string $title
     * @return ListBuilder
     */
    public function keyStatus($name = 'status', $title = '状态')
    {
        $map = array(-1 => L('_DELETE_'), 0 => L('_DISABLE_'), 1 => L('_ENABLE_'), 2 => L('_UNAUDITED_'));
        return $this->key($name, $title, 'status', $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 开关
     * @param $name
     * @param $title
     * @return ListBuilder
     */
    public function keyYesNo($name, $title)
    {
        $map = array(0 => L('_NOT_'), 1 => L('YES'));
        return $this->keymap($name, $title, $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     *开关
     * @param $name
     * @param $title
     * @return ListBuilder
     */
    public function keyBool($name, $title)
    {
        return $this->keyYesNo($name, $title);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 图片
     * @param $name
     * @param $title
     * @return ListBuilder
     */
    public function keyImage($name, $title)
    {
        return $this->key($name, $title, 'image');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 时间
     * @param $name
     * @param $title
     * @return ListBuilder
     */
    public function keyTime($name, $title)
    {
        return $this->key($name, $title, 'time');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 创建时间
     * @param string $name
     * @param string $title
     * @return ListBuilder
     */
    public function keyCreateTime($name = 'create_time', $title = '创建时间')
    {
        return $this->keyTime($name, $title);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 更新时间
     * @param string $name
     * @param string $title
     * @return ListBuilder
     */
    public function keyUpdateTime($name = 'update_time', $title = '更新时间')
    {
        return $this->keyTime($name, $title);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 用户id
     * @param string $name
     * @param string $title
     * @return ListBuilder
     */
    public function keyUid($name = 'uid', $title = '用户')
    {
        return $this->key($name, $title, 'uid');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 昵称
     * @param string $name
     * @param $title
     * @param null $subtitle
     * @return ListBuilder
     */
    public function keyNickname($name = 'uid', $title, $subtitle = null)
    {
        return $this->key($name, $title, $subtitle, 'nickname');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 标题
     * @param string $name
     * @param string $title
     * @return ListBuilder
     */
    public function keyTitle($name = 'title', $title = '标题')
    {
        return $this->keyText($name, $title);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 关联表字段显示+URL连接
     * @param $name
     * @param $title
     * @param $mate
     * @param $return
     * @param $model
     * @param string $url
     * @return ListBuilder
     */
    public function keyJoin($name, $title, $mate, $return, $model, $url = '')
    {
        $map = array('mate' => $mate, 'return' => $return, 'model' => $model, 'url' => $url);
        return $this->key($name, $title, 'join', $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 模态弹窗
     * @param $getUrl
     * @param $text
     * @param $title
     * @param array $attr
     * @return $this
     */
    public function keyDoActionModalPopup($getUrl, $text, $title, $attr = array())
    {
        //attr中需要设置data-title，用于设置模态弹窗标题
        $attr['data-role'] = 'modal_popup';
        //获取默认getUrl函数
        if (is_string($getUrl)) {
            $getUrl = $this->createDefaultGetUrlFunction($getUrl);
        }
        //确认已经创建了DOACTIONS字段
        $doActionKey = null;
        foreach ($this->_keyList as $key) {
            if ($key['name'] === 'DOACTIONS') {
                $doActionKey = $key;
                break;
            }
        }
        if (!$doActionKey) {
            $this->key('DOACTIONS', $title, 'doaction', $attr);
        }

        //找出第一个DOACTIONS字段
        $doActionKey = null;
        foreach ($this->_keyList as &$key) {
            if ($key['name'] == 'DOACTIONS') {
                $doActionKey = &$key;
                break;
            }
        }

        //在DOACTIONS中增加action
        $doActionKey['opt']['actions'][] = array('text' => $text, 'get_url' => $getUrl, 'opt' => $attr);
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 操作
     * @param $getUrl
     * @param $text
     * @param string $title
     * @return $this
     */
    public function keyDoAction($getUrl, $text, $title = '操作')
    {
        //获取默认getUrl函数
        if (is_string($getUrl)) {
            $getUrl = $this->createDefaultGetUrlFunction($getUrl);
        }

        //确认已经创建了DOACTIONS字段
        $doActionKey = null;
        foreach ($this->_keyList as $key) {
            if ($key['name'] === 'DOACTIONS') {
                $doActionKey = $key;
                break;
            }
        }
        if (!$doActionKey) {
            $this->key('DOACTIONS', $title, 'doaction', array());
        }

        //找出第一个DOACTIONS字段
        $doActionKey = null;
        foreach ($this->_keyList as &$key) {
            if ($key['name'] == 'DOACTIONS') {
                $doActionKey = &$key;
                break;
            }
        }

        //在DOACTIONS中增加action
        $doActionKey['opt']['actions'][] = array('text' => $text, 'get_url' => $getUrl);
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 编辑
     * @param $getUrl
     * @param string $text
     * @return ListBuilder
     */
    public function keyDoActionEdit($getUrl, $text = '编辑')
    {
        return $this->keyDoAction($getUrl, $text);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 还原
     * @param string $text
     * @return ListBuilder
     */
    public function keyDoActionRestore($text = '还原')
    {
        $that = $this;
        $setStatusUrl = $this->_setStatusUrl;
        $getUrl = function () use ($that, $setStatusUrl) {
            return $that->addUrlParam($setStatusUrl, array('status' => 1));
        };
        return $this->keyDoAction($getUrl, $text, array('class' => 'ajax-get'));
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     *树
     * @param $name
     * @param $title
     * @param $length
     * @return ListBuilder
     */
    public function keyTruncText($name, $title, $length)
    {
        return $this->key($name, $title, 'trunktext', $length);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 判断是否属于选择返回数据的列表页，如果是在列表页->display('admin_solist');
     * @param string $solist
     */
    public function display($solist = '')
    {
        //key类型的等价转换
        //map转换成text
        $this->convertKey('map', 'text', function ($value, $key) {
            return $key['opt'][$value];
        });

        //uid转换成text
        $this->convertKey('uid', 'text', function ($value) {
            $value = query_user(array('nickname', 'uid', 'space_url'), $value);
            return "<a href='" . $value['space_url'] . "' target='_blank'>[{$value[uid]}]" . $value['nickname'] . '</a>';
        });

        //nickname转换成text
        $this->convertKey('nickname', 'text', function ($value) {
            $value = query_user(array('nickname', 'uid', 'space_url'), $value);
            return "<a href='" . $value['space_url'] . "' target='_blank'>[{$value[uid]}]" . $value['nickname'] . '</a>';
        });

        //time转换成text
        $this->convertKey('time', 'text', function ($value) {
            if ($value != 0) {
                return time_format($value);
            } else {
                return '-';
            }
        });

        //trunctext转换成text
        $this->convertKey('trunktext', 'text', function ($value, $key) {
            $length = $key['opt'];
            return msubstr($value, 0, $length);
        });

        //text转换成html
        $this->convertKey('text', 'html', function ($value) {
            return $value;
        });

        //link转换为html
        $this->convertKey('link', 'html', function ($value, $key, $item) {
            $value = htmlspecialchars($value);
            $getUrl = $key['opt'];
            $url = $getUrl($item);
            //允许字段为空，如果字段名为空将标题名填充到A变现里
            if (!$value) {
                return "<a href=\"$url\" target=\"_blank\">" . $key['title'] . "</a>";
            } else {
                return "<a href=\"$url\" target=\"_blank\">$value</a>";
            }
        });

        //如果icon为空
        $this->convertKey('icon', 'html', function ($value, $key, $item) {
            $value = htmlspecialchars($value);
            if ($value == '') {
                $html = L('_NONE_');
            } else {
                $html = "<i class=\"$value\"></i> $value";
            }
            return $html;
        });

        //image转换为图片
        $this->convertKey('image', 'html', function ($value, $key, $item) {
            if (intval($value)) {//value是图片id
                $value = htmlspecialchars($value);
                $sc_src = get_cover($value, 'path');

                $src = getThumbImageById($value, 80, 80);
                $sc_src = $sc_src == '' ? $src : $sc_src;
                $html = "<div class='popup-gallery'><a title=\"" . L('_VIEW_BIGGER_') . "\" href=\"$sc_src\"><img src=\"$sc_src\"/ style=\"width:80px;height:80px\"></a></div>";
            } else {//value是图片路径
                $sc_src = $value;
                $html = "<div class='popup-gallery'><a title=\"" . L('_VIEW_BIGGER_') . "\" href=\"$sc_src\"><img src=\"$sc_src\"/ style=\"border-radius:100%;\"></a></div>";
            }
            return $html;
        });

        //doaction转换为html
        $this->convertKey('doaction', 'html', function ($value, $key, $item) {
            $actions = $key['opt']['actions'];
            $result = array();
            foreach ($actions as $action) {
                $getUrl = $action['get_url'];
                $linkText = $action['text'];
                $url = $getUrl($item);
                if (isset($action['opt'])) {
                    $content = array();
                    foreach ($action['opt'] as $key => $value) {
                        $value = htmlspecialchars($value);
                        $content[] = "$key=\"$value\"";
                    }
                    $content = implode(' ', $content);
                    if (isset($action['opt']['data-role']) && $action['opt']['data-role'] == "modal_popup") {//模态弹窗
                        $result[] = "<a href=\" javascrapt:void(0);\" modal-url=\"$url\" " . $content . ">$linkText</a>";
                    } else {
                        $result[] = "<a href=\"$url\" " . $content . ">$linkText</a>";
                    }
                } else {
                    $result[] = "<a href=\"$url\">$linkText</a>";
                }
            }
            return implode(' ', $result);
        });

        //join转换为html
        $this->convertKey('Join', 'html', function ($value, $key) {
            if ($value != 0) {
                $val = get_table_field($value, $key['opt']['mate'], $key['opt']['return'], $key['opt']['model']);
                if (!$key['opt']['url']) {
                    return $val;
                } else {
                    $urld = U($key['opt']['url'], array($key['opt']['return'] => $value));
                    return "<a href=\"$urld\">$val</a>";
                }
            } else {
                return '-';
            }
        });

        //status转换为html
        $setStatusUrl = $this->_setStatusUrl;
        $that = &$this;
        $this->convertKey('status', 'html', function ($value, $key, $item) use ($setStatusUrl, $that) {
            //如果没有设置修改状态的URL，则直接返回文字
            $map = $key['opt'];
            $text = $map[$value];
            if (!$setStatusUrl) {
                return $text;
            }

            //返回带链接的文字
            $switchStatus = $value == 1 ? 0 : 1;
            $url = $that->addUrlParam($setStatusUrl, array('status' => $switchStatus, 'ids' => $item['id']));
            return "<a href=\"{$url}\" class=\"ajax-get\">$text</a>";
        });

        //如果html为空
        $this->convertKey('html', 'html', function ($value) {
            if ($value === '') {
                return '<span style="color:#bbb;">' . L('_EMPTY_BRACED_') . '</span>';
            }
            return $value;
        });


        //编译buttonList中的属性
        foreach ($this->_buttonList as &$button) {
            $button['tag'] = isset($button['attr']['href']) ? 'a' : 'button';
            $this->addDefaultCssClass($button);
            $button['attr'] = $this->compileHtmlAttr($button['attr']);
        }

        //生成翻页HTML代码
        C('VAR_PAGE', 'page');
        $pager = new \Think\Page($this->_pagination['totalCount'], $this->_pagination['listRows'], $_REQUEST);
        $pager->setConfig('theme', '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        $paginationHtml = $pager->show();

        //显示页面
        $this->assign('title', $this->_title);
        $this->assign('suggest', $this->_suggest);
        $this->assign('keyList', $this->_keyList);
        $this->assign('buttonList', $this->_buttonList);
        $this->assign('pagination', $paginationHtml);
        $this->assign('list', $this->_data);
        /*加入搜索*/
        $this->assign('searches', $this->_search);
        $this->assign('searchPostUrl', $this->_searchPostUrl);

        /*加入筛选select*/
        $this->assign('selects', $this->_select);
        $this->assign('selectPostUrl', $this->_selectPostUrl);
        //如果是选择返回数据的列表页就调用admin_solist模板文件，否则编译原有模板
        if ($solist) {
            parent::display('admin_solist');
        } else {
            parent::display('admin_list');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 设置状态
     * @param $model
     * @param $ids
     * @param int $status
     */
    public function doSetStatus($model, $ids, $status = 1)
    {
        $id = array_unique((array)$ids);
        $rs = M($model)->where(array('id' => array('in', $id)))->save(array('status' => $status));
        if ($rs === false) {
            $this->error(L('error_setting') . L('_PERIOD_'));
        }
        $this->success(L('success_setting'), $_SERVER['HTTP_REFERER']);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 转换
     * @param $from
     * @param $to
     * @param $convertFunction
     */
    private function convertKey($from, $to, $convertFunction)
    {
        foreach ($this->_keyList as &$key) {
            if ($key['type'] == $from) {
                $key['type'] = $to;
                foreach ($this->_data as &$data) {
                    $value = &$data[$key['name']];
                    $value = $convertFunction($value, $key, $data);
                    unset($value);
                }
                unset($data);
            }
        }
        unset($key);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 添加默认css class
     * @param $button
     */
    private function addDefaultCssClass(&$button)
    {
        if (!isset($button['attr']['class'])) {
            $button['attr']['class'] = 'btn';
        } else {
            $button['attr']['class'] .= ' btn';
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @param $pattern U函数解析的URL字符串，例如 Admin/Test/index?test_id=###
     * Admin/Test/index?test_id={other_id}
     * ###将被id替换
     * {other_id}将被替换
     * @return callable
     */
    private function createDefaultGetUrlFunction($pattern)
    {
        $explode = explode('|', $pattern);
        $pattern = $explode[0];
        $fun = empty($explode[1]) ? 'U' : $explode[1];
        return function ($item) use ($pattern, $fun) {
            $pattern = str_replace('###', $item['id'], $pattern);
            //调用ThinkPHP中的解析引擎解析变量
            $view = new \Think\View();
            $view->assign($item);
            $pattern = $view->fetch('', $pattern);
            return $fun($pattern);
        };
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 添加url
     * @param $url
     * @param $params
     * @return string
     */
    public function addUrlParam($url, $params)
    {
        if (strpos($url, '?') === false) {
            $seperator = '?';
        } else {
            $seperator = '&';
        }
        $params = http_build_query($params);
        return $url . $seperator . $params;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 自动处理清空回收站
     * @param string $model 要清空的模型
     */
    public function clearTrash($model = '')
    {
        if (IS_POST) {
            if ($model != '') {
                $aIds = I('post.ids', array());
                if (!empty($aIds)) {
                    $map['id'] = array('in', $aIds);
                } else {
                    $map['status'] = -1;
                }

                $result = D($model)->where($map)->delete();
                if ($result) {
                    $this->success(L('_SUCCESS_TRASH_CLEARED_', array('result' => $result)));
                }
                $this->error(L('_TRASH_ALREADY_EMPTY_'));
            } else {
                $this->error(L('_TRASH_SELECT_'));
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 执行彻底删除数据，只适用于无关联的数据表
     * @param $model
     * @param $ids
     */
    public function doDeleteTrue($model, $ids)
    {
        $ids = is_array($ids) ? $ids : explode(',', $ids);
        M($model)->where(array('id' => array('in', $ids)))->delete();
        $this->success(L('_SUCCESS_DELETE_COMPLETELY_'), $_SERVER['HTTP_REFERER']);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * keyLinkByFlag  带替换表示的链接
     * @param        $name
     * @param        $title
     * @param        $getUrl
     * @param string $flag
     * @return $this
     */
    public function keyLinkByFlag($name, $title, $getUrl, $flag = 'id')
    {

        //如果getUrl是一个字符串，则表示getUrl是一个U函数解析的字符串
        if (is_string($getUrl)) {
            $getUrl = $this->parseUrl($getUrl, $flag);
        }

        //添加key
        return $this->key($name, $title, 'link', $getUrl);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 解析Url
     * @param $pattern URL文本
     * @param $flag
     * @return callable
     */
    private function parseUrl($pattern, $flag)
    {
        return function ($item) use ($pattern, $flag) {

            $pattern = str_replace('###', $item[$flag], $pattern);
            //调用ThinkPHP中的解析引擎解析变量
            $view = new \Think\View();
            $view->assign($item);
            $pattern = $view->fetch('', $pattern);
            return U($pattern);
        };
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End ListBuilder Class

/* End of file ListBuilder.class.php */
/* Location: ./Application/Admin/Builder/ListBuilder.class.php */
