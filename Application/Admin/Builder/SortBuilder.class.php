<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

namespace Admin\Builder;

// ---------------------------------------------------------------------------------------------------------------------


/**
 * 排序
 * Class SortBuilder
 * @package Admin\Builder
 */
class SortBuilder extends AdminBuilder
{

    //标题
    private $_title;

    //list数据
    private $_list;

    //按钮
    private $_buttonList;

    //提交地址
    private $_savePostUrl;

    /**
     * 标题
     * @param $title
     * @return $this
     */
    public function title($title)
    {
        $this->title = $title;
        $this->meta_title = $title;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 数据
     * @param $list
     * @return $this
     */
    public function data($list)
    {
        $this->_list = $list;
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 按钮
     * @param $title
     * @param array $attr
     * @return $this
     */
    public function button($title, $attr = array())
    {
        $this->_buttonList[] = array('title' => $title, 'attr' => $attr);
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 提交按钮
     * @param $url
     * @param string $title
     * @return SortBuilder
     */
    public function buttonSubmit($url, $title = '确定')
    {
        $this->savePostUrl($url);

        $attr = array();
        $attr['class'] = "sort_confirm btn submit-btn";
        $attr['type'] = 'button';
        $attr['target-form'] = 'form-horizontal';
        return $this->button($title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 返回按钮
     * @param null $url
     * @param string $title
     * @return SortBuilder
     */
    public function buttonBack($url = null, $title = '返回')
    {
        //默认返回当前页面
        if (!$url) {
            $url = $_SERVER['HTTP_REFERER'];
        }

        //添加按钮
        $attr = array();
        $attr['href'] = $url;
        $attr['onclick'] = 'javascript: location.href=$(this).attr("href");';
        $attr['class'] = 'sort_cancel btn btn-return';
        return $this->button($title, $attr);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 提交地址
     * @param $url
     */
    public function savePostUrl($url)
    {
        $this->_savePostUrl = $url;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 显示
     */
    public function display()
    {
        //编译按钮的属性
        foreach ($this->_buttonList as &$e) {
            $e['attr'] = $this->compileHtmlAttr($e['attr']);
        }
        unset($e);

        //显示页面
        $this->assign('title', $this->_title);
        $this->assign('list', $this->_list);
        $this->assign('buttonList', $this->_buttonList);
        $this->assign('savePostUrl', $this->_savePostUrl);
        parent::display('admin_sort');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 处理排序
     * @param $table
     * @param $ids
     */
    public function doSort($table, $ids)
    {
        $ids = explode(',', $ids);
        $res = 0;
        foreach ($ids as $key => $value) {
            $res += M($table)->where(array('id' => $value))->setField('sort', $key + 1);
        }
        if (!$res) {
            $this->error(L('_ERROR_SORT_') . L('_PERIOD_'));
        } else {
            $this->success(L('_SUCCESS_SORT_') . L('_PERIOD_'), cookie('__SELF__'));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End SortBuilder Class

/* End of file SortBuilder.class.php */
/* Location: ./Application/Admin/Builder/SortBuilder.class.php */
