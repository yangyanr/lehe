<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

/**
 * 后台公共文件
 * 主要定义后台公共函数库
 */


/**
 * 获取当前用户组
 * @param null $auth_group_id
 * @internal param int $id
 * @return array 用户组数组
 */
function get_auth_group($auth_group_id = null)
{

    $gid = is_array($auth_group_id) ? implode(',', $auth_group_id) : trim($auth_group_id, ',');
    $gid = explode(',', $gid);

    if (empty($gid)) {
        return false;
    }

    $auth_groups = '';

    foreach ($gid as $key => $g) {
        if (is_numeric($g)) {
            $count = (sizeof($gid) - 1);
            if ($key == $count) {
                $auth_group = M('AuthGroup')->where('id=' . $g)->getField('title');
            } else {
                $auth_group = M('AuthGroup')->where('id=' . $g)->getField('title') . '，';
            }
            $auth_groups .= $auth_group;
        }
    }

    if (empty($auth_groups)) {
        return false;
    }

    return $auth_groups;

}

// ---------------------------------------------------------------------------------------------------------------------

/**
 * 获取版块
 * @param null $forum_id
 * @return bool|mixed
 */
function get_forum($forum_id = null)
{

    if (empty($forum_id)) {
        return false;
    }

    $forum = M('Forum')->where('id=' . $forum_id)->getField('title');

    if (empty($forum)) {
        return false;
    }

    return $forum;
}

// ---------------------------------------------------------------------------------------------------------------------

/**
 * 获取帖子
 * @param null $post_id
 * @return bool|mixed
 */
function get_post($post_id = null)
{

    if (empty($post_id)) {
        return false;
    }

    $post = M('ForumPost')->where('id=' . $post_id)->getField('title');

    if (empty($post)) {
        return false;
    }

    return $post;
}

// ---------------------------------------------------------------------------------------------------------------------

/**
 * 获取版块
 * 根据回复帖子的id 查询
 * @param null $reply_id
 * @return bool|mixed
 */
function get_forum_by_reply_id($reply_id = null)
{

    if (empty($reply_id)) {
        return false;
    }
    $post = M('ForumPost')->where('id=' . $reply_id)->getField('forum_id');

    if (empty($post)) {
        return false;
    }

    $forum = M('Forum')->where('id=' . $post['forum_id'])->getField('title');

    if (empty($forum)) {
        return false;
    }

    return $forum;
}

// ---------------------------------------------------------------------------------------------------------------------

// End custom Function

/* End of file custom.php */
/* Location: ./Application/Admin/Common/custom.php */
