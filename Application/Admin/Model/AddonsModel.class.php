<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

// ---------------------------------------------------------------------------------------------------------------------


/**
 * 插件模型
 */

class AddonsModel extends Model {

    /**
     * 查找后置操作
     * @param $result
     * @param $options
     */
    protected function _after_find(&$result,$options) {

    }

    // -----------------------------------------------------------------------------------------------------------------

    protected function _after_select(&$result,$options){

        foreach($result as &$record){
            $this->_after_find($record,$options);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 文件模型自动完成
     * @var array
     */
    protected $_auto = array(
        array('create_time', NOW_TIME, self::MODEL_INSERT),
    );

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 获取插件列表
     * @param string $addon_dir
     * @return array
     */
    public function getList($addon_dir = ''){
        if(!$addon_dir)
            $addon_dir = LE_HE_ADDONS_PATH;
        $dirs = array_map('basename',glob($addon_dir.'*', GLOB_ONLYDIR));
        if($dirs === FALSE || !file_exists($addon_dir)){
            $this->error = '插件目录不可读或者不存在';
            return FALSE;
        }
		$addons			=	array();
		$where['name']	=	array('in',$dirs);
		$list			=	$this->where($where)->field(true)->select();
		foreach($list as $addon){
			$addon['uninstall']		=	0;
			$addons[$addon['name']]	=	$addon;
		}
        foreach ($dirs as $value) {
            if(!isset($addons[$value])){
				$class = get_addon_class($value);
				if(!class_exists($class)){ // 实例化插件失败忽略执行
					\Think\Log::record('插件'.$value.'的入口文件不存在');
					continue;
				}
                $obj    =   new $class;
				$addons[$value]	= $obj->info;
				if($addons[$value]){
					$addons[$value]['uninstall'] = 1;
                    unset($addons[$value]['status']);
				}
			}
        }
        int_to_string($addons, array('status'=>array(-1=>'损坏', 0=>'禁用', 1=>'启用', null=>'未安装')));
        $addons = list_sort_by($addons,'uninstall','desc');
        return $addons;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 获取插件的后台列表
     */
    public function getAdminList(){
        $admin = array();
        $db_addons = $this->where("status=1 AND has_adminlist=1")->field('title,name')->select();
        if($db_addons){
            foreach ($db_addons as $value) {
                $admin[] = array('title'=>$value['title'],'url'=>"Addons/adminList?name={$value['name']}");
            }
        }
        return $admin;
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End AddonsModel Model

/* End of file AddonsModel.class.php */
/* Location: ./Application/Admin/Model/AddonsModel.class.php */
