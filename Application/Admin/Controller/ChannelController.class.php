<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

namespace Admin\Controller;

// ---------------------------------------------------------------------------------------------------------------------

/**
 * 后台导航控制器
 * @property string meta_title
 */

class ChannelController extends AdminController
{

    /**
     * 频道列表
     */
    public function index()
    {
        $pid = I('get.pid', 0);
        /* 获取频道列表 */
        $map = array('status' => array('gt', -1), 'pid' => $pid);
        $list = M('Channel')->where($map)->order('sort asc,id asc')->select();

        $this->assign('list', $list);
        $this->assign('pid', $pid);
        $this->meta_title = '导航管理';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 添加频道
     */
    public function add()
    {
        if (IS_POST) {
            $Channel = D('Channel');
            $data = $Channel->create();
            if ($data) {
                $id = $Channel->add();
                if ($id) {
                    //记录行为
                    action_log('add_channel', 'channel', $id, UID);
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Channel->getError());
            }
        } else {
            $pid = I('get.pid', 0);
            //获取父导航
            if (!empty($pid)) {
                $parent = M('Channel')->where(array('id' => $pid))->field('title')->find();
                $this->assign('parent', $parent);
            }

            $this->assign('pid', $pid);
            $this->assign('info', null);
            $this->meta_title = '新增导航';
            $this->display('edit');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 编辑频道
     * @param int $id
     */
    public function edit($id = 0)
    {
        if (IS_POST) {
            $Channel = D('Channel');
            $data = $Channel->create();
            if ($data) {
                if ($Channel->save()) {
                    //记录行为
                    action_log('update_channel', 'channel', $data['id'], UID);
                    $this->success('编辑成功', U('index'));
                } else {
                    $this->error('编辑失败');
                }

            } else {
                $this->error($Channel->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('Channel')->find($id);

            if (false === $info) {
                $this->error('获取配置信息错误');
            }

            $pid = I('get.pid', 0);
            //获取父导航
            if (!empty($pid)) {
                $parent = M('Channel')->where(array('id' => $pid))->field('title')->find();
                $this->assign('parent', $parent);
            }

            $this->assign('pid', $pid);
            $this->assign('info', $info);
            $this->meta_title = '编辑导航';
            $this->display();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 删除频道
     */
    public function del()
    {
        $id = array_unique((array)I('id', 0));

        if (empty($id)) {
            $this->error('请选择要操作的数据');
        }

        $map = array('id' => array('in', $id));
        if (M('Channel')->where($map)->delete()) {
            //记录行为
            action_log('delete_channel', 'channel', $id, UID);
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 导航排序
     */
    public function sort()
    {
        if (IS_GET) {
            $ids = I('get.ids');
            $pid = I('get.pid');

            //获取排序的数据
            $map = array('status' => array('gt', -1));
            if (!empty($ids)) {
                $map['id'] = array('in', $ids);
            } else {
                if ($pid !== '') {
                    $map['pid'] = $pid;
                }
            }
            $list = M('Channel')->where($map)->field('id,title')->order('sort asc,id asc')->select();

            $this->assign('list', $list);
            $this->meta_title = '导航排序';
            $this->display();
        } elseif (IS_POST) {
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key => $value) {
                $res = M('Channel')->where(array('id' => $value))->setField('sort', $key + 1);
            }
            if ($res !== false) {
                //记录行为
                action_log('sort_channel', 'Channel', UID, UID);
                $this->success('排序成功');
            } else {
                $this->error('排序失败');
            }
        } else {
            $this->error('非法请求');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End ChannelController Class

/* End of file ChannelController.class.php */
/* Location: ./Application/Admin/Controller/ChannelController.class.php */
