<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

namespace Admin\Controller;

// ---------------------------------------------------------------------------------------------------------------------


/**
 * seo规则控制器
 * Class SeoController
 * @property string meta_title
 * @package Admin\Controller
 */
class SeoController extends AdminController
{

    /**
     * seo规则列表
     */
    public function index()
    {
        $map['status'] = array('gt', -1);
        $Seo = M('Seo');
        $list = $this->lists($Seo, $map, ' id asc ');
        int_to_string($list);
        $this->assign('_list', $list);
        $this->meta_title = 'SEO规则列表';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 添加seo规则
     */
    public function add()
    {
        if (IS_POST) {
            $Seo = D('Seo');
            $data = $Seo->create();
            if ($data) {
                $id = $Seo->add();
                if ($id) {
                    //记录行为
                    action_log('add_seo', 'seo', $id, UID);
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Seo->getError());
            }
        } else {
            $this->assign('info', null);
            $this->meta_title = '新增SEO规则';
            $this->display('edit');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 编辑seo规则
     * @param int $id
     */
    public function edit($id = 0)
    {
        if (IS_POST) {
            $Seo = D('Seo');
            $data = $Seo->create();
            if ($data) {
                if ($Seo->save()) {
                    //记录行为
                    action_log('update_seo', 'seo', $data['id'], UID);
                    $this->success('编辑成功', U('index'));
                } else {
                    $this->error('编辑失败');
                }

            } else {
                $this->error($Seo->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('Seo')->find($id);

            if (false === $info) {
                $this->error('获取信息错误');
            }

            $this->assign('info', $info);
            $this->meta_title = '编辑SEO规则';
            $this->display();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 删除频道
     */
    public function del()
    {
        $id = array_unique((array)I('id', 0));

        if (empty($id)) {
            $this->error('请选择要操作的数据');
        }

        $map = array('id' => array('in', $id));
        if (M('Seo')->where($map)->save()) {
            //记录行为
            action_log('delete_seo', 'seo', $id, UID);
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 回收站列表
     */
    public function recycle()
    {

        $map['status'] = -1;

        $Seo = M('Seo');
        $list = $this->lists($Seo, $map, ' id asc ');
        int_to_string($list);

        $this->assign('list', $list);
        $this->meta_title = '回收站';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 还原被删除的数据
     */
    public function permit()
    {
        /*参数过滤*/
        $ids = I('param.ids');
        if (empty($ids)) {
            $this->error('请选择要操作的数据');
        }

        /*拼接参数并修改状态*/
        $Model = 'Seo';
        $map = array();
        if (is_array($ids)) {
            $map['id'] = array('in', $ids);
        } elseif (is_numeric($ids)) {
            $map['id'] = $ids;
        }
        $this->restore($Model, $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 清空回收站
     */
    public function clear()
    {
        $res = D('Seo')->remove();
        if ($res !== false) {
            //行为记录
            action_log('clear_seo', 'seo', UID, UID);
            $this->success('清空回收站成功');
        } else {
            $this->error('清空回收站失败');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End SeoController Class

/* End of file SeoController.class.php */
/* Location: ./Application/Admin/Controller/SeoController.class.php */
