<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------

namespace Admin\Controller;

// ---------------------------------------------------------------------------------------------------------------------

/**
 * 属性控制器
 * @property string meta_title
 */
class AttributeController extends AdminController
{

    /**
     * 属性列表
     */
    public function index()
    {
        $model_id = I('get.model_id');
        /* 查询条件初始化 */
        $map['model_id'] = $model_id;

        $list = $this->lists('Attribute', $map);
        int_to_string($list);

        // 记录当前列表页的cookie
        Cookie('__forward__', $_SERVER['REQUEST_URI']);
        $this->assign('_list', $list);
        $this->assign('model_id', $model_id);
        $this->meta_title = '属性列表';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 新增页面初始化
     */
    public function add()
    {
        $model_id = I('get.model_id');
        $model = M('Model')->field('title,name,field_group')->find($model_id);
        $this->assign('model', $model);
        $this->assign('info', array('model_id' => $model_id));
        $this->meta_title = '新增属性';
        $this->display('edit');
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 编辑页面初始化
     */
    public function edit()
    {
        $id = I('get.id', '');
        if (empty($id)) {
            $this->error('参数不能为空');
        }

        /*获取一条记录的详细数据*/
        $Model = M('Attribute');
        $data = $Model->field(true)->find($id);
        if (!$data) {
            $this->error($Model->getError());
        }
        $model = M('Model')->field('title,name,field_group')->find($data['model_id']);
        $this->assign('model', $model);
        $this->assign('info', $data);
        $this->meta_title = '编辑属性';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 更新一条数据
     */
    public function update()
    {
        $res = D('Attribute')->update();
        if (!$res) {
            $this->error(D('Attribute')->getError());
        } else {
            $this->success($res['id'] ? '更新成功' : '新增成功', Cookie('__forward__'));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 删除一条数据
     */
    public function remove()
    {
        $id = I('id');
        empty($id) && $this->error('参数错误');

        $Model = D('Attribute');

        $info = $Model->getById($id);
        empty($info) && $this->error('该字段不存在');

        //删除属性数据
        $res = $Model->delete($id);

        //删除表字段
        $Model->deleteField($info);
        if (!$res) {
            $this->error(D('Attribute')->getError());
        } else {
            //记录行为
            action_log('update_attribute', 'attribute', $id, UID);
            $this->success('删除成功', U('index', 'model_id=' . $info['model_id']));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End AttributeController Class

/* End of file AttributeController.class.php */
/* Location: ./Application/Admin/Controller/AttributeController.class.php */

