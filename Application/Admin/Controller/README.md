﻿# 控制器文件目录
# AdminController  父类控制器，所有控制器继承该控制器
# ActionController 行为控制器
# AddonsController 扩展控制器
# ArticleController 内容控制器
# AttributeController 属性控制器
# AuthManagerController 权限管理控制器
# CategoryController 分类控制器
# ChannelController 导航控制器
# ConfigController 配置控制器
# DatabaseController 数据库备份还原控制器
# FileController 文件控制器
# IndexController 首页控制器
# MenuController 菜单控制器
# IndexController 首页控制器
# ModelController 模型控制器
# PublicController 登录控制器
# QiniuController 七牛扩展测试控制器
# SeoController seo规则控制器
# TestController 测试控制器
# ThinkController 模型数据管理控制器
# UpdateController 在线更新控制器
# UserController 用户控制器

