<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------------

namespace Admin\Controller;

// -----------------------------------------------------------------------------------------------------------------

/**
 * @property string meta_title
 */
class ForumController extends AdminController
{

    /**
     * 默认页面
     */
    public function index()
    {
        redirect(U('Forum/forum'));
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 版块列表
     */
    public function forum()
    {
        $map['status'] = array('gt', -1);
        $Forum = M('Forum');
        $list = $this->lists($Forum, $map, ' id asc ');
        $this->assign('_list', $list);
        $this->meta_title = '版块管理列表';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 帖子列表
     */
    public function post()
    {
        $map['status'] = array('gt', -1);
        $ForumPost = M('ForumPost');
        $list = $this->lists($ForumPost, $map, ' id asc ');
        $this->assign('_list', $list);
        $this->meta_title = '帖子管理列表';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 帖子回复列表
     */
    public function reply()
    {
        $map['status'] = array('gt', -1);
        $ForumPostReply = M('ForumPostReply');
        $list = $this->lists($ForumPostReply, $map, ' id asc ');
        $this->assign('_list', $list);
        $this->meta_title = '回复管理列表';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 添加版块
     */
    public function add()
    {

        if (IS_POST) {
            if (isset($_POST['allow_user_group'])) {
                sort($_POST['allow_user_group']);
                $_POST['allow_user_group'] = implode(',', array_unique($_POST['allow_user_group']));
            }
            $Forum = D('Forum');
            $data = $Forum->create();
            if ($data) {
                $id = $Forum->add();
                if ($id) {
                    //记录行为
                    action_log('add_forum', 'forum', $id, UID);
                    $this->success('新增成功', U('index'));
                } else {
                    $this->error('新增失败');
                }
            } else {
                $this->error($Forum->getError());
            }
        } else {
            /*读取用户组*/
            $AuthGroup = D('AuthGroup');
            $user_groups = $AuthGroup->getGroups();
            $this->assign('user_groups', $user_groups);
            $this->assign('info', null);
            $this->meta_title = '新增版块';
            $this->display('edit');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 编辑版块
     * @param int $id
     */
    public function edit($id = 0)
    {
        if (IS_POST) {
            if (isset($_POST['allow_user_group'])) {
                sort($_POST['allow_user_group']);
                $_POST['allow_user_group'] = implode(',', array_unique($_POST['allow_user_group']));
            }
            $Forum = D('Forum');
            $data = $Forum->create();
            if ($data) {
                if ($Forum->save()) {
                    //记录行为
                    action_log('update_forum', 'forum', $data['id'], UID);
                    $this->success('编辑成功', U('index'));
                } else {
                    $this->error('编辑失败');
                }

            } else {
                $this->error($Forum->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('Forum')->find($id);

            if (false === $info) {
                $this->error('获取信息错误');
            }
            /*读取用户组*/
            $AuthGroup = D('AuthGroup');
            $user_groups = $AuthGroup->getGroups();
            $this->assign('user_groups', $user_groups);
            $this->assign('info', $info);
            $this->meta_title = '编辑版块';
            $this->display();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 编辑帖子
     * @param int $id
     */
    public function editPost($id = 0)
    {
        if (IS_POST) {
            $ForumPost = D('ForumPost');
            $data = $ForumPost->create();
            if ($data) {
                if ($ForumPost->save()) {
                    //记录行为
                    action_log('update_post', 'forum_post', $data['id'], UID);
                    $this->success('编辑成功', U('post'));
                } else {
                    $this->error('编辑失败');
                }

            } else {
                $this->error($ForumPost->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('ForumPost')->find($id);

            if (false === $info) {
                $this->error('获取信息错误');
            }
            $this->assign('info', $info);
            $this->meta_title = '编辑帖子';
            $this->display();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 编辑帖子回复
     * @param int $id
     */
    public function editReply($id = 0)
    {
        if (IS_POST) {
            $ForumPostReply = D('ForumPostReply');
            $data = $ForumPostReply->create();
            if ($data) {
                if ($ForumPostReply->save()) {
                    //记录行为
                    action_log('update_post_reply', 'forum_post_reply', $data['id'], UID);
                    $this->success('编辑成功', U('reply'));
                } else {
                    $this->error('编辑失败');
                }
            } else {
                $this->error($ForumPostReply->getError());
            }
        } else {
            $info = array();
            /* 获取数据 */
            $info = M('ForumPostReply')->find($id);

            if (false === $info) {
                $this->error('获取信息错误');
            }
            $this->assign('info', $info);
            $this->meta_title = '编辑帖子回复';
            $this->display();
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 版块排序
     */
    public function sort()
    {
        if (IS_GET) {
            $ids = I('get.ids');

            //获取排序的数据
            $map = array('status' => array('gt', -1));
            if (!empty($ids)) {
                $map['id'] = array('in', $ids);
            }
            $list = M('Forum')->where($map)->field('id,title')->order('sort asc,id asc')->select();

            $this->assign('list', $list);
            $this->meta_title = '版块排序';
            $this->display();
        } elseif (IS_POST) {
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key => $value) {
                $res = M('Forum')->where(array('id' => $value))->setField('sort', $key + 1);
            }
            if ($res !== false) {
                //记录行为
                action_log('sort_forum', 'Forum', UID, UID);
                $this->success('排序成功');
            } else {
                $this->error('排序失败');
            }
        } else {
            $this->error('非法请求');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 版块回收站列表
     */
    public function forumTrash()
    {

        $map['status'] = -1;

        $list = $this->lists(M('Forum'), $map, 'update_time desc');

        $this->assign('list', $list);
        $this->meta_title = '版块回收站';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 帖子回收站列表
     */
    public function postTrash()
    {

        $map['status'] = -1;

        $list = $this->lists(M('ForumPost'), $map, 'update_time desc');

        $this->assign('list', $list);
        $this->meta_title = '帖子回收站';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 帖子回复回收站列表
     */
    public function replyTrash()
    {

        $map['status'] = -1;

        $list = $this->lists(M('ForumPostReply'), $map, 'update_time desc');

        $this->assign('list', $list);
        $this->meta_title = '帖子回复回收站';
        $this->display();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 还原被删除的版块数据
     */
    public function permit()
    {
        /*参数过滤*/
        $ids = I('param.ids');
        if (empty($ids)) {
            $this->error('请选择要操作的数据');
        }

        /*拼接参数并修改状态*/
        $Model = 'Forum';
        $map = array();
        if (is_array($ids)) {
            $map['id'] = array('in', $ids);
        } elseif (is_numeric($ids)) {
            $map['id'] = $ids;
        }
        $this->restore($Model, $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 还原被删除的帖子数据
     */
    public function postPermit()
    {
        /*参数过滤*/
        $ids = I('param.ids');
        if (empty($ids)) {
            $this->error('请选择要操作的数据');
        }

        /*拼接参数并修改状态*/
        $Model = 'ForumPost';
        $map = array();
        if (is_array($ids)) {
            $map['id'] = array('in', $ids);
        } elseif (is_numeric($ids)) {
            $map['id'] = $ids;
        }
        $this->restore($Model, $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 还原被删除的帖子回复数据
     */
    public function replyPermit()
    {
        /*参数过滤*/
        $ids = I('param.ids');
        if (empty($ids)) {
            $this->error('请选择要操作的数据');
        }

        /*拼接参数并修改状态*/
        $Model = 'ForumPostReply';
        $map = array();
        if (is_array($ids)) {
            $map['id'] = array('in', $ids);
        } elseif (is_numeric($ids)) {
            $map['id'] = $ids;
        }
        $this->restore($Model, $map);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 清空版块回收站
     */
    public function clear()
    {
        $res = D('Forum')->remove();
        if ($res !== false) {
            //行为记录
            action_log('clear_forum', 'forum', UID, UID);
            $this->success('清空回收站成功');
        } else {
            $this->error('清空回收站失败');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 清空版块回收站
     */
    public function postClear()
    {
        $res = D('ForumPost')->remove();
        if ($res !== false) {
            //行为记录
            action_log('clear_post', 'forum_post', UID, UID);
            $this->success('清空回收站成功');
        } else {
            $this->error('清空回收站失败');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 清空帖子回复回收站
     */
    public function replyClear()
    {
        $res = D('ForumPostReply')->remove();
        if ($res !== false) {
            //行为记录
            action_log('clear_post_reply', 'forum_post_reply', UID, UID);
            $this->success('清空回收站成功');
        } else {
            $this->error('清空回收站失败');
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End ForumController Class

/* End of file ForumController.class.php */
/* Location: ./Application/Admin/Controller/ForumController.class.php */
