<?php

/**
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         有你就好 [ 有节骨乃坚，无心品自端 ]     <http://kaifa.lehe.so>
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                                    独在异乡为异客             每逢佳节倍思亲
 * // +-----------------------------------------------------------------------------------------------------------------
 * // |                         联系:   <707069100@qq.com>        <http://weibo.com/513778937>
 * // +-----------------------------------------------------------------------------------------------------------------
 */

// ---------------------------------------------------------------------------------------------------------------------
// +--------------------------------------------------------------------------------------------------------------------
// |                     ErYang出品    属于小极品          共同学习    共同进步
// +--------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------

namespace Common\Controller;

// ---------------------------------------------------------------------------------------------------------------------

/**
 * 插件类
 */
abstract class Addon
{

    /**
     * 视图实例对象
     * @var view
     * @access protected
     */
    protected $view = null;

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * $info = array(
     *  'name'=>'Editor',
     *  'title'=>'编辑器',
     *  'description'=>'用于增强整站长文本的输入和显示',
     *  'status'=>1,
     *  'author'=>'thinkphp',
     *  'version'=>'0.1'
     *  )
     */
    public $info = array();
    public $addon_path = '';
    public $config_file = '';
    public $custom_config = '';
    public $admin_list = array();
    public $custom_adminlist = '';
    public $access_url = array();

    // -----------------------------------------------------------------------------------------------------------------

    public function __construct()
    {
        $this->view = \Think\Think::instance('Think\View');
        $this->addon_path = LE_HE_ADDONS_PATH . $this->getName() . '/';
        $TMPL_PARSE_STRING = C('TMPL_PARSE_STRING');
        $TMPL_PARSE_STRING['__ADDONROOT__'] = __ROOT__ . '/Addons/' . $this->getName();
        C('TMPL_PARSE_STRING', $TMPL_PARSE_STRING);
        if (is_file($this->addon_path . 'config.php')) {
            $this->config_file = $this->addon_path . 'config.php';
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 模板主题设置
     * @access protected
     * @param string $theme 模版主题
     * @return Action
     */
    final protected function theme($theme)
    {
        $this->view->theme($theme);
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 显示方法
     * @param string $template
     * @throws \Exception
     */
    final protected function display($template = '')
    {
        if ($template == '')
            $template = CONTROLLER_NAME;
        echo($this->fetch($template));
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 模板变量赋值
     * @access protected
     * @param mixed $name 要显示的模板变量
     * @param mixed $value 变量的值
     * @return Action
     */
    final protected function assign($name, $value = '')
    {
        $this->view->assign($name, $value);
        return $this;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 用于显示模板的方法
     * @param mixed|string $templateFile
     * @return mixed
     * @throws \Exception
     */
    final protected function fetch($templateFile = CONTROLLER_NAME)
    {
        if (!is_file($templateFile)) {
            $templateFile = $this->addon_path . $templateFile . C('TMPL_TEMPLATE_SUFFIX');
            if (!is_file($templateFile)) {
                throw new \Exception("模板不存在:$templateFile");
            }
        }
        return $this->view->fetch($templateFile);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 得到名称
     * @return string
     */
    final public function getName()
    {
        $class = get_class($this);
        return substr($class, strrpos($class, '\\') + 1, -5);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 检查信息
     * @return bool
     */
    final public function checkInfo()
    {
        $info_check_keys = array('name', 'title', 'description', 'status', 'author', 'version');
        foreach ($info_check_keys as $value) {
            if (!array_key_exists($value, $this->info))
                return FALSE;
        }
        return TRUE;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 获取插件的配置数组
     * @param string $name
     * @return array|mixed
     */
    final public function getConfig($name = '')
    {
        static $_config = array();
        if (empty($name)) {
            $name = $this->getName();
        }
        if (isset($_config[$name])) {
            return $_config[$name];
        }
        $config = array();
        $map['name'] = $name;
        $map['status'] = 1;
        $config = M('Addons')->where($map)->getField('config');
        if ($config) {
            $config = json_decode($config, true);
        } else {
            $temp_arr = include $this->config_file;
            foreach ($temp_arr as $key => $value) {
                if ($value['type'] == 'group') {
                    foreach ($value['options'] as $gkey => $gvalue) {
                        foreach ($gvalue['options'] as $ikey => $ivalue) {
                            $config[$ikey] = $ivalue['value'];
                        }
                    }
                } else {
                    $config[$key] = $temp_arr[$key]['value'];
                }
            }
        }
        $_config[$name] = $config;
        return $config;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 必须实现安装
     * @return mixed
     */
    abstract public function install();

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * 必须卸载插件方法
     * @return mixed
     */
    abstract public function uninstall();

    // -----------------------------------------------------------------------------------------------------------------

}

// ---------------------------------------------------------------------------------------------------------------------

// End Addon Class

/* End of file Addon.php */
/* Location: ./Application/Common/Controller/Addon.class.php */
