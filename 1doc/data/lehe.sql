-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.17 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 lehe 的数据库结构
CREATE DATABASE IF NOT EXISTS `lehe` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lehe`;


-- 导出  表 lehe.lh_action 结构
CREATE TABLE IF NOT EXISTS `lh_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '行为唯一标识',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '行为说明',
  `remark` char(140) NOT NULL DEFAULT '' COMMENT '行为描述',
  `rule` text NOT NULL COMMENT '行为规则',
  `log` text NOT NULL COMMENT '日志规则',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态，-1 已删除，0 被禁用，1 正常，2 未审核',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统行为表';

-- 正在导出表  lehe.lh_action 的数据：68 rows
DELETE FROM `lh_action`;
/*!40000 ALTER TABLE `lh_action` DISABLE KEYS */;
INSERT INTO `lh_action` (`id`, `name`, `title`, `remark`, `rule`, `log`, `type`, `status`, `update_time`) VALUES
	(1, 'user_login', '用户登录', '积分+10，每天一次', 'table:member|field:score|condition:uid={$self} AND status>-1|rule:score+10|cycle:24|max:1;', '[user|get_nickname]在[time|time_format]登录了后台', 1, 1, 1387181220),
	(2, 'add_document', '新增文档', '新增文档', '', '[user|get_nickname]在[time|time_format]新增了一篇文章。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(3, 'update_document', '更新文档', '更新文档', '', '[user|get_nickname]在[time|time_format]更新了一篇文章。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(4, 'update_table_data', '更新表内容', '对数据表中的单行或多行记录执行修改', '', '[user|get_nickname]在[time|time_format]更新了表内容。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(5, 'sort_document', '文档排序', '文档排序', '', '[user|get_nickname]在[time|time_format]进行文档排序。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(6, 'clear_document', '清空回收站', '清空回收站', '', '[user|get_nickname]在[time|time_format]进行清空回收站。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(7, 'add_user', '新增用户', '新增用户', '', '[user|get_nickname]在[time|time_format]新增了用户。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(8, 'update_user', '更新用户', '更新用户', '', '[user|get_nickname]在[time|time_format]更新了用户。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(9, 'update_user_password', '更新用户密码', '更新用户密码', '', '[user|get_nickname]在[time|time_format]更新了用户密码。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(10, 'update_user_nickname', '更新用户昵称', '更新用户昵称', '', '[user|get_nickname]在[time|time_format]更新了用户昵称。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(11, 'add_action', '新增行为', '新增行为', '', '[user|get_nickname]在[time|time_format]le新增了行为。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(12, 'update_action', '更新行为', '更新行为', '', '[user|get_nickname]在[time|time_format]更新了行为。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(13, 'delete_action_log', '删除行为日志', '删除行为日志', '', '[user|get_nickname]在[time|time_format]删除了行为日志。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(14, 'clear_action_log', '清空行为日志', '清空行为日志', '', '[user|get_nickname]在[time|time_format]清空了行为日志。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(15, 'add_auth_group', '新增用户组', '新增用户组', '', '[user|get_nickname]在[time|time_format]新增了用户组。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(16, 'update_auth_group', '更新用户组', '更新用户组', '', '[user|get_nickname]在[time|time_format]更新了用户组。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(17, 'update_config_group', '更新网站设置', '新增更新或者删除网站设置', '', '[user|get_nickname]在[time|time_format]更新了网站设置。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(18, 'add_config', '新增配置', '新增配置', '', '[user|get_nickname]在[time|time_format]新增了配置。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(19, 'update_config', '更新配置', '更新配置', '', '[user|get_nickname]在[time|time_format]更新了配置。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(20, 'delete_config', '删除配置', '删除配置', '', '[user|get_nickname]在[time|time_format]删除了配置。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(21, 'sort_config', '配置排序', '配置排序', '', '[user|get_nickname]在[time|time_format]更新了网站设置。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(22, 'add_category', '新增分类', '新增分类', '', '[user|get_nickname]在[time|time_format]新增了分类。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(23, 'update_category', '更新分类', '更新分类', '', '[user|get_nickname]在[time|time_format]更新了分类。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(24, 'delete_category', '删除分类', '删除分类', '', '[user|get_nickname]在[time|time_format]删除了分类。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(25, 'move_category', '移动分类', '移动分类', '', '[user|get_nickname]在[time|time_format]移动了分类。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(26, 'merge_category', '合并分类', '合并分类', '', '[user|get_nickname]在[time|time_format]合并了分类。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(27, 'add_menu', '新增菜单', '新增菜单', '', '[user|get_nickname]在[time|time_format]新增了菜单。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(28, 'update_menu', '更新菜单', '更新菜单', '', '[user|get_nickname]在[time|time_format]更新了菜单。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(29, 'delete_menu', '删除菜单', '删除菜单', '', '[user|get_nickname]在[time|time_format]删除了菜单。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(30, 'sort_menu', '菜单排序', '菜单排序', '', '[user|get_nickname]在[time|time_format]进行了菜单排序。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(31, 'add_channel', '新增导航', '新增导航', '', '[user|get_nickname]在[time|time_format]新增了导航。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(32, 'update_channel', '更新导航', '更新导航', '', '[user|get_nickname]在[time|time_format]更新了导航。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(33, 'delete_channel', '删除导航', '删除导航', '', '[user|get_nickname]在[time|time_format]删除了导航。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(34, 'sort_channel', '导航排序', '导航排序', '', '[user|get_nickname]在[time|time_format]进行了导航排序。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(35, 'export_database', '数据备份', '数据备份', '', '[user|get_nickname]在[time|time_format]进行了数据备份。表[model](database)，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(36, 'optimize_table', '优化表', '优化表', '', '[user|get_nickname]在[time|time_format]优化了表。表[model](database)，记录编号[record]。', 1, 1, 1387181220),
	(37, 'repair_table', '修复表', '修复表', '', '[user|get_nickname]在[time|time_format]修复了表。表[model](database)，记录编号[record]。', 1, 1, 1387181220),
	(38, 'import_database', '还原备份文件', '还原备份文件', '', '[user|get_nickname]在[time|time_format]还原了备份文件。表[model](database)，记录编号[record]](管理员UID)。', 1, 1, 1387181220),
	(39, 'delete_database', '删除备份文件', '删除备份文件', '', '[user|get_nickname]在[time|time_format]删除了备份文件。表[model](database)，记录编号[record]](管理员UID)。', 1, 1, 1387181220),
	(40, 'add_addons', '新增插件', '新增插件', '', '[user|get_nickname]在[time|time_format]新增了插件。表[model]，记录编号[record]](管理员UID)。', 1, 1, 1387181220),
	(41, 'update_addons', '更新插件', '更新插件', '', '[user|get_nickname]在[time|time_format]更新了插件。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(42, 'install_addons', '安装插件', '安装插件', '', '[user|get_nickname]在[time|time_format]安装了插件。表[model]，记录编号[record]](管理员UID)。', 1, 1, 1387181220),
	(43, 'uninstall_addons', '卸载插件', '卸载插件', '', '[user|get_nickname]在[time|time_format]卸载了插件。表[model]，记录编号[record]](管理员UID)。', 1, 1, 1387181220),
	(44, 'enable_addons', '启用插件', '启用插件', '', '[user|get_nickname]在[time|time_format]卸载了插件。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(45, 'disable_addons', '禁用插件', '禁用插件', '', '[user|get_nickname]在[time|time_format]卸载了插件。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(46, 'add_hooks', '新增钩子', '新增钩子', '', '[user|get_nickname]在[time|time_format]新增了钩子。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(47, 'update_hooks', '更新钩子', '更新钩子', '', '[user|get_nickname]在[time|time_format]新增了钩子。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(48, 'delete_hooks', '删除钩子', '删除钩子', '', '[user|get_nickname]在[time|time_format]删除了钩子。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(49, 'add_seo', '新增seo规则', '新增seo规则', '', '[user|get_nickname]在[time|time_format]新增了seo规则。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(50, 'edit_seo', '编辑seo规则', '编辑seo规则', '', '[user|get_nickname]在[time|time_format]编辑了seo规则。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(51, 'delete_seo', '删除seo规则', '删除seo规则', '', '[user|get_nickname]在[time|time_format]删除了seo规则。表[model]，记录编号[record]]。', 1, 1, 1387181220),
	(52, 'clear_seo', '清空回收站', '清空回收站', '', '[user|get_nickname]在[time|time_format]清空了seo规则回收站。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(53, 'add_patch', '添加补丁', '添加补丁', '', '[user|get_nickname]在[time|time_format]新增了补丁。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(54, 'update_patch', '更新补丁', '更新补丁', '', '[user|get_nickname]在[time|time_format]更新了补丁。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(55, 'delete_patch', '删除补丁', '删除补丁', '', '[user|get_nickname]在[time|time_format]删除了补丁。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(56, 'add_forum', '新增版块', '新增版块', '', '[user|get_nickname]在[time|time_format]新增了版块。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(57, 'update_forum', '编辑版块', '编辑版块', '', '[user|get_nickname]在[time|time_format]编辑了版块。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(58, 'sort_forum', '版块排序', '版块排序', '', '[user|get_nickname]在[time|time_format]进行了版块排序。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(59, 'clear_forum', '清空版块回收站', '清空版块回收站', '', '[user|get_nickname]在[time|time_format]清空了版块回收站。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(60, 'add_post', '发帖子', '积分+3，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+3|cycle:24|max:5;', '', 1, 1, 1387181220),
	(61, 'update_post', '编辑帖子', '编辑帖子', '', '[user|get_nickname]在[time|time_format]编辑了帖子。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(62, 'clear_post', '清空帖子回收站', '清空帖子回收站', '', '[user|get_nickname]在[time|time_format]清空了帖子回收站。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(63, 'add_post_reply', '发帖子回复', '积分+1，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+1|cycle:24|max:5;', '', 1, 1, 1387181220),
	(64, 'update_post_reply', '编辑帖子回复', '编辑帖子回复', '', '[user|get_nickname]在[time|time_format]编辑了帖子回复。表[model]，记录编号[record]。', 1, 1, 1387181220),
	(65, 'clear_post_reply', '清空帖子回复', '清空帖子回复', '', '[user|get_nickname]在[time|time_format]清空了帖子回复回收站。表[model]，记录编号[record](管理员UID)。', 1, 1, 1387181220),
	(66, 'send_register_email', '发送注册邮件', '发送注册邮件', '', '[user|get_nickname]在[time|time_format]向用户(ID为[record])发送了注册邮件。', 1, 1, 1387181220),
	(67, 'send_password_email', '发送找回密码邮件', '发送找回密码邮件', '', '[user|get_nickname]在[time|time_format]向用户(ID为[record])发送了找回密码邮件。', 1, 1, 1387181220),
	(68, 'send_change_email', '发送更换邮箱邮件', '发送更换邮箱邮件', '', '[user|get_nickname]在[time|time_format]向用户(ID为[record])发送了更换邮箱邮件。', 1, 1, 1387181220);
/*!40000 ALTER TABLE `lh_action` ENABLE KEYS */;


-- 导出  表 lehe.lh_action_log 结构
CREATE TABLE IF NOT EXISTS `lh_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行为id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行用户id',
  `action_ip` bigint(20) NOT NULL COMMENT '执行行为者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '触发行为的表',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '触发行为的数据id',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '日志备注',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态，-1 已删除，0 被禁用，1 正常，2 未审核',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '执行行为的时间',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行为日志表';

-- 正在导出表  lehe.lh_action_log 的数据：3 rows
DELETE FROM `lh_action_log`;
/*!40000 ALTER TABLE `lh_action_log` DISABLE KEYS */;
INSERT INTO `lh_action_log` (`id`, `action_id`, `user_id`, `action_ip`, `model`, `record_id`, `remark`, `status`, `create_time`) VALUES
	(1, 7, 1, 2130706433, 'ucenter_member', 21, 'admin在2015-11-01 00:19新增了用户。表ucenter_member，记录编号21。', 1, 1446308376),
	(2, 7, 1, 2130706433, 'member', 21, 'admin在2015-11-01 00:19新增了用户。表member，记录编号21。', 1, 1446308376),
	(3, 1, 1, 2130706433, 'member', 1, 'admin在2015-11-01 00:20登录了后台', 1, 1446308411);
/*!40000 ALTER TABLE `lh_action_log` ENABLE KEYS */;


-- 导出  表 lehe.lh_addons 结构
CREATE TABLE IF NOT EXISTS `lh_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- 正在导出表  lehe.lh_addons 的数据：9 rows
DELETE FROM `lh_addons`;
/*!40000 ALTER TABLE `lh_addons` DISABLE KEYS */;
INSERT INTO `lh_addons` (`id`, `name`, `title`, `description`, `status`, `config`, `author`, `version`, `create_time`, `has_adminlist`) VALUES
	(15, 'EditorForAdmin', '后台编辑器', '用于增强整站长文本的输入和显示', 1, '{"editor_type":"2","editor_wysiwyg":"1","editor_height":"500px","editor_resize_type":"1"}', 'thinkphp', '0.1', 1383126253, 0),
	(2, 'SiteStat', '站点统计信息', '统计站点的基础信息', 1, '{"title":"\\u7cfb\\u7edf\\u4fe1\\u606f","width":"1","display":"1","status":"0"}', 'thinkphp', '0.1', 1379512015, 0),
	(3, 'DevTeam', '开发团队信息', '开发团队成员信息', 1, '{"title":"OneThink\\u5f00\\u53d1\\u56e2\\u961f","width":"6","display":"0"}', 'thinkphp', '0.1', 1379512022, 0),
	(4, 'SystemInfo', '系统环境信息', '用于显示一些服务器的信息', 1, '{"title":"\\u7cfb\\u7edf\\u4fe1\\u606f","width":"12","display":"1"}', 'thinkphp', '0.1', 1379512036, 0),
	(5, 'Editor', '前台编辑器', '用于增强整站长文本的输入和显示', 1, '{"editor_type":"2","editor_wysiwyg":"1","editor_height":"300px","editor_resize_type":"1"}', 'thinkphp', '0.1', 1379830910, 0),
	(6, 'Attachment', '附件', '用于文档模型上传附件', 1, 'null', 'thinkphp', '0.1', 1379842319, 1),
	(9, 'SocialComment', '通用社交化评论', '集成了各种社交化评论插件，轻松集成到系统中。', 1, '{"comment_type":"1","comment_uid_youyan":"1954848","comment_short_name_duoshuo":"","comment_data_list_duoshuo":""}', 'thinkphp', '0.1', 1380273962, 0),
	(16, 'Avatar', '头像插件', '用于头像的上传', 1, '{"random":"1"}', '无名', '0.1', 1409038282, 1),
	(17, 'SuperLinks', '合作单位', '合作单位', 1, '{"random":"1"}', '苏南 newsn.net', '0.1', 1413523570, 1);
/*!40000 ALTER TABLE `lh_addons` ENABLE KEYS */;


-- 导出  表 lehe.lh_attachment 结构
CREATE TABLE IF NOT EXISTS `lh_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '附件显示名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件类型',
  `source` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '资源ID',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联记录ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小',
  `dir` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '上级目录ID',
  `sort` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `idx_record_status` (`record_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='附件表';

-- 正在导出表  lehe.lh_attachment 的数据：0 rows
DELETE FROM `lh_attachment`;
/*!40000 ALTER TABLE `lh_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_attachment` ENABLE KEYS */;


-- 导出  表 lehe.lh_attribute 结构
CREATE TABLE IF NOT EXISTS `lh_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段注释',
  `field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定义',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '数据类型',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默认值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '参数',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态，-1 已删除，0 被禁用，1 正常，2 未审核',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `validate_rule` varchar(255) NOT NULL,
  `validate_time` tinyint(1) unsigned NOT NULL,
  `error_info` varchar(100) NOT NULL,
  `validate_type` varchar(25) NOT NULL,
  `auto_rule` varchar(100) NOT NULL,
  `auto_time` tinyint(1) unsigned NOT NULL,
  `auto_type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='模型属性表';

-- 正在导出表  lehe.lh_attribute 的数据：34 rows
DELETE FROM `lh_attribute`;
/*!40000 ALTER TABLE `lh_attribute` DISABLE KEYS */;
INSERT INTO `lh_attribute` (`id`, `name`, `title`, `field`, `type`, `value`, `remark`, `is_show`, `extra`, `model_id`, `is_must`, `status`, `update_time`, `create_time`, `validate_rule`, `validate_time`, `error_info`, `validate_type`, `auto_rule`, `auto_time`, `auto_type`) VALUES
	(1, 'uid', '用户ID', 'int(10) unsigned NOT NULL ', 'num', '0', '', 0, '', 1, 0, 1, 1384508362, 1383891233, '', 0, '', '', '', 0, ''),
	(2, 'name', '标识', 'char(40) NOT NULL ', 'string', '', '同一根节点下标识不重复', 1, '', 1, 0, 1, 1383894743, 1383891233, '', 0, '', '', '', 0, ''),
	(3, 'title', '标题', 'char(80) NOT NULL ', 'string', '', '文档标题', 1, '', 1, 0, 1, 1383894778, 1383891233, '', 0, '', '', '', 0, ''),
	(4, 'category_id', '所属分类', 'int(10) unsigned NOT NULL ', 'string', '', '', 0, '', 1, 0, 1, 1384508336, 1383891233, '', 0, '', '', '', 0, ''),
	(5, 'description', '描述', 'char(140) NOT NULL ', 'textarea', '', '', 1, '', 1, 0, 1, 1383894927, 1383891233, '', 0, '', '', '', 0, ''),
	(6, 'root', '根节点', 'int(10) unsigned NOT NULL ', 'num', '0', '该文档的顶级文档编号', 0, '', 1, 0, 1, 1384508323, 1383891233, '', 0, '', '', '', 0, ''),
	(7, 'pid', '所属ID', 'int(10) unsigned NOT NULL ', 'num', '0', '父文档编号', 0, '', 1, 0, 1, 1384508543, 1383891233, '', 0, '', '', '', 0, ''),
	(8, 'model_id', '内容模型ID', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '该文档所对应的模型', 0, '', 1, 0, 1, 1384508350, 1383891233, '', 0, '', '', '', 0, ''),
	(9, 'type', '内容类型', 'tinyint(3) unsigned NOT NULL ', 'select', '2', '', 1, '1:目录\r\n2:主题\r\n3:段落', 1, 0, 1, 1384511157, 1383891233, '', 0, '', '', '', 0, ''),
	(10, 'position', '推荐位', 'smallint(5) unsigned NOT NULL ', 'checkbox', '0', '多个推荐则将其推荐值相加', 1, '[DOCUMENT_POSITION]', 1, 0, 1, 1383895640, 1383891233, '', 0, '', '', '', 0, ''),
	(11, 'link_id', '外链', 'int(10) unsigned NOT NULL ', 'num', '0', '0-非外链，大于0-外链ID,需要函数进行链接与编号的转换', 1, '', 1, 0, 1, 1383895757, 1383891233, '', 0, '', '', '', 0, ''),
	(12, 'cover_id', '封面', 'int(10) unsigned NOT NULL ', 'picture', '0', '0-无封面，大于0-封面图片ID，需要函数处理', 1, '', 1, 0, 1, 1384147827, 1383891233, '', 0, '', '', '', 0, ''),
	(13, 'display', '可见性', 'tinyint(3) unsigned NOT NULL ', 'radio', '1', '', 1, '0:不可见\r\n1:所有人可见', 1, 0, 1, 1386662271, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
	(14, 'deadline', '截至时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '0-永久有效', 1, '', 1, 0, 1, 1387163248, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
	(15, 'attach', '附件数量', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '', 0, '', 1, 0, 1, 1387260355, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
	(16, 'view', '浏览量', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 1, 0, 1, 1383895835, 1383891233, '', 0, '', '', '', 0, ''),
	(17, 'comment', '评论数', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 1, 0, 1, 1383895846, 1383891233, '', 0, '', '', '', 0, ''),
	(18, 'extend', '扩展统计字段', 'int(10) unsigned NOT NULL ', 'num', '0', '根据需求自行使用', 0, '', 1, 0, 1, 1384508264, 1383891233, '', 0, '', '', '', 0, ''),
	(19, 'level', '优先级', 'int(10) unsigned NOT NULL ', 'num', '0', '越高排序越靠前', 1, '', 1, 0, 1, 1383895894, 1383891233, '', 0, '', '', '', 0, ''),
	(20, 'create_time', '创建时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', 1, '', 1, 0, 1, 1383895903, 1383891233, '', 0, '', '', '', 0, ''),
	(21, 'update_time', '更新时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', 0, '', 1, 0, 1, 1384508277, 1383891233, '', 0, '', '', '', 0, ''),
	(22, 'status', '数据状态', 'tinyint(4) NOT NULL ', 'radio', '0', '', 0, '-1:删除\r\n0:禁用\r\n1:正常\r\n2:待审核\r\n3:草稿', 1, 0, 1, 1384508496, 1383891233, '', 0, '', '', '', 0, ''),
	(23, 'parse', '内容解析类型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', 0, '0:html\r\n1:ubb\r\n2:markdown', 2, 0, 1, 1384511049, 1383891243, '', 0, '', '', '', 0, ''),
	(24, 'content', '文章内容', 'text NOT NULL ', 'editor', '', '', 1, '', 2, 0, 1, 1383896225, 1383891243, '', 0, '', '', '', 0, ''),
	(25, 'template', '详情页显示模板', 'varchar(100) NOT NULL ', 'string', '', '参照display方法参数的定义', 1, '', 2, 0, 1, 1383896190, 1383891243, '', 0, '', '', '', 0, ''),
	(26, 'bookmark', '收藏数', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 2, 0, 1, 1383896103, 1383891243, '', 0, '', '', '', 0, ''),
	(27, 'parse', '内容解析类型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', 0, '0:html\r\n1:ubb\r\n2:markdown', 3, 0, 1, 1387260461, 1383891252, '', 0, '', 'regex', '', 0, 'function'),
	(28, 'content', '下载详细描述', 'text NOT NULL ', 'editor', '', '', 1, '', 3, 0, 1, 1383896438, 1383891252, '', 0, '', '', '', 0, ''),
	(29, 'template', '详情页显示模板', 'varchar(100) NOT NULL ', 'string', '', '', 1, '', 3, 0, 1, 1383896429, 1383891252, '', 0, '', '', '', 0, ''),
	(30, 'file_id', '文件ID', 'int(10) unsigned NOT NULL ', 'file', '0', '需要函数处理', 1, '', 3, 0, 1, 1383896415, 1383891252, '', 0, '', '', '', 0, ''),
	(31, 'download', '下载次数', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 3, 0, 1, 1383896380, 1383891252, '', 0, '', '', '', 0, ''),
	(32, 'size', '文件大小', 'bigint(20) unsigned NOT NULL ', 'num', '0', '单位bit', 1, '', 3, 0, 1, 1383896371, 1383891252, '', 0, '', '', '', 0, ''),
	(33, 'source_name', '文章原文网站名称', 'char(100) NOT NULL ', 'string', '', '', 1, '', 1, 0, 1, 0, 0, '', 0, '', '', '', 0, ''),
	(34, 'source_url', '文章原文网站url', 'char(255) NOT NULL ', 'string', '', '', 1, '', 1, 0, 1, 0, 0, '', 0, '', '', '', 0, '');
/*!40000 ALTER TABLE `lh_attribute` ENABLE KEYS */;


-- 导出  表 lehe.lh_auth_extend 结构
CREATE TABLE IF NOT EXISTS `lh_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用户id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '扩展表中数据的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '扩展类型标识 1:栏目分类权限;2:模型权限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组与分类的对应关系表';

-- 正在导出表  lehe.lh_auth_extend 的数据：28 rows
DELETE FROM `lh_auth_extend`;
/*!40000 ALTER TABLE `lh_auth_extend` DISABLE KEYS */;
INSERT INTO `lh_auth_extend` (`group_id`, `extend_id`, `type`) VALUES
	(1, 1, 1),
	(1, 1, 2),
	(1, 2, 1),
	(1, 2, 2),
	(1, 3, 1),
	(1, 3, 2),
	(1, 4, 1),
	(1, 5, 1),
	(1, 6, 1),
	(1, 7, 1),
	(1, 8, 1),
	(1, 9, 1),
	(1, 10, 1),
	(1, 11, 1),
	(1, 12, 1),
	(2, 1, 1),
	(2, 2, 1),
	(2, 3, 1),
	(2, 4, 1),
	(2, 5, 1),
	(2, 6, 1),
	(2, 7, 1),
	(2, 8, 1),
	(2, 9, 1),
	(2, 10, 1),
	(2, 11, 1),
	(2, 12, 1),
	(2, 49, 1);
/*!40000 ALTER TABLE `lh_auth_extend` ENABLE KEYS */;


-- 导出  表 lehe.lh_auth_group 结构
CREATE TABLE IF NOT EXISTS `lh_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '用户组所属模块',
  `type` tinyint(4) NOT NULL COMMENT '组类型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id，多个规则 , 隔开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_auth_group 的数据：2 rows
DELETE FROM `lh_auth_group`;
/*!40000 ALTER TABLE `lh_auth_group` DISABLE KEYS */;
INSERT INTO `lh_auth_group` (`id`, `module`, `type`, `title`, `description`, `status`, `rules`) VALUES
	(1, 'admin', 1, '超级管理员', '超级管理员', 1, '1,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,99,100,102,103,107,108,109,110,111,195,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222'),
	(2, 'admin', 1, '编辑组', '编辑组', 1, '1,3,4,7,8,9,10,11,12,13,14,15,17,18,26,53,54,79,99,107,108,109,110,111,195,211');
/*!40000 ALTER TABLE `lh_auth_group` ENABLE KEYS */;


-- 导出  表 lehe.lh_auth_group_access 结构
CREATE TABLE IF NOT EXISTS `lh_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_auth_group_access 的数据：6 rows
DELETE FROM `lh_auth_group_access`;
/*!40000 ALTER TABLE `lh_auth_group_access` DISABLE KEYS */;
INSERT INTO `lh_auth_group_access` (`uid`, `group_id`) VALUES
	(2, 1),
	(3, 1),
	(4, 2),
	(5, 2),
	(7, 2),
	(10, 2);
/*!40000 ALTER TABLE `lh_auth_group_access` ENABLE KEYS */;


-- 导出  表 lehe.lh_auth_rule 结构
CREATE TABLE IF NOT EXISTS `lh_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '规则所属module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜单',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_auth_rule 的数据：243 rows
DELETE FROM `lh_auth_rule`;
/*!40000 ALTER TABLE `lh_auth_rule` DISABLE KEYS */;
INSERT INTO `lh_auth_rule` (`id`, `module`, `type`, `name`, `title`, `status`, `condition`) VALUES
	(1, 'admin', 2, 'Admin/Index/index', '首页', 1, ''),
	(2, 'admin', 2, 'Admin/Article/mydocument', '内容', -1, ''),
	(3, 'admin', 2, 'Admin/User/index', '用户', 1, ''),
	(4, 'admin', 2, 'Admin/Addons/index', '扩展', 1, ''),
	(5, 'admin', 2, 'Admin/Config/group', '系统', 1, ''),
	(7, 'admin', 1, 'Admin/article/add', '新增', 1, ''),
	(8, 'admin', 1, 'Admin/article/edit', '编辑', 1, ''),
	(9, 'admin', 1, 'Admin/article/setStatus', '改变状态', 1, ''),
	(10, 'admin', 1, 'Admin/article/update', '保存', 1, ''),
	(11, 'admin', 1, 'Admin/article/autoSave', '保存草稿', 1, ''),
	(12, 'admin', 1, 'Admin/article/move', '移动', 1, ''),
	(13, 'admin', 1, 'Admin/article/copy', '复制', 1, ''),
	(14, 'admin', 1, 'Admin/article/paste', '粘贴', 1, ''),
	(15, 'admin', 1, 'Admin/article/permit', '还原', 1, ''),
	(16, 'admin', 1, 'Admin/article/clear', '清空', 1, ''),
	(17, 'admin', 1, 'Admin/article/index', '文档列表', 1, ''),
	(18, 'admin', 1, 'Admin/article/recycle', '回收站', 1, ''),
	(19, 'admin', 1, 'Admin/User/addaction', '新增用户行为', 1, ''),
	(20, 'admin', 1, 'Admin/User/editaction', '编辑用户行为', 1, ''),
	(21, 'admin', 1, 'Admin/User/saveAction', '保存用户行为', 1, ''),
	(22, 'admin', 1, 'Admin/User/setStatus', '变更行为状态', 1, ''),
	(23, 'admin', 1, 'Admin/User/changeStatus?method=forbidUser', '禁用会员', 1, ''),
	(24, 'admin', 1, 'Admin/User/changeStatus?method=resumeUser', '启用会员', 1, ''),
	(25, 'admin', 1, 'Admin/User/changeStatus?method=deleteUser', '删除会员', 1, ''),
	(26, 'admin', 1, 'Admin/User/index', '用户信息', 1, ''),
	(27, 'admin', 1, 'Admin/User/action', '用户行为', 1, ''),
	(28, 'admin', 1, 'Admin/AuthManager/changeStatus?method=deleteGroup', '删除', 1, ''),
	(29, 'admin', 1, 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', 1, ''),
	(30, 'admin', 1, 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢复', 1, ''),
	(31, 'admin', 1, 'Admin/AuthManager/createGroup', '新增', 1, ''),
	(32, 'admin', 1, 'Admin/AuthManager/editGroup', '编辑', 1, ''),
	(33, 'admin', 1, 'Admin/AuthManager/writeGroup', '保存用户组', 1, ''),
	(34, 'admin', 1, 'Admin/AuthManager/group', '授权', 1, ''),
	(35, 'admin', 1, 'Admin/AuthManager/access', '访问授权', 1, ''),
	(36, 'admin', 1, 'Admin/AuthManager/user', '成员授权', 1, ''),
	(37, 'admin', 1, 'Admin/AuthManager/removeFromGroup', '解除授权', 1, ''),
	(38, 'admin', 1, 'Admin/AuthManager/addToGroup', '保存成员授权', 1, ''),
	(39, 'admin', 1, 'Admin/AuthManager/category', '分类授权', 1, ''),
	(40, 'admin', 1, 'Admin/AuthManager/addToCategory', '保存分类授权', 1, ''),
	(41, 'admin', 1, 'Admin/AuthManager/index', '权限管理', 1, ''),
	(42, 'admin', 1, 'Admin/Addons/create', '创建', 1, ''),
	(43, 'admin', 1, 'Admin/Addons/checkForm', '检测创建', 1, ''),
	(44, 'admin', 1, 'Admin/Addons/preview', '预览', 1, ''),
	(45, 'admin', 1, 'Admin/Addons/build', '快速生成插件', 1, ''),
	(46, 'admin', 1, 'Admin/Addons/config', '设置', 1, ''),
	(47, 'admin', 1, 'Admin/Addons/disable', '禁用', 1, ''),
	(48, 'admin', 1, 'Admin/Addons/enable', '启用', 1, ''),
	(49, 'admin', 1, 'Admin/Addons/install', '安装', 1, ''),
	(50, 'admin', 1, 'Admin/Addons/uninstall', '卸载', 1, ''),
	(51, 'admin', 1, 'Admin/Addons/saveconfig', '更新配置', 1, ''),
	(52, 'admin', 1, 'Admin/Addons/adminList', '插件后台列表', 1, ''),
	(53, 'admin', 1, 'Admin/Addons/execute', 'URL方式访问插件', 1, ''),
	(54, 'admin', 1, 'Admin/Addons/index', '插件管理', 1, ''),
	(55, 'admin', 1, 'Admin/Addons/hooks', '钩子管理', 1, ''),
	(56, 'admin', 1, 'Admin/model/add', '新增', 1, ''),
	(57, 'admin', 1, 'Admin/model/edit', '编辑', 1, ''),
	(58, 'admin', 1, 'Admin/model/setStatus', '改变状态', 1, ''),
	(59, 'admin', 1, 'Admin/model/update', '保存数据', 1, ''),
	(60, 'admin', 1, 'Admin/Model/index', '模型管理', 1, ''),
	(61, 'admin', 1, 'Admin/Config/edit', '编辑', 1, ''),
	(62, 'admin', 1, 'Admin/Config/del', '删除', 1, ''),
	(63, 'admin', 1, 'Admin/Config/add', '新增', 1, ''),
	(64, 'admin', 1, 'Admin/Config/save', '保存', 1, ''),
	(65, 'admin', 1, 'Admin/Config/group', '网站设置', 1, ''),
	(66, 'admin', 1, 'Admin/Config/index', '配置管理', 1, ''),
	(67, 'admin', 1, 'Admin/Channel/add', '新增', 1, ''),
	(68, 'admin', 1, 'Admin/Channel/edit', '编辑', 1, ''),
	(69, 'admin', 1, 'Admin/Channel/del', '删除', 1, ''),
	(70, 'admin', 1, 'Admin/Channel/index', '导航管理', 1, ''),
	(71, 'admin', 1, 'Admin/Category/edit', '编辑', 1, ''),
	(72, 'admin', 1, 'Admin/Category/add', '新增', 1, ''),
	(73, 'admin', 1, 'Admin/Category/remove', '删除', 1, ''),
	(74, 'admin', 1, 'Admin/Category/index', '分类管理', 1, ''),
	(75, 'admin', 1, 'Admin/file/upload', '上传控件', -1, ''),
	(76, 'admin', 1, 'Admin/file/uploadPicture', '上传图片', -1, ''),
	(77, 'admin', 1, 'Admin/file/download', '下载', -1, ''),
	(94, 'admin', 1, 'Admin/AuthManager/modelauth', '模型授权', 1, ''),
	(79, 'admin', 1, 'Admin/article/batchOperate', '导入', 1, ''),
	(80, 'admin', 1, 'Admin/Database/index?type=export', '备份数据库', 1, ''),
	(81, 'admin', 1, 'Admin/Database/index?type=import', '还原数据库', 1, ''),
	(82, 'admin', 1, 'Admin/Database/export', '备份', 1, ''),
	(83, 'admin', 1, 'Admin/Database/optimize', '优化表', 1, ''),
	(84, 'admin', 1, 'Admin/Database/repair', '修复表', 1, ''),
	(86, 'admin', 1, 'Admin/Database/import', '恢复', 1, ''),
	(87, 'admin', 1, 'Admin/Database/del', '删除', 1, ''),
	(88, 'admin', 1, 'Admin/User/add', '新增用户', 1, ''),
	(89, 'admin', 1, 'Admin/Attribute/index', '属性管理', 1, ''),
	(90, 'admin', 1, 'Admin/Attribute/add', '新增', 1, ''),
	(91, 'admin', 1, 'Admin/Attribute/edit', '编辑', 1, ''),
	(92, 'admin', 1, 'Admin/Attribute/setStatus', '改变状态', 1, ''),
	(93, 'admin', 1, 'Admin/Attribute/update', '保存数据', 1, ''),
	(95, 'admin', 1, 'Admin/AuthManager/addToModel', '保存模型授权', 1, ''),
	(96, 'admin', 1, 'Admin/Category/move', '移动', -1, ''),
	(97, 'admin', 1, 'Admin/Category/merge', '合并', -1, ''),
	(98, 'admin', 1, 'Admin/Config/menu', '后台菜单管理', -1, ''),
	(99, 'admin', 1, 'Admin/article/mydocument', '我的文档', 1, ''),
	(100, 'admin', 1, 'Admin/Menu/index', '菜单管理', 1, ''),
	(101, 'admin', 1, 'Admin/other', '其他', -1, ''),
	(102, 'admin', 1, 'Admin/Menu/add', '新增', 1, ''),
	(103, 'admin', 1, 'Admin/Menu/edit', '编辑', 1, ''),
	(104, 'admin', 1, 'Admin/Think/lists?model=article', '文章管理', -1, ''),
	(105, 'admin', 1, 'Admin/Think/lists?model=download', '下载管理', -1, ''),
	(106, 'admin', 1, 'Admin/Think/lists?model=config', '配置管理', -1, ''),
	(107, 'admin', 1, 'Admin/Action/actionlog', '行为日志', 1, ''),
	(108, 'admin', 1, 'Admin/User/updatePassword', '修改密码', 1, ''),
	(109, 'admin', 1, 'Admin/User/updateNickname', '修改昵称', 1, ''),
	(110, 'admin', 1, 'Admin/action/edit', '查看行为日志', 1, ''),
	(205, 'admin', 1, 'Admin/think/add', '新增数据', 1, ''),
	(111, 'admin', 2, 'Admin/Article/index', '内容', 1, ''),
	(112, 'admin', 2, 'Admin/article/add', '新增', -1, ''),
	(113, 'admin', 2, 'Admin/article/edit', '编辑', -1, ''),
	(114, 'admin', 2, 'Admin/article/setStatus', '改变状态', -1, ''),
	(115, 'admin', 2, 'Admin/article/update', '保存', -1, ''),
	(116, 'admin', 2, 'Admin/article/autoSave', '保存草稿', -1, ''),
	(117, 'admin', 2, 'Admin/article/move', '移动', -1, ''),
	(118, 'admin', 2, 'Admin/article/copy', '复制', -1, ''),
	(119, 'admin', 2, 'Admin/article/paste', '粘贴', -1, ''),
	(120, 'admin', 2, 'Admin/article/batchOperate', '导入', -1, ''),
	(121, 'admin', 2, 'Admin/article/recycle', '回收站', -1, ''),
	(122, 'admin', 2, 'Admin/article/permit', '还原', -1, ''),
	(123, 'admin', 2, 'Admin/article/clear', '清空', -1, ''),
	(124, 'admin', 2, 'Admin/User/add', '新增用户', -1, ''),
	(125, 'admin', 2, 'Admin/User/action', '用户行为', -1, ''),
	(126, 'admin', 2, 'Admin/User/addAction', '新增用户行为', -1, ''),
	(127, 'admin', 2, 'Admin/User/editAction', '编辑用户行为', -1, ''),
	(128, 'admin', 2, 'Admin/User/saveAction', '保存用户行为', -1, ''),
	(129, 'admin', 2, 'Admin/User/setStatus', '变更行为状态', -1, ''),
	(130, 'admin', 2, 'Admin/User/changeStatus?method=forbidUser', '禁用会员', -1, ''),
	(131, 'admin', 2, 'Admin/User/changeStatus?method=resumeUser', '启用会员', -1, ''),
	(132, 'admin', 2, 'Admin/User/changeStatus?method=deleteUser', '删除会员', -1, ''),
	(133, 'admin', 2, 'Admin/AuthManager/index', '权限管理', -1, ''),
	(134, 'admin', 2, 'Admin/AuthManager/changeStatus?method=deleteGroup', '删除', -1, ''),
	(135, 'admin', 2, 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', -1, ''),
	(136, 'admin', 2, 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢复', -1, ''),
	(137, 'admin', 2, 'Admin/AuthManager/createGroup', '新增', -1, ''),
	(138, 'admin', 2, 'Admin/AuthManager/editGroup', '编辑', -1, ''),
	(139, 'admin', 2, 'Admin/AuthManager/writeGroup', '保存用户组', -1, ''),
	(140, 'admin', 2, 'Admin/AuthManager/group', '授权', -1, ''),
	(141, 'admin', 2, 'Admin/AuthManager/access', '访问授权', -1, ''),
	(142, 'admin', 2, 'Admin/AuthManager/user', '成员授权', -1, ''),
	(143, 'admin', 2, 'Admin/AuthManager/removeFromGroup', '解除授权', -1, ''),
	(144, 'admin', 2, 'Admin/AuthManager/addToGroup', '保存成员授权', -1, ''),
	(145, 'admin', 2, 'Admin/AuthManager/category', '分类授权', -1, ''),
	(146, 'admin', 2, 'Admin/AuthManager/addToCategory', '保存分类授权', -1, ''),
	(147, 'admin', 2, 'Admin/AuthManager/modelauth', '模型授权', -1, ''),
	(148, 'admin', 2, 'Admin/AuthManager/addToModel', '保存模型授权', -1, ''),
	(149, 'admin', 2, 'Admin/Addons/create', '创建', -1, ''),
	(150, 'admin', 2, 'Admin/Addons/checkForm', '检测创建', -1, ''),
	(151, 'admin', 2, 'Admin/Addons/preview', '预览', -1, ''),
	(152, 'admin', 2, 'Admin/Addons/build', '快速生成插件', -1, ''),
	(153, 'admin', 2, 'Admin/Addons/config', '设置', -1, ''),
	(154, 'admin', 2, 'Admin/Addons/disable', '禁用', -1, ''),
	(155, 'admin', 2, 'Admin/Addons/enable', '启用', -1, ''),
	(156, 'admin', 2, 'Admin/Addons/install', '安装', -1, ''),
	(157, 'admin', 2, 'Admin/Addons/uninstall', '卸载', -1, ''),
	(158, 'admin', 2, 'Admin/Addons/saveconfig', '更新配置', -1, ''),
	(159, 'admin', 2, 'Admin/Addons/adminList', '插件后台列表', -1, ''),
	(160, 'admin', 2, 'Admin/Addons/execute', 'URL方式访问插件', -1, ''),
	(161, 'admin', 2, 'Admin/Addons/hooks', '钩子管理', -1, ''),
	(162, 'admin', 2, 'Admin/Model/index', '模型管理', -1, ''),
	(163, 'admin', 2, 'Admin/model/add', '新增', -1, ''),
	(164, 'admin', 2, 'Admin/model/edit', '编辑', -1, ''),
	(165, 'admin', 2, 'Admin/model/setStatus', '改变状态', -1, ''),
	(166, 'admin', 2, 'Admin/model/update', '保存数据', -1, ''),
	(167, 'admin', 2, 'Admin/Attribute/index', '属性管理', -1, ''),
	(168, 'admin', 2, 'Admin/Attribute/add', '新增', -1, ''),
	(169, 'admin', 2, 'Admin/Attribute/edit', '编辑', -1, ''),
	(170, 'admin', 2, 'Admin/Attribute/setStatus', '改变状态', -1, ''),
	(171, 'admin', 2, 'Admin/Attribute/update', '保存数据', -1, ''),
	(172, 'admin', 2, 'Admin/Config/index', '配置管理', -1, ''),
	(173, 'admin', 2, 'Admin/Config/edit', '编辑', -1, ''),
	(174, 'admin', 2, 'Admin/Config/del', '删除', -1, ''),
	(175, 'admin', 2, 'Admin/Config/add', '新增', -1, ''),
	(176, 'admin', 2, 'Admin/Config/save', '保存', -1, ''),
	(177, 'admin', 2, 'Admin/Menu/index', '菜单管理', -1, ''),
	(178, 'admin', 2, 'Admin/Channel/index', '导航管理', -1, ''),
	(179, 'admin', 2, 'Admin/Channel/add', '新增', -1, ''),
	(180, 'admin', 2, 'Admin/Channel/edit', '编辑', -1, ''),
	(181, 'admin', 2, 'Admin/Channel/del', '删除', -1, ''),
	(182, 'admin', 2, 'Admin/Category/index', '分类管理', -1, ''),
	(183, 'admin', 2, 'Admin/Category/edit', '编辑', -1, ''),
	(184, 'admin', 2, 'Admin/Category/add', '新增', -1, ''),
	(185, 'admin', 2, 'Admin/Category/remove', '删除', -1, ''),
	(186, 'admin', 2, 'Admin/Category/move', '移动', -1, ''),
	(187, 'admin', 2, 'Admin/Category/merge', '合并', -1, ''),
	(188, 'admin', 2, 'Admin/Database/index?type=export', '备份数据库', -1, ''),
	(189, 'admin', 2, 'Admin/Database/export', '备份', -1, ''),
	(190, 'admin', 2, 'Admin/Database/optimize', '优化表', -1, ''),
	(191, 'admin', 2, 'Admin/Database/repair', '修复表', -1, ''),
	(192, 'admin', 2, 'Admin/Database/index?type=import', '还原数据库', -1, ''),
	(193, 'admin', 2, 'Admin/Database/import', '恢复', -1, ''),
	(194, 'admin', 2, 'Admin/Database/del', '删除', -1, ''),
	(195, 'admin', 2, 'Admin/other', '其他', 1, ''),
	(196, 'admin', 2, 'Admin/Menu/add', '新增', -1, ''),
	(197, 'admin', 2, 'Admin/Menu/edit', '编辑', -1, ''),
	(198, 'admin', 2, 'Admin/Think/lists?model=article', '应用', -1, ''),
	(199, 'admin', 2, 'Admin/Think/lists?model=download', '下载管理', -1, ''),
	(200, 'admin', 2, 'Admin/Think/lists?model=config', '应用', -1, ''),
	(201, 'admin', 2, 'Admin/Action/actionlog', '行为日志', -1, ''),
	(202, 'admin', 2, 'Admin/User/updatePassword', '修改密码', -1, ''),
	(203, 'admin', 2, 'Admin/User/updateNickname', '修改昵称', -1, ''),
	(204, 'admin', 2, 'Admin/action/edit', '查看行为日志', -1, ''),
	(206, 'admin', 1, 'Admin/think/edit', '编辑数据', 1, ''),
	(207, 'admin', 1, 'Admin/Menu/import', '导入', 1, ''),
	(208, 'admin', 1, 'Admin/Model/generate', '生成', 1, ''),
	(209, 'admin', 1, 'Admin/Addons/addHook', '新增钩子', 1, ''),
	(210, 'admin', 1, 'Admin/Addons/edithook', '编辑钩子', 1, ''),
	(211, 'admin', 1, 'Admin/Article/sort', '文档排序', 1, ''),
	(212, 'admin', 1, 'Admin/Config/sort', '排序', 1, ''),
	(213, 'admin', 1, 'Admin/Menu/sort', '排序', 1, ''),
	(214, 'admin', 1, 'Admin/Channel/sort', '排序', 1, ''),
	(215, 'admin', 1, 'Admin/Category/operate/type/move', '移动', 1, ''),
	(216, 'admin', 1, 'Admin/Category/operate/type/merge', '合并', 1, ''),
	(217, 'admin', 1, 'Admin/Seo/index', '规则管理', 1, ''),
	(218, 'admin', 1, 'Admin/Seo/add', '新增', 1, ''),
	(219, 'admin', 1, 'Admin/Seo/edit', '编辑', 1, ''),
	(220, 'admin', 1, 'Admin/Seo/recycle', '规则回收站', 1, ''),
	(221, 'admin', 1, 'Admin/think/lists', '数据列表', 1, ''),
	(222, 'admin', 1, 'Admin/Test/email', '邮件测试', 1, ''),
	(223, 'admin', 1, 'Admin/Cache/delcache', '清除缓存', 1, ''),
	(224, 'admin', 2, 'Admin/Forum/index', '贴吧', 1, ''),
	(225, 'admin', 1, 'Admin/Forum/forum', '版块管理', 1, ''),
	(226, 'admin', 1, 'Admin/Forum/add', '新增版块', 1, ''),
	(227, 'admin', 1, 'Admin/Forum/edit', '编辑版块', 1, ''),
	(228, 'admin', 1, 'Admin/Forum/sort', '版块排序', 1, ''),
	(229, 'admin', 1, 'Admin/Forum/forumTrash', '版块回收站', 1, ''),
	(230, 'admin', 1, 'Admin/Forum/permit', '还原', 1, ''),
	(231, 'admin', 1, 'Admin/Forum/clear', '清空', 1, ''),
	(232, 'admin', 1, 'Admin/Forum/post', '帖子管理', 1, ''),
	(233, 'admin', 1, 'Admin/Forum/editPost', '编辑帖子', 1, ''),
	(234, 'admin', 1, 'Admin/Forum/postPermit', '还原', 1, ''),
	(235, 'admin', 1, 'Admin/Forum/postClear', '清空', 1, ''),
	(236, 'admin', 1, 'Admin/Forum/reply', '回复管理', 1, ''),
	(237, 'admin', 1, 'Admin/Forum/editReply', '编辑回复', 1, ''),
	(238, 'admin', 1, 'Admin/Forum/replyTrach', '回复回收站', -1, ''),
	(239, 'admin', 1, 'Admin/Forum/replyPermit', '还原', 1, ''),
	(240, 'admin', 1, 'Admin/Forum/replyClear', '清空', 1, ''),
	(241, 'admin', 1, 'Admin/index/index', '首页', 1, ''),
	(242, 'admin', 2, 'Admin/Article/index', '内容', 1, ''),
	(243, 'admin', 1, 'Admin/Seo/sort', '排序', 1, ''),
	(244, 'admin', 1, 'Admin/Forum/sort', '版块排序', -1, ''),
	(245, 'admin', 1, 'Admin/Forum/postTrash', '帖子回收站', 1, ''),
	(246, 'admin', 1, 'Admin/Forum/replyTrash', '回复回收站', 1, '');
/*!40000 ALTER TABLE `lh_auth_rule` ENABLE KEYS */;


-- 导出  表 lehe.lh_avatar 结构
CREATE TABLE IF NOT EXISTS `lh_avatar` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `path` varchar(70) NOT NULL COMMENT '路径',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `status` int(11) NOT NULL COMMENT '数据状态，-1 已删除，0 被禁用，1 正常，2 未审核，3是草稿箱',
  `is_temp` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1是临时,0不是临时,默认是0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='头像表';

-- 正在导出表  lehe.lh_avatar 的数据：~2 rows (大约)
DELETE FROM `lh_avatar`;
/*!40000 ALTER TABLE `lh_avatar` DISABLE KEYS */;
INSERT INTO `lh_avatar` (`id`, `uid`, `path`, `create_time`, `status`, `is_temp`) VALUES
	(9, 1, '2014-10-15/543e39548fa9d-05505543.jpg', 1413364054, 1, 0),
	(10, 1, '', 1413364054, 1, 1);
/*!40000 ALTER TABLE `lh_avatar` ENABLE KEYS */;


-- 导出  表 lehe.lh_category 结构
CREATE TABLE IF NOT EXISTS `lh_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(30) NOT NULL COMMENT '标志',
  `title` varchar(50) NOT NULL COMMENT '标题',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每页行数',
  `meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的网页标题',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `template_index` varchar(100) NOT NULL COMMENT '频道页模板',
  `template_lists` varchar(100) NOT NULL COMMENT '列表页模板',
  `template_detail` varchar(100) NOT NULL COMMENT '详情页模板',
  `template_edit` varchar(100) NOT NULL COMMENT '编辑页模板',
  `model` varchar(100) NOT NULL DEFAULT '' COMMENT '列表绑定模型',
  `model_sub` varchar(100) NOT NULL DEFAULT '' COMMENT '子文档绑定模型',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '允许发布的内容类型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
  `allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许发布内容',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '可见性',
  `reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许回复',
  `check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '发布的文章是否需要审核',
  `reply_model` varchar(100) NOT NULL DEFAULT '',
  `extend` text NOT NULL COMMENT '扩展设置',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态',
  `icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类图标',
  `groups` varchar(255) NOT NULL DEFAULT '' COMMENT '分组定义',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- 正在导出表  lehe.lh_category 的数据：13 rows
DELETE FROM `lh_category`;
/*!40000 ALTER TABLE `lh_category` DISABLE KEYS */;
INSERT INTO `lh_category` (`id`, `name`, `title`, `pid`, `sort`, `list_row`, `meta_title`, `keywords`, `description`, `template_index`, `template_lists`, `template_detail`, `template_edit`, `model`, `model_sub`, `type`, `link_id`, `allow_publish`, `display`, `reply`, `check`, `reply_model`, `extend`, `create_time`, `update_time`, `status`, `icon`, `groups`) VALUES
	(1, 'index', '首页', 0, 1, 10, '', '', '', '', '', '', '', '2,3', '2', '2,1,3', 0, 0, 1, 0, 0, '1', '', 1379474947, 1407163981, 1, 0, ''),
	(5, 'tech', '科技前沿', 1, 5, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '1', '', 1379475028, 1408874187, 0, 0, ''),
	(2, 'city', '社会聚焦', 1, 2, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161711, 1408874092, 0, 0, ''),
	(3, 'war', '军事观察', 1, 3, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161748, 1408874177, 0, 0, ''),
	(4, 'education', '教育', 1, 4, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161768, 1407221081, 0, 0, ''),
	(6, 'photo', '手机', 1, 6, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161787, 1407226766, 0, 0, ''),
	(7, 'pad', '平板', 1, 7, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161807, 1407228048, 0, 0, ''),
	(8, 'pc', '电脑', 1, 8, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161826, 1407221100, 0, 0, ''),
	(9, 'joke', '要你好笑', 1, 9, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161841, 1408874124, 0, 0, ''),
	(10, 'ana', '暖心话语', 1, 10, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161860, 1408874146, 0, 0, ''),
	(11, 'bulletin', '本站公告', 1, 11, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161878, 1407221133, 0, 0, ''),
	(12, 'about', '关于本站', 1, 12, 10, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1407161895, 1407221141, 0, 0, ''),
	(13, 'funthing', '有趣的事', 1, 0, 20, '', '', '', '', '', '', '', '2', '2', '2,1,3', 0, 1, 1, 0, 0, '', '', 1413274276, 1413274276, 1, 0, '');
/*!40000 ALTER TABLE `lh_category` ENABLE KEYS */;


-- 导出  表 lehe.lh_channel 结构
CREATE TABLE IF NOT EXISTS `lh_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '频道ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级频道ID',
  `title` char(30) NOT NULL COMMENT '频道标题',
  `name` char(30) NOT NULL COMMENT '标识',
  `url` char(100) NOT NULL COMMENT '频道连接',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '导航排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `target` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '新窗口打开',
  `style_css` varchar(50) NOT NULL DEFAULT 'red' COMMENT '页面样式',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_channel 的数据：13 rows
DELETE FROM `lh_channel`;
/*!40000 ALTER TABLE `lh_channel` DISABLE KEYS */;
INSERT INTO `lh_channel` (`id`, `pid`, `title`, `name`, `url`, `sort`, `create_time`, `update_time`, `status`, `target`, `style_css`) VALUES
	(1, 0, '首页', 'index', 'Index/index', 1, 1379475111, 1407162263, 0, 0, 'red'),
	(2, 0, '社会聚焦', 'city', 'Article/index?category=city', 4, 1379475131, 1408876681, 0, 0, 'blue'),
	(3, 0, '军事观察', 'war', 'Article/index?category=war', 5, 1379475154, 1408876716, 0, 0, 'green'),
	(4, 0, '教育', 'education', 'Article/index?category=education', 7, 1407162068, 1408876743, 0, 0, 'orange'),
	(5, 0, '科技前沿', 'tech', 'Article/index?category=tech', 6, 1407162090, 1408876729, 0, 0, 'gray'),
	(6, 0, '手机', 'photo', 'Article/index?category=photo', 8, 1407162109, 1408876782, 0, 0, 'turquoise'),
	(7, 0, '电脑', 'pc', 'Article/index?category=pc', 10, 1407162128, 1408876857, 0, 0, 'blue'),
	(8, 0, '要你好笑', 'joke', 'Article/index?category=joke', 2, 1407162164, 1408876982, 0, 0, 'orange'),
	(9, 0, '暖心话语', 'ana', 'Article/index?category=ana', 3, 1407162216, 1408876660, 0, 0, 'turquoise'),
	(10, 0, '本站公告', 'bulletin', 'Article/index?category=bulletin', 11, 1407162238, 1408876645, 0, 0, 'red'),
	(11, 0, '关于本站', 'about', 'Article/index?category=about', 12, 1407162255, 1407223725, 0, 0, 'turquoise'),
	(12, 0, '平板', 'pad', 'Article/index?category=pad', 9, 1407163886, 1408876805, 0, 0, 'red'),
	(13, 0, '有趣的事', 'funthing', 'Article/index?category=funthing', 13, 1407163886, 1408876805, 0, 0, 'red');
/*!40000 ALTER TABLE `lh_channel` ENABLE KEYS */;


-- 导出  表 lehe.lh_config 结构
CREATE TABLE IF NOT EXISTS `lh_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置说明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分组',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_config 的数据：35 rows
DELETE FROM `lh_config`;
/*!40000 ALTER TABLE `lh_config` DISABLE KEYS */;
INSERT INTO `lh_config` (`id`, `name`, `type`, `title`, `group`, `extra`, `remark`, `create_time`, `update_time`, `status`, `value`, `sort`) VALUES
	(1, 'WEB_SITE_TITLE', 1, '网站标题', 1, '', '网站标题前台显示标题', 1378898976, 1407286337, 1, '我们一起乐呵', 0),
	(2, 'WEB_SITE_DESCRIPTION', 2, '网站描述', 1, '', '网站搜索引擎描述', 1378898976, 1379235841, 1, '我们一起乐呵', 1),
	(3, 'WEB_SITE_KEYWORD', 2, '网站关键字', 1, '', '网站搜索引擎关键字', 1378898976, 1381390100, 1, '我们一起乐呵', 8),
	(4, 'WEB_SITE_CLOSE', 4, '关闭站点', 1, '0:关闭,1:开启', '站点关闭后其他用户不能访问，管理员可以正常访问', 1378898976, 1379235296, 1, '1', 1),
	(9, 'CONFIG_TYPE_LIST', 3, '配置类型列表', 4, '', '主要用于数据解析和页面表单的生成', 1378898976, 1379235348, 1, '0:数字\r\n1:字符\r\n2:文本\r\n3:数组\r\n4:枚举', 2),
	(10, 'WEB_SITE_ICP', 1, '网站备案号', 1, '', '设置在网站底部显示的备案号，如“沪ICP备12007941号-2', 1378900335, 1379235859, 1, '浙ICP备14025451号-1', 9),
	(11, 'DOCUMENT_POSITION', 3, '文档推荐位', 2, '', '文档推荐位，推荐到多个位置KEY值相加即可', 1379053380, 1379235329, 1, '1:列表推荐\r\n2:频道推荐\r\n4:首页推荐', 3),
	(12, 'DOCUMENT_DISPLAY', 3, '文档可见性', 2, '', '文章可见性仅影响前台显示，后台不收影响', 1379056370, 1379235322, 1, '0:所有人可见\r\n1:仅注册会员可见\r\n2:仅管理员可见', 4),
	(13, 'COLOR_STYLE', 4, '后台色系', 1, 'default_color:默认\r\nblue_color:紫罗兰', '后台颜色风格', 1379122533, 1379235904, -1, 'default_color', 10),
	(20, 'CONFIG_GROUP_LIST', 3, '配置分组', 4, '', '配置分组', 1379228036, 1384418383, 1, '1:基本\r\n2:内容\r\n3:用户\r\n4:系统\r\n5:邮件', 4),
	(21, 'HOOKS_TYPE', 3, '钩子的类型', 4, '', '类型 1-用于扩展显示内容，2-用于扩展业务处理', 1379313397, 1379313407, 1, '1:视图\r\n2:控制器', 6),
	(22, 'AUTH_CONFIG', 3, 'Auth配置', 4, '', '自定义Auth.class.php类配置', 1379409310, 1379409564, 1, 'AUTH_ON:1\r\nAUTH_TYPE:2', 8),
	(23, 'OPEN_DRAFTBOX', 4, '是否开启草稿功能', 2, '0:关闭草稿功能\r\n1:开启草稿功能\r\n', '新增文章时的草稿功能配置', 1379484332, 1379484591, 1, '1', 1),
	(24, 'DRAFT_AOTOSAVE_INTERVAL', 0, '自动保存草稿时间', 2, '', '自动保存草稿的时间间隔，单位：秒', 1379484574, 1386143323, 1, '120', 2),
	(25, 'LIST_ROWS', 0, '后台每页记录数', 2, '', '后台数据每页显示记录数', 1379503896, 1380427745, 1, '25', 10),
	(26, 'USER_ALLOW_REGISTER', 4, '是否允许用户注册', 3, '0:关闭注册\r\n1:允许注册', '是否开放用户注册', 1379504487, 1379504580, 1, '1', 3),
	(27, 'CODEMIRROR_THEME', 4, '预览插件的CodeMirror主题', 4, '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '详情见CodeMirror官网', 1379814385, 1384740813, 1, 'ambiance', 3),
	(28, 'DATA_BACKUP_PATH', 1, '数据库备份根路径', 4, '', '路径必须以 / 结尾', 1381482411, 1381482411, 1, './Data/', 5),
	(29, 'DATA_BACKUP_PART_SIZE', 0, '数据库备份卷大小', 4, '', '该值用于限制压缩后的分卷最大长度。单位：B；建议设置20M', 1381482488, 1381729564, 1, '20971520', 7),
	(30, 'DATA_BACKUP_COMPRESS', 4, '数据库备份文件是否启用压缩', 4, '0:不压缩\r\n1:启用压缩', '压缩备份文件需要PHP环境支持gzopen,gzwrite函数', 1381713345, 1381729544, 1, '1', 9),
	(31, 'DATA_BACKUP_COMPRESS_LEVEL', 4, '数据库备份文件压缩级别', 4, '1:普通\r\n4:一般\r\n9:最高', '数据库备份文件的压缩级别，该配置在开启压缩时生效', 1381713408, 1381713408, 1, '9', 10),
	(32, 'DEVELOP_MODE', 4, '开启开发者模式', 4, '0:关闭\r\n1:开启', '是否开启开发者模式', 1383105995, 1383291877, 1, '1', 11),
	(33, 'ALLOW_VISIT', 3, '不受限控制器方法', 0, '', '', 1386644047, 1386644741, 1, '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', 0),
	(34, 'DENY_VISIT', 3, '超管专限控制器方法', 0, '', '仅超级管理员可访问的控制器方法', 1386644141, 1386644659, 1, '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', 0),
	(35, 'REPLY_LIST_ROWS', 0, '回复列表每页条数', 2, '', '', 1386645376, 1387178083, 1, '10', 0),
	(36, 'ADMIN_ALLOW_IP', 2, '后台允许访问IP', 4, '', '多个用逗号分隔，如果不配置表示不限制IP访问', 1387165454, 1387165553, 1, '', 12),
	(37, 'SHOW_PAGE_TRACE', 4, '是否显示页面Trace', 4, '0:关闭\r\n1:开启', '是否显示页面Trace信息', 1387165685, 1387165685, 1, '1', 1),
	(38, 'WEB_SITE_AUTHOR', 2, '网站作者', 1, '', '网站作者', 1407286220, 1407286220, 1, '二阳', 0),
	(39, 'MAIL_TYPE', 4, '邮件类型', 5, '1:SMTP 模块发送\r\n2:mail() 函数发送', '如果您选择了采用服务器内置的 Mail 服务，您不需要填写下面的内容', 1408953490, 1408953490, 1, '1', 0),
	(40, 'MAIL_SMTP_HOST', 1, 'SMTP服务器', 5, '', 'SMTP服务器', 1408953556, 1408953556, 1, 'smtp.163.com', 0),
	(41, 'MAIL_SMTP_PORT', 0, 'SMTP服务器端口', 5, '', '默认25', 1408953586, 1408953586, 1, '25', 0),
	(42, 'MAIL_SMTP_USER', 1, 'SMTP服务器用户名', 5, '', '填写完整用户名', 1408953649, 1408953649, 1, 'shangshanshishui@163.com', 0),
	(43, 'MAIL_SMTP_PASS', 1, 'SMTP服务器密码', 5, '', '填写您的密码', 1408953697, 1408953697, 1, 'anhui99', 0),
	(44, 'MAIL_SMTP_CE', 1, '邮件发送测试', 5, '', '填写测试邮件地址', 1408953737, 1408953737, 1, '707069100@qq.com', 0),
	(45, 'AFTER_LOGIN_JUMP_URL', 2, '登陆后跳转的Url', 1, '', '支持形如home/index/index的ThinkPhp路由写法，支持普通的url写法', 1409726536, 1409727160, 1, '/index', 7);
/*!40000 ALTER TABLE `lh_config` ENABLE KEYS */;


-- 导出  表 lehe.lh_document 结构
CREATE TABLE IF NOT EXISTS `lh_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `name` char(40) NOT NULL DEFAULT '' COMMENT '标识',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '标题',
  `category_id` int(10) unsigned NOT NULL COMMENT '所属分类',
  `group_id` smallint(3) unsigned NOT NULL COMMENT '所属分组',
  `description` text NOT NULL COMMENT '描述',
  `source_name` varchar(50) NOT NULL COMMENT '文档原文网站名称',
  `source_url` varchar(255) NOT NULL COMMENT '文档原文网站url',
  `model_id` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容模型ID',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '推荐位',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可见性',
  `attach` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件数量',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论数',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '优先级',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态，-1 已删除，0 被禁用，1 正常，2 未审核，3是草稿箱',
  `root` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '根节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '内容类型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至时间',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '扩展统计字段',
  PRIMARY KEY (`id`),
  KEY `idx_category_status` (`category_id`,`status`),
  KEY `idx_status_type_pid` (`status`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='文档模型基础表';

-- 正在导出表  lehe.lh_document 的数据：9 rows
DELETE FROM `lh_document`;
/*!40000 ALTER TABLE `lh_document` DISABLE KEYS */;
INSERT INTO `lh_document` (`id`, `uid`, `name`, `title`, `category_id`, `group_id`, `description`, `source_name`, `source_url`, `model_id`, `position`, `display`, `attach`, `view`, `comment`, `cover_id`, `level`, `create_time`, `update_time`, `status`, `root`, `pid`, `type`, `link_id`, `deadline`, `extend`) VALUES
	(1, 5, '', '你不是一无所有 起码你还有脸面对', 13, 0, '', '', '', 2, 0, 1, 0, 0, 0, 0, 0, 1413275402, 1413275402, -1, 0, 0, 2, 0, 0, 0),
	(2, 5, '', '你不是一无所有 起码你还有脸面对', 13, 0, '　　生活，不是在等暴风雨过去，而是在暴风雨中学会跳舞。　　人，永远是矛盾的主体，经常处在犹豫和憧憬的困惑中，夹在世俗的单行道上，走不远也回不去。人，真的是一个难以琢磨的生灵，最了解自己的永远只有自己。...', '', '', 2, 0, 1, 0, 1, 0, 0, 0, 1413276388, 1413276388, -1, 0, 0, 2, 0, 0, 0),
	(3, 5, '', '骚年,你不是一无所有 起码你还有脸面对', 13, 0, '　　生活，不是在等暴风雨过去，而是在暴风雨中学会跳舞。　　人，永远是矛盾的主体，经常处在犹豫和憧憬的困惑中，夹在世俗的单行道上，走不远也回不去。人，真的是一个难以琢磨的生灵，最了解自己的永远只有自己。...', '笑多了不怀孕', '', 2, 0, 1, 0, 62, 0, 1, 0, 1413252840, 1414635338, 1, 0, 0, 2, 0, 0, 0),
	(4, 5, '', '为了自己不胖 请远离胖朋友', 13, 0, '在这年头，交个朋友也要看胖瘦，因为交了胖朋友，自己也会变胖。还能愉快的玩耍吗？', '', '', 2, 0, 1, 0, 44, 0, 2, 0, 1413302400, 1413353661, 1, 0, 0, 2, 0, 0, 0),
	(5, 7, '', '我 就要与众不同', 13, 0, '12年前，学校里很多人没手机的时候，我手里拿着“高端”NOKIA，那时我亮了。12年后，老同学聚会，人人手里拿着IPHONE，这时我拿出NOKIA，我又亮了。', '', '', 2, 0, 1, 0, 51, 0, 3, 0, 1413388800, 1413424676, 1, 0, 0, 2, 0, 0, 0),
	(6, 7, '', '人哪 怎样才能做到不在乎别人骂', 13, 0, '一个三岁的小男孩拉着一个三岁的小女孩的手说：“我爱你。”小女孩说：“你能为我的未来负责吗？”小男孩说：“当然能！我们都不是一两岁的人了！”', '', '', 2, 0, 1, 0, 56, 0, 4, 0, 1413507682, 1413507682, 1, 0, 0, 2, 0, 0, 0),
	(7, 7, '', '每天醒来 面朝阳光', 13, 0, '每天醒来，面朝阳光，努力向上，相信日子会变得单纯而美好。', '', '', 2, 0, 1, 0, 14, 0, 5, 0, 1414457578, 1414457578, 1, 0, 0, 2, 0, 0, 0),
	(8, 7, '', '下辈子你住我家隔壁吧 与我青梅竹马，两小无猜', 13, 0, '早上接到一电话：“先生您好，我是xx公司的…能打扰您一分钟吗？”我果断回到：“不行，最少十分钟，我已经很久没人打电话聊天了…”说多了都是泪！', '', '', 2, 0, 1, 0, 8, 0, 6, 0, 1414512000, 1414573602, 1, 0, 0, 2, 0, 0, 0),
	(9, 7, '', '我愿以朋友的名义陪在你身边，静静地看着你是怎么嫁不出去的', 13, 0, '妈知道我谈恋爱了，百般要挟下我不得不把男友的照片给她看。妈看着照片说：“你确定没骗我，这是你男友？长得这么帅怎么可能成为你男友。”我听后就不高兴：“妈，你女儿也不差啊！”妈说：“我知道你不差，我不是说你配不上他，我的意思是他没有男朋友？”妈，你是不是玩微博了？！', '', '', 2, 0, 1, 0, 21, 0, 7, 0, 1414598400, 1414630204, 1, 0, 0, 2, 0, 0, 0);
/*!40000 ALTER TABLE `lh_document` ENABLE KEYS */;


-- 导出  表 lehe.lh_document_article 结构
CREATE TABLE IF NOT EXISTS `lh_document_article` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容解析类型',
  `content` text NOT NULL COMMENT '文章内容',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '详情页显示模板',
  `bookmark` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '收藏数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档模型文章表';

-- 正在导出表  lehe.lh_document_article 的数据：9 rows
DELETE FROM `lh_document_article`;
/*!40000 ALTER TABLE `lh_document_article` DISABLE KEYS */;
INSERT INTO `lh_document_article` (`id`, `parse`, `content`, `template`, `bookmark`) VALUES
	(1, 0, '', '', 0),
	(2, 0, '<p style="text-indent:2em;">　　生活，不是在等暴风雨过去，而是在暴风雨中学会跳舞。</p><br/><p style="text-indent:2em;"><br />　　人，永远是矛盾的主体，经常处在犹豫和憧憬的困惑中，夹在世俗的单行道上，走不远也回不去。人，真的是一个难以琢磨的生灵，最了解自己的永远只有自己。人生在世都不容易，懂得珍惜该珍惜的一切，懂得把握应该把握的一切，懂得享受该享受的一切。让自己这辈子，过得充实、快乐！</p><br/><p style="text-indent:2em;"><br /><strong>　　</strong><strong>【每日新闻趣评】</strong></p><br/><p style="text-indent:2em;">　　【爽歪歪被曝风干变乳胶】</p><br/><p style="text-indent:2em;">　　近日，一条“知名饮品爽歪歪风干后可以做避孕套”的帖子在朋友圈流传，内称爽歪歪营养酸奶风干后会变成乳胶状的物质，会影响孩子的食欲，影响健康。对此，多位专家表示，网友所述的乳胶状物质为乳饮料中都有的增稠剂，相比增稠剂，家长更应关注乳饮料中大量的糖对孩子身体潜在的危害。（新浪网）</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　逗哥随口一说：为人父母者需关注此类信息，爽歪歪、AD钙奶等风干后成透明“胶膜”，有网友淘气了：可以做避孕套使用！哈哈哈哈！我想说的是：妈妈以后再也不用担心我会意外造人了！噢耶！</p><br/><p style="text-indent:2em;">　　【北京地铁调价听证方案出炉】</p><br/><p style="text-indent:2em;">　　北京发改委上午将发布公交票制票价调整听证方案，其中，地面公交和轨道交通均实行计程值并按递远递减原则加价。地铁起步价分别为两元和三元，乘客均担分别为4.3或4.4元，同时对通勤族优惠。地面公交调价后仍是全国同类城市最低。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　逗哥随口一说：平均乘坐下来均摊费用分别为4.3或4.4元，看来从单程2元的价格直降飙升5元以上了，虽说同时对通勤族优惠，但是通勤族应该会用通勤卡来进行界定，那么贩吠ㄇ谧寤嵘栌薪朊偶鳎畹拖押蜕昵氡曜伎ǎ纷佑钟星耍』岵换嵊谢Э谙拗疲遣皇潜匦氲帽本┗Э冢岵换嵊掷锤鲈诰┙赡?年社保才能申请，会不会要摇号住在内城，年龄低于35，硕士以上，党员优先是不是当然的啊~~哎，想到这里整个人都不好了。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　若只是对比票面的价格，北京公交地铁价格确实比全球其他大城市低得多。粗略比较可以发现，北京市的地铁票价堪称全球最低票价之一。这一低票价给了不少在北京高房价、高物价中挣扎的“北漂”们慰藉。但“砖家”们表示，长期对公共交通实行“低票价、高补贴”的做法是不明智的，因为纳税人所缴的税款应该覆盖百姓生活的方方面面滴。</p><br/><p style="text-indent:2em;">　　【女子疑被当小三遭当街扒衣群殴】</p><br/><p style="text-indent:2em;">　　疑为原配女子率众群殴小三，当街扒光致其三点全露。10月10日上午，在濮阳县老城发生的这一幕劲爆场面，被网友上传到了微博、论坛上，内容包含了多张未进行处理的全裸照片，引发众多网友热议。人们在转发时，均未对照片中“小三”的女性裸露部位进行遮挡处理，相关网站管理方也一路绿灯。（人民网）</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　逗哥随口一说：告诉我逗哥你不是为了围观看图而来的！！！</p><br/><p style="text-indent:2em;">　　你们如果不是用批判眼光看过来的，说明你邪恶了！你坏透了囖～！</p><br/><p style="text-indent:2em;">　　愤怒，愤怒，严重侵害人权，公共场所当众污辱妇女，必须严惩，此气焰不可长啊！男人正直何来小三！打了小三还有小四、小五、管好自己老公最重要，实在管不住就离婚，要是不理干脆把XX给咔嚓了，要么忍要么狠贩房瓤龋舛挝淖挚梢院雎浴２还苁遣皇恰靶∪保耆枧宰镆丫闪ⅲ荒芑煳惶浮Ｎ蘼廴绾斡σ允胤ㄗ鸱ㄎ嚷铮?/p><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　为什么出轨的男人没事？净挑软柿子捏？有本事就告上法庭把你老公的家产都抢过来，再将他踢出家门，看看他和小三能腻多久。女人何苦为难女人呢，为什么不能有气节点？！</p><br/><p style="text-indent:2em;">　　老公找小三不论你怎么哭闹，没用的，就是打死小三也没用，还得搭上自己，孩子就毁了，现在这个社会太多了，所以倒不如活的漂漂亮亮的，把婚离了，随他怎么过，做一个优雅的女人更快乐，不是吗？</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　【广西“暴力剪发”校长：学生哈日哈韩不能不管】 </p><br/><p style="text-indent:2em;">　　10月10日，广西梧州藤县濛江一中一黄姓副校长被曝光“暴力剪发”：几十名长发男生集合到校内的空地上，并广播让其他师生前来围观他对部分长发男生强行理发。 </p><br/><p style="text-indent:2em;">　　对于“暴力剪发”，当事人则在先前的媒体回应中称，剪发事件说明学校对学生负责。他还称“我这个副校长当不当都无所谓，但是对学生肯定要负责任。”该副校长此举引发学生在网络上吐槽，广西师范大学教授谢晖指出，当事人的做法不尊重基本人格，不利于学生的成长。 </p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　逗哥随口一说：事发后，此事经媒体报道后，引发了网友热议，面对该副校长的“暴力剪发”，质疑其方法不当者也颇多，不过，也有人表示支持。</p><br/><p style="text-indent:2em;">　　管不严父之过，教不严师之堕！有些学生的确不好管，青春期的小孩，就喜欢表面上的东西，还以为自己什么都懂，回想自己那个时候多数都是就这样，现在回头看看，都觉得自己可笑。家长老师有时候对孩子适当的严厉不是坏事，树苗要育，才能直。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　不以规矩，无以成方圆。但是可是，您的两剪刀“真的”能把所谓的坏的都给剪好么？当今，对学生、对家长、对社会负责的老师需要受到尊重！但是坏学生也需要被尊重的权利，“暴力”执法真的就给扶正捋直了么？！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　教育是为了传播知识还是为了人的发展？知识可以量化速成，但是育人之德则不能速成走捷径。都说十年树木百年树人，若不在德育方面多下些功夫，岂是为人师长者所不作为！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"><br /></p><br/><p style="text-indent:2em;"><strong>　　【每日逗比精选】</strong></p><br/><p style="text-indent:2em;">　　在外拼三年，一无所有的回到家。本以为妈妈会大发雷霆。可没想到妈妈没有骂我，还安慰我：“孩子，你并不是一无所有，最起码你还有脸回来。”</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　童鞋们对号入错吧，我是男屌.........丝.！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　刚看到了一条围脖.个人觉得是很浪漫的表白喇..如果我这样跟一位女生说..不知道她会有什么反应呢?</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　小家伙为了吃口奶也真是蛮拼的！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　这是哪家的熊孩子？还有没有人管管了？</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　火锅要和一群不熟的人吃，烤串要和最好的朋友吃，饺子和爱人亲人吃。而面条，一碗热汤面，只在孤单一人的时候最好吃。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　据说每一个女生的房间里都有一把这样的椅子。。。其实男生也有！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　有一种定格叫做“喷嚏好久才打出来”。。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　心理学上说：女人大多数只对有安全度的人发脾气。因为在那个安全度之内。潜意识知道对方不会离开你。胡闹是一种依赖。你的女票是酱紫么~！？</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　【逗哥带你涨姿势】男生别说逗哥偏向了，这期给你男淫们支招了！这些应该都是女生最需要的点，男生记牢了。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　你身边有没有那种自己长的很丑，天天还在朋友圈，微博里晒各种自我感觉良好的自拍照，最无法理解的是竟然还有人评论一句，美女啊！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　头大真是世界级硬伤，整容都整不了，头大导致脸大，脸大导致脖子短，显胖，看起来身体比例不好，明明身体是八九头身，因为头大变成了五六头身。头大太可怜了，想组成一个头大者联盟，给予彼此温暖，互相欣赏慰籍。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　钱包里一定要放你最爱的人，我感觉我的情敌已经多到全国都是了！逗哥钱包上镜～～了！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　骆驼拉车，50一位。这真的是骆驼吗？我读书少，你不要骗我。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　和老婆同床时的地理划分......</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　如果有人问起，你怎么变胖了，你就说忘了。 不要解释，越解释越悲伤。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　【一大波萌宠想你狂奔！】开饭啦！！一组吃货开饭时各种反应的动图！简直萌哭了哈哈哈哈哈，最后那几只贩?/p><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　昨天一家4口外面吃饭，结帐255块，我自言自语：霍，差点就250了，找老板娘要了发票一刮奖，我擦，中了5块！！！老板娘碎碎念了一句，这回真是250了。</p><br/><p style="text-indent:2em;">　　冬天快到了，大家都要注意保暖，毕竟单身狗需要学会自强。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　如何拒绝女神表白!</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　现在100块钱真心不算什么了！早上带着红色毛爷爷出门，手机欠费充值，他变绿了；吃了顿饭，他变黄了；买了本杂志，他变蓝了；再买个煎饼果子，他变紫了；坐个地铁然后转公交，他只剩菊花了；最后买了一个棒棒糖，菊花也爆没了。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　日本老大爷冈田健一桑退休后在家赋闲，为哄两个孙子开心，老人家用木材自制了高达“百式”模型，细心涂装，完美还原了高达的金属质感。令人遗憾的是，两个孙子都表示不太喜欢这个模型.而我看到的是蓝天 草地！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　【奇图大观】高手，这图拍的！！欢迎无极限，配上字幕，简直笑死人不偿命！哈哈！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　辣子鸡的“别名”，你知道么？</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">小螃蟹教你们，不管敌人多么强大我们也不能轻言放弃！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"> </p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　真想一巴掌拍死脑袋里面的那些胡思乱想</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　每个人都会有一段异常艰难的时光，生活的窘迫，工作的失意，学业的压力，爱的惶惶不可终日。挺过来的，人生就会豁然开朗；挺不过来的，时间也会教会你怎么与它们握手言和，所以你都不必害怕的。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　一张图告诉你与土豪的差别。。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　延时摄影让你感受到地球的自转，感受到漫长的黑夜变短，让你感受到斗转星移、云卷云舒的奇妙！可以在30秒内带你领略浩瀚星轨！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　上大学之前，以为男生到180是特平常的事，现在才知道，男生上175都是上辈子拯救了全宇宙……</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　处女座怎么出洗手间……</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　狗狗，你的表情能再蠢一点吗。。。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　这是哪位高人画的啊，吊炸天！跪了跪了~</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　感觉胖子已经无法在这个星球生存了…拜拜!</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　有时候特别想谈恋爱, 但一想到, “我脸上皮肤不好,ta要是亲我就会看到了,ta 抱着我腰要发现我隐藏的肥肉了, 一起吃饭就知道我饭量像三个汉子了, 平时捧个手机刷微博哈哈哈哈卧槽的笑他会觉得我是神经病吧, ”我就觉得还是单身好。不过想着我有这么多毛病根本就找不到对象,也就释然了。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　你们都不要拦住我，我要去迪拜看看人家的喷泉.</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　当魂斗罗遇到俄罗斯方块时。。。 看一次笑一次！ 哈哈哈哈</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　今天地理课，一女生不听讲，在底下化妆。。老师走到面前问：“你能用两个地名描述一下你的脸吗？” 女生怎么想也想不出来，于是问他是什么。老师答曰：“大连 ，太原。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　国内很少见到这样的街头艺术，艺术家David Zinn的粉笔画，有爱，萌萌哒！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　每次上完马桶擦屁股的时候感觉自己就像是蜘蛛侠。</p><br/> <p style="text-indent:2em;"> </p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　我猜到了开头却没猜到这结局...汪星人被吓尿!</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　这位护士你下班别走，我保证不打你！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　一次男友在火车上,给对面的小盆友表演魔术,拿出一盒纸牌,抽出红桃A,扔出窗外,再拿出准备好的一张红桃A。小朋友一个劲地鼓掌,向他走来,他以为小盆友要对他大佳赞赏，没想到小盆友却把他桌上的手表扔出窗外,说:“哥哥，再表演一个。”</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　一则富含哲理的漫画：努力总是有回报的。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　【毛巾兔折叠法】萌萌哒兔纸 ~你学会了吗？兔子</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　如何用最快的速度画出一只玩具熊！！！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　两只捉迷藏的喵星人，小喵的智商还是略胜一筹!</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　女生在生气时放出的狠话，大多数都是假的。可男生基本上都信以为真，心惊肉跳！女生在撒娇时提出的要求基本都是真的，可男生基本上都觉得是个玩笑。这种行为就叫做找抽！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　老天是公平的，虽然你长的丑，但是你想的美啊。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　对于这样的主人，已不想再爱，呵呵，你开心就好！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　朋友圈十大奇葩，你有木有躺着也中枪。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　当两个人在给对方买东西的时候，都不会心疼钱大手大脚，到了为自己花钱却总要犹豫半天反复思量，这个时候差不多就可以结婚了吧！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　“嗨！老乡，你们去哪儿？” “肯德基，你呢？” “哦，我去全聚德。” 今日一别，再见已是来生！泪</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　卧槽槽槽槽槽槽槽槽槽..... ！！！！！no zuo no die!</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　高考结果应该现在都尘埃落定了，很多人在读着大学，有些人开始了走进社会生活，我今天作为一个长者告诉你们一些做人的道理，考的好的同学，一定要记得请考的不好的同学吃个饭，等你们毕业出来搬砖的时候，人家可能早混成工头了。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　作为一只癞皮狗，告诉你，老子说不放，就不放…… 哈哈</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　专门治疗那些手机不离手的病人。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　今天路上看见前面一女汉子在打电话谈分手，听她说：不就分手嘛，有什么了不起的，嘻嘻哈哈的说改天咱两吃个分手饭呗……心想这女的真洒脱，霸气！走快点去前面看看长什么样，然后我就看到她满脸泪水…….</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　这狗居然有洁癖，撒尿怕弄到自己脚上，所以用倒立高难度动作，但是就不怕弄到嘴里？</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　小学辍学的同学，现资产上亿，旗下3家5星级宾馆；初中辍学的同学，现资产上千万，旗下2个特种车队；高中辍学的同学，现存款上百万，每月上班时间不超过10天；大学肄业的同学，现有房有车，工作稳定；大学毕业的我，现在小学辍学的同学单位加班，无双休；仅几万元积蓄，而读研的同学，现正在找工作。。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　你帮人100分，当有一天你只肯帮80了，他便会清空你所有的恩，宁愿选择只帮他70分的人做朋友。一粒米养恩人，一石米养仇人，老人说的话没错。不要动不动就倾其所有，留一些骄傲与心疼给自己，记得了，最暖不过人心，最凉不过人心。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　饭，我所欲也；瘦，亦我所欲也。二者不可兼得，我嘞个去也！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　推主“napalmthing”出门玩耍，突然发现前面路人的打扮很眼熟……</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　当对方拒绝你的时候，不要总是打破砂锅问到底，免得伤了自己那颗脆弱的小心脏！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　你们憋逼我吃药，我要一直蠢蠢哒！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　和同事在酒吧喝酒，隔壁桌的美女不断的挑逗我，正准备过去搭讪，突然想起含辛茹苦的老婆还在家中为我留了一盏灯等我回家，心里实在过意不去。于是我立即走出酒吧，在门口找到一小孩对他说：“给你十块钱，你去把XX小区3栋602的电闸给我拉了。”</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　南加州Chiweenie Farms草泥马农场，因为快要变冷了，农场里的工作人员怕小羊驼冻着，给它们穿上了小朋友的衣服。。有点萌！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　别去试人心，它会让你失望。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　美国版的“三个代表”！↓</p><br/><p style="text-indent:2em;">　　其实美国选择白头鹰作为国家形象，本身就代表了种族歧视：头部代表白人负责统治，身体代表黑人负责干活，嘴部代表黄皮白心的中国公知负责嘴炮……</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　论单身狗的自我安慰，事实向你证明单身也有很多好处。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　骚年你如此妖娆，你家里人知道吗？！哈哈第5个，绝了！！！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　假如你有了上电视的机会，你会怎样把握这短暂的几十秒钟？机会只给有准备的人！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　这样一来，漏“沟”的问题就得到了完美的解决，直接给跪了。。。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　国外一位网友买了只森林之王，到货的时候。。。他炸了！这霸气忧郁的眼神！~</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　OH！马勒个臀！你个贱崽子就是这么对待人的啊~~！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　你们说，周董要是知道了，会不会吐血三斤。。。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　　没我在的日子里，你要好好照顾自己，记得按时喝酒，不舒服多抽烟，多熬夜，早饭不要经常吃，天气冷穿凉鞋多穿对袜子，没事多玩手机，看书记得关灯，过马路的时候记得不要看红绿灯，如果睡不着要多吃安眠药，无聊就烧烧头发，洗澡一定要用沸水，难过了就吸吸毒，一切都会变好的。。。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　本想把日子过成诗，时而简单，时而精致。不料日子却过成了我的歌，时而不靠谱，时而不着调。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　深呼吸，累了一天了，思考下下人生吧，30岁前的这几年，累呢~是一定的！但相信人生不可能就止于此了的。不想变成街上一抓一把的庸人，不想以后为钱发愁，不想以后每天做的都是不喜欢却必须做的事，不想成为那种人。活着么，奏得有梦想，有方向，所以要努力。只有坚持这阵子，才不会辛苦一辈子！</p><br/><p style="text-indent:2em;">　　又到每年这个时候了：要么一个月内恋爱，要么一个月后过节。</p><br/><p style="text-indent:2em;"><br />   善意的提醒一下，距离光棍节只有30天！有女朋友的千万要看好支付宝！</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;">　　（安全看世界，笑多了不怀孕。所有栏目内容推荐均撷取自网友的智慧言辞，仅出于传递信息的目的，不代表360新闻官方声音。与我们就文章内容交流、声明或侵删请发邮件至360newsfankui@ex.360.cn。）</p><br/><h2>　<strong>【笑多了不怀孕】微信公众号：<p style="text-indent:2em;">xiaoduo360</p><br/><br/></strong></h2><p style="text-indent:2em;"><strong>　　小伙伴们，火速关注起来吧ｇｅｔ&radic;，微信公众号在逐步完善中，让我们的笑多栏目更强大起来，期待你的参与。扫码关注!↓↓↓</strong><br /></p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"><strong>　　【广告来袭脱裤推荐】</strong></p><br/><p style="text-indent:2em;">　　我搜你看：我来搜索，给你好看。祖传搜索技术，专注看点300年。下载【每日看点】App请移至<p style="text-indent:2em;">下载页面</p><br/><br/>。扫一扫也可下载↓↓欢迎吐槽，闲聊请自备茶水。看点微信公众号码：ikandian。</p><br/><p style="text-indent:2em;"></p><br/><p style="text-indent:2em;"></p><br/>', '', 0),
	(3, 0, '<p style="text-indent:2em;">\r\n	　　生活，不是在等暴风雨过去，而是在暴风雨中学会跳舞。\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="http://p2.qhimg.com/t01230edaf249cd2214.png?size=390x375" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<br />\r\n</p>\r\n　　人，永远是矛盾的主体，经常处在犹豫和憧憬的困惑中，夹在世俗的单行道上，走不远也回不去。人，真的是一个难以琢磨的生灵，最了解自己的永远只有自己。人生在世都不容易，懂得珍惜该珍惜的一切，懂得把握应该把握的一切，懂得享受该享受的一切。让自己这辈子，过得充实、快乐！\r\n<p>\r\n	<br />\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="http://p7.qhimg.com/t0108dac405b76f7213.png?size=424x268" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<br />\r\n</p>\r\n<div style="text-align:center;">\r\n	<img src="http://p7.qhimg.com/t01ac7951cc0a803731.jpg?size=554x19" /><strong>　</strong> \r\n</div>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style="text-indent:2em;">\r\n	<strong>　&nbsp;&nbsp;&nbsp;&nbsp;</strong><strong>【每日新闻趣评】</strong> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【爽歪歪被曝风干变乳胶】\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　近日，一条“知名饮品爽歪歪风干后可以做避孕套”的帖子在朋友圈流传，内称爽歪歪营养酸奶风干后会变成乳胶状的物质，会影响孩子的食欲，影响健康。对此，多位专家表示，网友所述的乳胶状物质为乳饮料中都有的增稠剂，相比增稠剂，家长更应关注乳饮料中大量的糖对孩子身体潜在的危害。（新浪网）\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t011fb1b84ca6d9e82a.jpg?size=478x314" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　逗哥随口一说：为人父母者需关注此类信息，爽歪歪、AD钙奶等风干后成透明“胶膜”，有网友淘气了：可以做避孕套使用！哈哈哈哈！我想说的是：妈妈以后再也不用担心我会意外造人了！噢耶！\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【北京地铁调价听证方案出炉】\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　北京发改委上午将发布公交票制票价调整听证方案，其中，地面公交和轨道交通均实行计程值并按递远递减原则加价。地铁起步价分别为两元和三元，乘客均担分别为4.3或4.4元，同时对通勤族优惠。地面公交调价后仍是全国同类城市最低。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t017498c347aeafd601.gif?size=372x262" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　逗哥随口一说：平均乘坐下来均摊费用分别为4.3或4.4元，看来从单程2元的价格直降飙升5元以上了，虽说同时对通勤族优惠，但是通勤族应该会用通勤卡来进行界定，那么···通勤族会设有进入门槛，最低消费和申请标准卡，贩子又有钱赚了！会不会有户口限制，是不是必须得北京户口，会不会又来个在京缴纳5年社保才能申请，会不会要摇号住在内城，年龄低于35，硕士以上，党员优先是不是当然的啊~~哎，想到这里整个人都不好了。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t0116a25f33b731b942.gif?size=245x191" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　若只是对比票面的价格，北京公交地铁价格确实比全球其他大城市低得多。粗略比较可以发现，北京市的地铁票价堪称全球最低票价之一。这一低票价给了不少在北京高房价、高物价中挣扎的“北漂”们慰藉。但“砖家”们表示，长期对公共交通实行“低票价、高补贴”的做法是不明智的，因为纳税人所缴的税款应该覆盖百姓生活的方方面面滴。\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【女子疑被当小三遭当街扒衣群殴】\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　疑为原配女子率众群殴小三，当街扒光致其三点全露。10月10日上午，在濮阳县老城发生的这一幕劲爆场面，被网友上传到了微博、论坛上，内容包含了多张未进行处理的全裸照片，引发众多网友热议。人们在转发时，均未对照片中“小三”的女性裸露部位进行遮挡处理，相关网站管理方也一路绿灯。（人民网）\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t015f584a17de4b8f24.jpg?size=394x530" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01d117b31df194fc2c.jpg?size=409x549" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t0171fae01408537c56.jpg?size=398x534" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t012104be48e7b0d34f.png?size=391x498" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01237b76c372e5785e.png?size=393x508" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01ddc31f6ed4a755ae.png?size=395x497" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　逗哥随口一说：告诉我逗哥你不是为了围观看图而来的！！！\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　你们如果不是用批判眼光看过来的，说明你邪恶了！你坏透了囖～！\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;愤怒，愤怒，严重侵害人权，公共场所当众污辱妇女，必须严惩，此气焰不可长啊！男人正直何来小三！打了小三还有小四、小五、管好自己老公最重要，实在管不住就离婚，要是不理干脆把XX给咔嚓了，要么忍要么狠···咳咳，这段文字可以忽略。不管是不是“小三”，侮辱女性罪已经成立，不能混为一谈。无论如何应以守法尊法为先嘛！\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t015b4d8390304e2ce6.jpg?size=373x301" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　为什么出轨的男人没事？净挑软柿子捏？有本事就告上法庭把你老公的家产都抢过来，再将他踢出家门，看看他和小三能腻多久。女人何苦为难女人呢，为什么不能有气节点？！\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　老公找小三不论你怎么哭闹，没用的，就是打死小三也没用，还得搭上自己，孩子就毁了，现在这个社会太多了，所以倒不如活的漂漂亮亮的，把婚离了，随他怎么过，做一个优雅的女人更快乐，不是吗？\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t015c0884370c8863a7.png?size=474x234" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【广西“暴力剪发”校长：学生哈日哈韩不能不管】\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　10月10日，广西梧州藤县濛江一中一黄姓副校长被曝光“暴力剪发”：几十名长发男生集合到校内的空地上，并广播让其他师生前来围观他对部分长发男生强行理发。\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　对于“暴力剪发”，当事人则在先前的媒体回应中称，剪发事件说明学校对学生负责。他还称“我这个副校长当不当都无所谓，但是对学生肯定要负责任。”该副校长此举引发学生在网络上吐槽，广西师范大学教授谢晖指出，当事人的做法不尊重基本人格，不利于学生的成长。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t017fd283628a0945ca.jpg?size=373x278" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　逗哥随口一说：事发后，此事经媒体报道后，引发了网友热议，面对该副校长的“暴力剪发”，质疑其方法不当者也颇多，不过，也有人表示支持。\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　管不严父之过，教不严师之堕！有些学生的确不好管，青春期的小孩，就喜欢表面上的东西，还以为自己什么都懂，回想自己那个时候多数都是就这样，现在回头看看，都觉得自己可笑。家长老师有时候对孩子适当的严厉不是坏事，树苗要育，才能直。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01881b1cca33d2dc19.png?size=435x335" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　不以规矩，无以成方圆。但是可是，您的两剪刀“真的”能把所谓的坏的都给剪好么？当今，对学生、对家长、对社会负责的老师需要受到尊重！但是坏学生也需要被尊重的权利，“暴力”执法真的就给扶正捋直了么？！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01c96dd83d0a76d883.jpg?size=189x212" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　教育是为了传播知识还是为了人的发展？知识可以量化速成，但是育人之德则不能速成走捷径。都说十年树木百年树人，若不在德育方面多下些功夫，岂是为人师长者所不作为！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01129202ab238c8c03.jpg?size=510x335" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<div style="text-align:center;">\r\n	<img src="http://p7.qhimg.com/t01ac7951cc0a803731.jpg?size=554x19" /> \r\n</div>\r\n<p>\r\n	<br />\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	<strong>　　【每日逗比精选】</strong> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　在外拼三年，一无所有的回到家。本以为妈妈会大发雷霆。可没想到妈妈没有骂我，还安慰我：“孩子，你并不是一无所有，最起码你还有脸回来。”\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01c2715351ddc58a94.jpg?size=141x153" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　童鞋们对号入错吧，我是男屌.........丝.！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01476d87d581367a04.jpg?size=433x470" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　刚看到了一条围脖.个人觉得是很浪漫的表白喇..如果我这样跟一位女生说..不知道她会有什么反应呢?\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01fe93e162f19a2240.jpg?size=378x574" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　小家伙为了吃口奶也真是蛮拼的！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01fd0aa4f8c227bd90.gif?size=400x226" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t015ed4acea561010e7.gif?size=400x226" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t0121e8e9668c6c1e0c.gif?size=400x226" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　这是哪家的熊孩子？还有没有人管管了？\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01168c775269040d42.jpg?size=440x1263" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　火锅要和一群不熟的人吃，烤串要和最好的朋友吃，饺子和爱人亲人吃。而面条，一碗热汤面，只在孤单一人的时候最好吃。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01944bf47ac70a2044.gif?size=70x70" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　据说每一个女生的房间里都有一把这样的椅子。。。其实男生也有！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01f3f456fad3833d8f.jpg?size=440x440" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　有一种定格叫做“喷嚏好久才打出来”。。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t0139fcb139d1cd7ac9.gif?size=330x330" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　心理学上说：女人大多数只对有安全度的人发脾气。因为在那个安全度之内。潜意识知道对方不会离开你。胡闹是一种依赖。你的女票是酱紫么~！？\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01691e91c21255956c.png?size=395x319" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【逗哥带你涨姿势】男生别说逗哥偏向了，这期给你男淫们支招了！这些应该都是女生最需要的点，男生记牢了。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01bf82ea3574995bdc.png?size=374x140" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01897385ed9b79c66c.png?size=394x232" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t0113e8deb8db6411ec.png?size=368x185" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01eba06f6f3da64c86.png?size=373x142" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　你身边有没有那种自己长的很丑，天天还在朋友圈，微博里晒各种自我感觉良好的自拍照，最无法理解的是竟然还有人评论一句，美女啊！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01f6ad7fc6ab0906ed.jpg?size=292x235" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　头大真是世界级硬伤，整容都整不了，头大导致脸大，脸大导致脖子短，显胖，看起来身体比例不好，明明身体是八九头身，因为头大变成了五六头身。头大太可怜了，想组成一个头大者联盟，给予彼此温暖，互相欣赏慰籍。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01230df9f4d4d7ba7d.jpg?size=155x110" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t014ae753a3d96f3377.gif?size=100x100" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　钱包里一定要放你最爱的人，我感觉我的情敌已经多到全国都是了！逗哥钱包上镜～～了！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01b6f46f7ef81684e2.jpg?size=408x234" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　骆驼拉车，50一位。这真的是骆驼吗？我读书少，你不要骗我。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01acd17655c160dd65.jpg?size=318x352" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　和老婆同床时的地理划分......\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01c9333d878ff83c6c.jpg?size=440x354" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　如果有人问起，你怎么变胖了，你就说忘了。 不要解释，越解释越悲伤。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01dbca6ab938fca576.png?size=378x392" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【一大波萌宠想你狂奔！】开饭啦！！一组吃货开饭时各种反应的动图！简直萌哭了哈哈哈哈哈，最后那几只贩?/p&gt;\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t018458763a63adba59.gif?size=296x240" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01f534893773fa51dc.gif?size=380x260" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01a474a79142fbbb0e.gif?size=192x256" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t015c3aa91bb93be23e.gif?size=250x184" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0118a23bbc336658e4.gif?size=320x474" /> \r\n</p>\r\n<br />\r\n<p style="text-align:left;text-indent:2em;">\r\n	　　昨天一家4口外面吃饭，结帐255块，我自言自语：霍，差点就250了，找老板娘要了发票一刮奖，我擦，中了5块！！！老板娘碎碎念了一句，这回真是250了。\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t010f5e4da85a6e1d2a.png?size=390x218" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　冬天快到了，大家都要注意保暖，毕竟单身狗需要学会自强。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t012a96cf75a4281383.png?size=218x215" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　如何拒绝女神表白!\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t017152ed3a169857b9.jpg?size=440x724" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　现在100块钱真心不算什么了！早上带着红色毛爷爷出门，手机欠费充值，他变绿了；吃了顿饭，他变黄了；买了本杂志，他变蓝了；再买个煎饼果子，他变紫了；坐个地铁然后转公交，他只剩菊花了；最后买了一个棒棒糖，菊花也爆没了。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01e9300c09a0b08bce.png?size=263x127" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　日本老大爷冈田健一桑退休后在家赋闲，为哄两个孙子开心，老人家用木材自制了高达“百式”模型，细心涂装，完美还原了高达的金属质感。令人遗憾的是，两个孙子都表示不太喜欢这个模型.而我看到的是蓝天 草地！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t014209ee6760bef4a9.jpg?size=440x539" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t018f4c8bb040bd3757.jpg?size=440x687" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01969115ac113410ed.jpg?size=440x624" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01fa5679a8a289816f.jpg?size=440x292" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t0179a69e93a6982b72.jpg?size=440x292" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t0196e061bd98dbf28a.jpg?size=440x292" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t0196ce8f3dbcda9cc7.jpg?size=440x292" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【奇图大观】高手，这图拍的！！欢迎无极限，配上字幕，简直笑死人不偿命！哈哈！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t015ade9499d1458ba7.png?size=367x321" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01700732ff90ded644.png?size=404x285" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t013c462f2b2fb52dc6.png?size=361x375" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t015a68ac40fa9df27b.png?size=435x343" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t013685b621ae8ea553.png?size=434x342" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0184698507597e67f3.png?size=368x452" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t018baf03c243bda33c.png?size=420x304" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01a9c44874b4458a81.png?size=423x350" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t0116aa94a4a6c26baa.png?size=404x269" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t0118ac8bd4de616f37.png?size=404x336" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t013890797af9085402.png?size=347x444" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01c70a4cda5acc4e79.png?size=444x271" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01c068ea256321859d.png?size=329x331" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t013aad6bbecce6f7a7.png?size=357x371" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01cdb5db95d8d0e144.png?size=384x248" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01f0206c3cba970689.png?size=439x359" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t018eec3aeab1fab029.png?size=395x293" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t013ec00c24df899da2.png?size=318x461" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01a8d523cbf27ed0cb.png?size=405x313" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t011804e5b88427efb6.png?size=433x328" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01e7aa6c602762d8f1.png?size=437x324" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01a8546105abc32dc0.png?size=290x377" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　辣子鸡的“别名”，你知道么？\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01c17d9a0928d410cc.jpg?size=439x1647" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	小螃蟹教你们，不管敌人多么强大我们也不能轻言放弃！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t0195fe8db25f2f59b1.jpg?size=440x274" /> \r\n</p>\r\n<br />\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0193d45be0a3dba24e.gif?size=500x375" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t010a0106a46dfc67dc.png?size=477x223" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01ac7951cc0a803731.jpg?size=554x19" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　真想一巴掌拍死脑袋里面的那些胡思乱想\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t0117b68308cef64f19.gif?size=125x100" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　每个人都会有一段异常艰难的时光，生活的窘迫，工作的失意，学业的压力，爱的惶惶不可终日。挺过来的，人生就会豁然开朗；挺不过来的，时间也会教会你怎么与它们握手言和，所以你都不必害怕的。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t017d98ab904b1041d9.png?size=385x339" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01742155643b91c634.png?size=396x314" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01b1f2efc4cb5b8407.png?size=389x259" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01f9c0513797978a9e.png?size=382x192" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01de2f2a3658093203.png?size=387x327" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01a705013cf34c669b.png?size=390x258" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01a118711b69fdaa54.png?size=393x285" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01d7d9c903c052ec4b.png?size=390x236" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01400bcad60f565aef.png?size=393x242" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　一张图告诉你与土豪的差别。。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01431cc7ae50ce2fb7.png?size=426x355" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　延时摄影让你感受到地球的自转，感受到漫长的黑夜变短，让你感受到斗转星移、云卷云舒的奇妙！可以在30秒内带你领略浩瀚星轨！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01d246ea6d83ff4813.gif?size=300x170" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01d3b41c14dcfda924.gif?size=300x170" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01b10778d4c1dbf27d.gif?size=300x171" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t0142b3de440f9dd519.gif?size=300x171" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01cbc288ace1954edb.gif?size=300x170" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　上大学之前，以为男生到180是特平常的事，现在才知道，男生上175都是上辈子拯救了全宇宙……\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01f9ac623df91e41fb.gif?size=399x266" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　处女座怎么出洗手间……\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01a07e0419bc51e4c8.gif?size=235x234" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　狗狗，你的表情能再蠢一点吗。。。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t0183fcbddd367edff2.gif?size=245x138" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　这是哪位高人画的啊，吊炸天！跪了跪了~\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0150f48edf2cd30c3c.gif?size=279x188" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　感觉胖子已经无法在这个星球生存了…拜拜!\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01ad87ba7c34d2ae9b.jpg?size=440x1280" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　有时候特别想谈恋爱, 但一想到, “我脸上皮肤不好,ta要是亲我就会看到了,ta 抱着我腰要发现我隐藏的肥肉了, 一起吃饭就知道我饭量像三个汉子了, 平时捧个手机刷微博哈哈哈哈卧槽的笑他会觉得我是神经病吧, ”我就觉得还是单身好。不过想着我有这么多毛病根本就找不到对象,也就释然了。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01fb81f0b8e0947692.png?size=337x245" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　你们都不要拦住我，我要去迪拜看看人家的喷泉.\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01e5b68995a823e8ce.gif?size=280x280" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　当魂斗罗遇到俄罗斯方块时。。。 看一次笑一次！ 哈哈哈哈\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01b6a3d5bbd0dabae2.gif?size=300x225" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　今天地理课，一女生不听讲，在底下化妆。。老师走到面前问：“你能用两个地名描述一下你的脸吗？” 女生怎么想也想不出来，于是问他是什么。老师答曰：“大连 ，太原。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0184006226fca1b3bf.jpg?size=440x281" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　国内很少见到这样的街头艺术，艺术家David Zinn的粉笔画，有爱，萌萌哒！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t017ca93002d36bcad7.jpg?size=440x291" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t017ca93002d36bcad7.jpg?size=440x291" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t014a1a1c5c989bcbc5.jpg?size=440x547" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01ac9f0f1564995993.jpg?size=440x289" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t018ed924ed167ccde3.jpg?size=440x547" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01a0a9d16646f2fb8a.jpg?size=440x547" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t0186bb8c4c19e1d281.jpg?size=440x573" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01bea0b3525ced163e.jpg?size=440x547" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t0165daa13d92732df8.jpg?size=440x547" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　每次上完马桶擦屁股的时候感觉自己就像是蜘蛛侠。\r\n</p>\r\n<br />\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t0178a1d200ccacd64a.jpg?size=440x411" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　我猜到了开头却没猜到这结局...汪星人被吓尿!\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01e97642ed4acf454c.gif?size=320x165" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　这位护士你下班别走，我保证不打你！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01bd2b0fa8223fe295.gif?size=270x270" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　一次男友在火车上,给对面的小盆友表演魔术,拿出一盒纸牌,抽出红桃A,扔出窗外,再拿出准备好的一张红桃A。小朋友一个劲地鼓掌,向他走来,他以为小盆友要对他大佳赞赏，没想到小盆友却把他桌上的手表扔出窗外,说:“哥哥，再表演一个。”\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01390b243889f6cfa4.jpg?size=192x170" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　一则富含哲理的漫画：努力总是有回报的。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t019a5a2c9489ca576c.jpg?size=440x545" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　【毛巾兔折叠法】萌萌哒兔纸 ~你学会了吗？兔子\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t013d090d0b2fa15063.jpg?size=440x392" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t0165527c4e05c10a83.jpg?size=440x602" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01383a44d7155616b3.jpg?size=440x616" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01f22d596cb415bb1a.jpg?size=440x644" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01df0094630471003c.jpg?size=440x658" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t015cfea5cc555690bd.jpg?size=440x659" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01266e478cce08021d.jpg?size=440x725" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01c5ca9297a7336945.jpg?size=440x672" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t013e1fa70352397df6.jpg?size=440x341" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　如何用最快的速度画出一只玩具熊！！！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t011f6b1b4a62813b40.jpg?size=390x547" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　两只捉迷藏的喵星人，小喵的智商还是略胜一筹!\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01aa31d6982dd1c90a.gif?size=400x225" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01ded4f25f711dcc77.gif?size=400x225" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01758fb423e1e3a708.gif?size=400x225" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　女生在生气时放出的狠话，大多数都是假的。可男生基本上都信以为真，心惊肉跳！女生在撒娇时提出的要求基本都是真的，可男生基本上都觉得是个玩笑。这种行为就叫做找抽！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01ce75398c0883b0b9.jpg?size=201x149" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　老天是公平的，虽然你长的丑，但是你想的美啊。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01e3e4ceb869debbe9.jpg?size=161x162" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　对于这样的主人，已不想再爱，呵呵，你开心就好！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01532a6f958b5aba75.gif?size=269x400" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t0109463b80c1b86267.gif?size=316x320" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01db209b35d1cb253b.gif?size=320x221" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t012ba1fa432e5d1892.gif?size=350x263" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t0196973626f6e8c4ad.gif?size=250x250" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01d55006ebdc866ba6.gif?size=400x370" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t015fbf8b4aec9cd66d.gif?size=206x196" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t013c2f89bfacff3ead.gif?size=240x149" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　朋友圈十大奇葩，你有木有躺着也中枪。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t0104c7ecbd8ee07c52.png?size=371x500" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t018522d70b99bebd90.png?size=372x443" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01bbd53067bf2d016b.png?size=370x597" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t0105e901809fe68817.png?size=372x434" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0181926368d8ed7c80.png?size=378x484" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t019a669f2ba27bc542.png?size=372x534" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t0101f315dcbad78e70.png?size=372x498" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t0115552952417cdda6.png?size=374x400" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01214b22f580358a4a.png?size=368x545" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t0137ef8c267e51a022.png?size=373x314" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　当两个人在给对方买东西的时候，都不会心疼钱大手大脚，到了为自己花钱却总要犹豫半天反复思量，这个时候差不多就可以结婚了吧！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01ca80e8822dd4a730.png?size=379x411" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　“嗨！老乡，你们去哪儿？” “肯德基，你呢？” “哦，我去全聚德。” 今日一别，再见已是来生！泪\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01489903abce348cff.jpg?size=440x276" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　卧槽槽槽槽槽槽槽槽槽..... ！！！！！no zuo no die!\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01a71a6ca72086b50e.gif?size=400x208" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　高考结果应该现在都尘埃落定了，很多人在读着大学，有些人开始了走进社会生活，我今天作为一个长者告诉你们一些做人的道理，考的好的同学，一定要记得请考的不好的同学吃个饭，等你们毕业出来搬砖的时候，人家可能早混成工头了。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01df3244ba8a1c84cb.png?size=340x196" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　作为一只癞皮狗，告诉你，老子说不放，就不放…… 哈哈\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01148431ed9f8e5c94.gif?size=256x158" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　专门治疗那些手机不离手的病人。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01d26b4db4cd8254f1.gif?size=246x137" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　今天路上看见前面一女汉子在打电话谈分手，听她说：不就分手嘛，有什么了不起的，嘻嘻哈哈的说改天咱两吃个分手饭呗……心想这女的真洒脱，霸气！走快点去前面看看长什么样，然后我就看到她满脸泪水…….\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t0116b656e9d2de19ed.gif?size=80x80" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　这狗居然有洁癖，撒尿怕弄到自己脚上，所以用倒立高难度动作，但是就不怕弄到嘴里？\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01ebe71d2256b51ad7.gif?size=380x213" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　小学辍学的同学，现资产上亿，旗下3家5星级宾馆；初中辍学的同学，现资产上千万，旗下2个特种车队；高中辍学的同学，现存款上百万，每月上班时间不超过10天；大学肄业的同学，现有房有车，工作稳定；大学毕业的我，现在小学辍学的同学单位加班，无双休；仅几万元积蓄，而读研的同学，现正在找工作。。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t015e3eb0c6a4557fa0.jpg?size=300x164" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　你帮人100分，当有一天你只肯帮80了，他便会清空你所有的恩，宁愿选择只帮他70分的人做朋友。一粒米养恩人，一石米养仇人，老人说的话没错。不要动不动就倾其所有，留一些骄傲与心疼给自己，记得了，最暖不过人心，最凉不过人心。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01443a725eca02fee7.png?size=303x301" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　饭，我所欲也；瘦，亦我所欲也。二者不可兼得，我嘞个去也！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01e4c1f39ecca197fa.jpg?size=294x249" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　推主“napalmthing”出门玩耍，突然发现前面路人的打扮很眼熟……\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t0194557d8f66593a2d.jpg?size=247x267" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　当对方拒绝你的时候，不要总是打破砂锅问到底，免得伤了自己那颗脆弱的小心脏！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01792145214dbbf49e.jpg?size=440x565" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　你们憋逼我吃药，我要一直蠢蠢哒！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t01a00fc120833398b9.gif?size=400x300" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t0194ca6c29a8853761.gif?size=200x134" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t0169e1c7d5471aa004.gif?size=340x250" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01e87e328504834f53.gif?size=320x217" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01a9bb2f0523e66bfd.gif?size=400x225" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t015ae75f8ed65aae63.gif?size=245x149" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01b8dfd11d4f94d80f.gif?size=400x225" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0167d3a77391095324.gif?size=340x258" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　和同事在酒吧喝酒，隔壁桌的美女不断的挑逗我，正准备过去搭讪，突然想起含辛茹苦的老婆还在家中为我留了一盏灯等我回家，心里实在过意不去。于是我立即走出酒吧，在门口找到一小孩对他说：“给你十块钱，你去把XX小区3栋602的电闸给我拉了。”\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01b479ae0a4a70bd3d.jpg?size=177x216" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　南加州Chiweenie Farms草泥马农场，因为快要变冷了，农场里的工作人员怕小羊驼冻着，给它们穿上了小朋友的衣服。。有点萌！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01aa40f12cc224e2d7.jpg?size=440x294" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01a37fad7344c3b4ae.jpg?size=440x315" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t0145f708fdd6ac1e6e.jpg?size=440x294" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01de12dd9e75e099b9.jpg?size=440x294" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t016f0a638dce5cec84.jpg?size=440x294" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01f3fe2127b6679974.jpg?size=440x294" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t019ebaed6677cf746f.jpg?size=440x294" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t010a91c2961c6c33d3.jpg?size=440x280" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t014e6a45fe4f4f115c.jpg?size=440x294" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　别去试人心，它会让你失望。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t014b0f9f184f7f3adf.png?size=390x274" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　美国版的“三个代表”！↓\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　其实美国选择白头鹰作为国家形象，本身就代表了种族歧视：头部代表白人负责统治，身体代表黑人负责干活，嘴部代表黄皮白心的中国公知负责嘴炮……\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t0158812de21b5a38f3.png?size=392x261" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　论单身狗的自我安慰，事实向你证明单身也有很多好处。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01f3a7554175f356d6.png?size=343x125" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p9.qhimg.com/t01561201c40d97622e.png?size=348x79" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t0141a4833e2e7c9dd9.png?size=339x96" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01513f308e5d9e1a53.png?size=343x94" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t010498e5915e04863f.png?size=343x89" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01bea8a78b8f7556b1.png?size=346x86" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01d8789e1a69012108.png?size=343x84" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01ba44034390999d74.png?size=345x93" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t01b93329ca4688c72a.png?size=345x114" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　骚年你如此妖娆，你家里人知道吗？！哈哈第5个，绝了！！！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t0102a21f169d8c3dc3.gif?size=342x372" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t0160b08202ec93e992.gif?size=249x215" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t0110bd4056217febb3.gif?size=180x240" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01f1a3eea0aa6ee3f1.gif?size=144x176" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p5.qhimg.com/t0192539077a9ccde58.gif?size=184x180" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01a4aaba4fe49500a5.gif?size=180x180" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t017bb68fc1e12d4f84.gif?size=200x150" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t015b59d67f96e480dc.gif?size=310x174" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01a2f8afa5b6fbb2ce.gif?size=180x240" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　假如你有了上电视的机会，你会怎样把握这短暂的几十秒钟？机会只给有准备的人！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t012aa855079b8deab2.gif?size=210x312" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　这样一来，漏“沟”的问题就得到了完美的解决，直接给跪了。。。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01ca5896ecd537eef2.jpg?size=440x586" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t014a6762b14f36afa2.jpg?size=440x586" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t014a6762b14f36afa2.jpg?size=440x586" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t014a6762b14f36afa2.jpg?size=440x586" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t014a6762b14f36afa2.jpg?size=440x586" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t0172724ad90ef1a57c.jpg?size=440x558" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　国外一位网友买了只森林之王，到货的时候。。。他炸了！这霸气忧郁的眼神！~\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t015c6f162ad8e3b022.jpg?size=440x369" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t01d079769000dd3576.jpg?size=440x587" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01fe58dcb60c0cfa42.jpg?size=440x586" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　OH！马勒个臀！你个贱崽子就是这么对待人的啊~~！\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01be7f1b06e23d5e4a.gif?size=400x268" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t0107e8bef45df43ba6.gif?size=270x270" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01068202fcf7cc28b4.gif?size=400x320" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01947da2959a2e93a8.gif?size=320x180" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　你们说，周董要是知道了，会不会吐血三斤。。。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t01abc19a29d9c42d70.jpg?size=440x330" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t01dc077a5e16063b48.jpg?size=439x579" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01c0bfc508bd9f7bd9.jpg?size=440x486" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01fff209aaf8fe1765.jpg?size=440x314" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01e6140b70a2a08806.jpg?size=343x220" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p1.qhimg.com/t01cd25cd82127e4e59.jpg?size=440x310" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p2.qhimg.com/t0184b26dbafa7e52eb.jpg?size=341x468" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01a89355a9d2fe2ed2.jpg?size=382x307" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p3.qhimg.com/t016a3f341c5326d235.jpg?size=440x332" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　　没我在的日子里，你要好好照顾自己，记得按时喝酒，不舒服多抽烟，多熬夜，早饭不要经常吃，天气冷穿凉鞋多穿对袜子，没事多玩手机，看书记得关灯，过马路的时候记得不要看红绿灯，如果睡不着要多吃安眠药，无聊就烧烧头发，洗澡一定要用沸水，难过了就吸吸毒，一切都会变好的。。。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p6.qhimg.com/t01a85b155c4ef0af49.jpg?size=236x236" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p8.qhimg.com/t01ac7951cc0a803731.jpg?size=554x19" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　本想把日子过成诗，时而简单，时而精致。不料日子却过成了我的歌，时而不靠谱，时而不着调。\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p7.qhimg.com/t016c7ad2625e7286d1.png?size=390x351" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　深呼吸，累了一天了，思考下下人生吧，30岁前的这几年，累呢~是一定的！但相信人生不可能就止于此了的。不想变成街上一抓一把的庸人，不想以后为钱发愁，不想以后每天做的都是不喜欢却必须做的事，不想成为那种人。活着么，奏得有梦想，有方向，所以要努力。只有坚持这阵子，才不会辛苦一辈子！\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　又到每年这个时候了：要么一个月内恋爱，要么一个月后过节。\r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<div style="text-align:center;">\r\n	<img src="http://p3.qhimg.com/t0176a45a15314185c7.gif?size=68x68" /> \r\n</div>\r\n善意的提醒一下，距离光棍节只有30天！有女朋友的千万要看好支付宝！\r\n<p>\r\n	<br />\r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p0.qhimg.com/t015b5bae5727601774.png?size=392x494" /> \r\n</p>\r\n<br />\r\n<p style="text-align:center;text-indent:2em;">\r\n	<img src="http://p4.qhimg.com/t01ac7951cc0a803731.jpg?size=554x19" /> \r\n</p>\r\n<br />\r\n<p style="text-indent:2em;">\r\n	　　（内容推荐均撷取自网友的智慧言辞，仅出于传递信息的目的，不代表本站的声音。与我们就文章内容交流、声明或侵删请发邮件至shangshanshishui@163.com。）\r\n</p>', '', 0),
	(4, 0, '<p style="text-indent:2em;">\r\n	美国芝加哥洛约拉大学研究人员发现，那些交了些朋友比自己还胖的人，他们也会更容易发胖。相反，对于那些交了些比自己还瘦的朋友的人，他们要么会慢慢瘦下来，或者至少胖得特别慢。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e046372cc6.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-indent:2em;">\r\n	看到这里，顿时摸了身上的肉，心里想，我朋友真好，到现在还没打死我。\r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e062bf06e2.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	狗狗是人类的朋友，我嚼的吧，这话一点不假。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e072323065.jpg" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	一切为了卖相\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e07ab40515.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e07ab755f7.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p style="text-indent:2em;">\r\n	吃货都想有个神器，好想有一台啊。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e08f83ff58.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	三五好友里面，总会有几个逗比啊。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e096ca1c87.gif" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e09765d973.gif" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e098138e50.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	父母和孩子也会有很多欢乐\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e09993ac3b.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e09a360912.gif" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e09ab0848f.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e09b4e8dfb.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e09c70aebd.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	喂，大哥，我恐高，快放我下去。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0a1729d52.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-indent:2em;">\r\n	手机什么的，指纹解锁什么的都弱爆了\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0a81e0919.jpg" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-indent:2em;">\r\n	来，亲爱的，亲一个，额。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0ac6e8b91.jpg" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	人家就要睡你旁边\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0afc14de9.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	走，兄弟们，去吃好吃的，快，跟上\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0b4193d61.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-indent:2em;">\r\n	快停车，警察查牌\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0bb05c48f.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-indent:2em;">\r\n	卧槽，吓死老子了\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0bfa116b1.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	身边有个胖子也挺不错，欢乐多啊\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0c739494e.gif" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	我们都是得瑟的小孩，给点阳光就灿烂，好了伤疤就忘了疼，别说好听的话，我们很容易当真\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-15/543e0cdbddc3d.jpg" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>', '', 0),
	(5, 0, '<p style="text-align:left;text-indent:2em;">\r\n	凡事不必苛求，来了就来了；凡事不必计较，过了就过了；遇事不要皱眉，笑了就笑了；结果不要强求，做了就对了；生活就是一种简单，心静了就平和了。 不要去拒绝忙碌，因为它是一种充实；不要去抱怨挫折，因为它是一种坚强；不要去选择沉默因为它是一种软弱。\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f1e8d1fbbc.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【小盆友，你有对象嘛？】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f1efae0071.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【男朋友永远无法理解】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f1f67dd476.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【女人真的不要动不动就和男人生气.....】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f24ec4c283.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【小编被一个游戏震住了，你来感受下】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f1fb7adf12.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【我们的狗狗受到二湿兄的影响。第四张，亲，疼不，我看着都疼啊。】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f20d5710fd.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f20dfced87.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f20ea75a3e.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f20f32aad0.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f20fd3facf.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f210d6d21b.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f211911c14.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f212463d1c.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【我最讨厌有人跟我说东南西北了！指路不会说左右啊....坏人】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f218f1edd2.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【悟空，你又调皮了！】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f21dc2f3d3.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【看什么看，我这可是真皮手机壳】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f220af391e.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【新技能，必须get】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f223f71572.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【今日一别，再见已是来生】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f2262b71f0.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【见过大的，没见过这么大的啊！！！】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f22b2c1995.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【真相只有一个，我们都是二货。。。】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f23190b272.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【那是我小时候，擦黑板的情形】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f256c86d36.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【我用飘柔，我骄傲】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f2357da08f.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【免费pos机，不含防腐剂】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f2392753d5.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【岁月啊，看到这个，哥哭了。】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f23c21b401.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【别家孩子的大学】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f24a85a744.jpg" alt="" /> \r\n</p>\r\n<p style="text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【叫你还敢不冲厕所】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f25bec8057.jpg" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	【突然想到，今天周四了，来，大家动起来】\r\n</p>\r\n<p style="text-align:center;">\r\n	<img src="/Uploads/Editor/2014-10-16/543f23edb2dd7.gif" alt="" /> \r\n</p>\r\n<p style="text-align:center;text-indent:2em;">\r\n	<br />\r\n</p>', '', 0),
	(6, 0, '<p>\r\n	【想一想有一个脸大的朋友还是挺不错的嘛，你不开心的时候可以揉脸逗你笑！】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544062753c970.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【大闸蟹，我最爱吃，大闸蟹也有很萌很萌的时刻哦，老板，来盆大闸蟹。。。别误会，我只是想看它们游泳而已啦。】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544062ee95dae.gif" alt="" /> \r\n</p>\r\n<p>\r\n	不要带给自己烦恼，也不要带给别人困恼。对自己好，就要用心；对别人好，就要关心。看别人，烦恼起；看自己，智慧生。体谅别人，就会做人；清楚自己，就会做事。人经不起考验，故不要轻易考验于人。有一种承担，叫提得起，放得下；有一种负担，叫提得起，放不下。\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	【这货竟然叫“开始”，可我每次点它似乎都是为了结束！求解】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544063419118a.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【00后开始恋爱了，90后开始离婚了，而80后还是单身，是我想不明白还是社会变化太快。唉！！！小编还是单身呢。呜呜呜呜呜呜】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544065dc63bf2.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【喵星人已经逆天了】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440660982711.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【大家一起来跳剁手舞，一起剁剁剁剁！！】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/54406669641c3.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【汪星人总是很欢乐的啊，只不过这是别人的家狗狗，别人家的。。。】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/54406692aa27c.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【友情提示：为了孩子的身心健康，千万别让男人带娃。。。】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544066b41a78c.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【非常牛逼的心理和人格测试，可以非常准确地判断出你属于哪一类人，测过的都说准！小编不敢测试啊，看到答案的我，眼泪流下来】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544066e938089.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【上学那会，总感觉有双眼睛盯着我】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440671bb2894.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【还是自己给的安全感最有安全感。。。这话真安全啊】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440674f82e32.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	【同问啊：我一直在纳闷，老师为什么要请家长到学校？一个连未成年人都没教育好的人，还想教育成年人。。。】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/54406779645b1.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【大哥，拉住我，要掉下去了。。。】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440679cd77ec.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【家有好女儿】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544067b903493.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【哎啊，妈啊，好激动啊，幸福来的太快了，我感觉我要登上人生的巅峰了。】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544067de0bcb8.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【丧心病狂的自动洗喵机，养猫人士福音！，老板，来一台啊！！！】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544068143b430.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【“怎样才能做到不在乎别人骂？” 网友回答：“你要相信，比你优秀的人是不会鸟你的。”】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440684084b6c.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【唱歌跑调是病】英国科学杂志上一份研究发现，唱歌跑调是种病，这类人多半方向感不好、整理能力差。研究人员称，这类人可能还存在沟通障碍，比如辨别不出对方语言中是生气、害怕还是讽刺。有国内专家也说，天生爱跑调的人可能不那么善解人意。你中枪了吗？\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440685fd79ff.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【手机触屏坏掉还可以这样用？！我读书少，你不要骗我...】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/54406887340a7.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【终于明白：没有共同语言的聊天就像一盘沙，都不用风吹，聊两句就想去洗澡了。怪不得，好多人和我聊着聊着，就去洗澡了。】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544068b88bacc.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【我相信，每个胖子，心里都住着一个瘦子】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544068f2a0740.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【笑一笑，十年少】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440691052723.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	工作不行，恋爱不行，化妆不行，唱K不行，长相不行，身材不行，经济实力不行。我一直都在思考一个问题：到底是什么支撑我活了这么多年。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/5440692a931e2.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【哈哈哈哈哈哈哈哈哈哈,直接看图】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544069523728c.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【刺猬宝宝踢被子】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544069851432d.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【身高越高，责任越大。。。哈哈哈哈太有爱了！！】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544069bd704c1.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【又是别人家的，别人家的领导啊】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/544069fe398e6.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【知道你为什么瘦不下来么？方法不对！这样减你早都瘦了！】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/54406a1c01635.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【有些喵星人，它们忘了自己的身份，在逗比的路上越走越远...】\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/54406a42eac69.gif" alt="" /> \r\n</p>\r\n<p>\r\n	真正的友谊就是你牵着我的尾巴，一起在湖畔的落日下\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-17/54406a5881ae6.gif" alt="" /> \r\n</p>', '', 0),
	(7, 0, '<p>\r\n	【喵星人威武】逆天的喵星人，白喵说，想打架嘛？你给老子站起来。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee2b625247.gif" alt="" />\r\n</p>\r\n<p>\r\n	【妈妈最了解女儿啦】你妈妈是不是永远觉得你把刘海全部梳上去扎着大马尾光溜溜一张大脸最好看。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee2d29dbef.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【男友暖心话语】“因为她才一米五，所以一旦吵架了，我就必须先低头”。 今天听到最暖心的一句话&nbsp;<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/19/heia_org.gif" title="[偷笑]" alt="[偷笑]" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee2f1c3ad0.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	【珍惜眼前人】如果我用你待我的方式来待你， 恐怕你早就离开了。我不走，是因为我爱你。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee31f60047.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【真心对待，会有结果】光棍节就要到了...只能帮你们到这了......<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/70/88_org.gif" title="[拜拜]" alt="[拜拜]" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee34b4ac6a.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【为制作人点赞】卧槽。这个也太牛了吧，不转对不起那个制作人了。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee36e78155.gif" alt="" />\r\n</p>\r\n<p>\r\n	【黑毛（猫）】因为嘴下长有一块黑色的毛，使得这只叫Banye的猫咪看起来总是一副吃惊的表情，网友称之为“OMG”猫~\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee3b8d7493.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee3c012050.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee3c66ce83.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee3ccd5326.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee57061e1f.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee5780e662.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee5809171f.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee58c818d0.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【最近老是梦到一个人啊】据说：你梦到了一个人，是因为那个人在想你。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee5ab00bd2.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【好难选择啊，不过我一定是在做梦】红色与绿色，选哪个？\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee5d8a2def.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【专治各种不服】那些神一样的统计，专治各种不服。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee61b0e847.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【我不想不想长大】看到这图，我终于明白了SHE的歌词：我不想，我不想长大！！太惬意了！！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee64342725.gif" alt="" />\r\n</p>\r\n<p>\r\n	【好妖媚】在马路上遇到这货你会怎么做！？\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee65e75a2c.gif" alt="" />\r\n</p>\r\n<p>\r\n	【哈哈哈哈】珍惜你身边笑点低的女孩子吧，听说西周灭亡就是因为一个笑点太高的女人。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee68a7bafb.gif" alt="" />\r\n</p>\r\n<p>\r\n	【时光的句子】那些年方文山 周杰伦惊艳了我们时光的句子~ 词太美 流过我记忆里最温柔的岁月~ ..~(〃ω〃)\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee6d0807e2.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee6d7e0b1e.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee6de90edb.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee6e58f534.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee6ee8b9c9.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee6f4a1633.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee706b0dfe.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee70e50b03.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee716ebad6.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【哈哈哈哈，危险来了】当下一个是你时，有些事就没有那么好笑了。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee7325e8ab.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【啊啊啊，你妹啊，好疼】如何快速脱毛……小盆友们在家可不要模仿哟\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee755380c6.gif" alt="" />\r\n</p>\r\n<p>\r\n	【摸摸我嘛】主人，来麻来麻摸摸我麻~~\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee78ad0cbd.gif" alt="" />\r\n</p>\r\n<p>\r\n	【你们都要让着我啊】我读书少，长的丑，你们都要让着我<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/9d/sada_org.gif" title="[泪]" alt="[泪]" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee7c19e90a.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【肉肉，我们分手吧】肉肉你走吧，别那么迷恋我，我们分手吧。我们不合适。真的。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee7d759af7.gif" alt="" />\r\n</p>\r\n<p>\r\n	【女朋友真好，爱你一万年】第一次去女朋友家，她五岁的弟弟说我长得丑，好丑……等我回家后，小家伙用我女朋友电话给我打说我帅，刚才是逗我玩，尼玛，带着哭腔赞美我。你姐到底对你做了什么！！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee7fd63def.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【温馨到家】网友拍摄的一组宝宝对待弟弟妹妹的各种温暖瞬间，简直有爱到爆！！<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/40/hearta_org.gif" title="[心]" alt="[心]" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee858bd0cf.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee85f1ece6.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee866b1b42.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee86e4364e.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee87679b66.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee87c5bc25.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee88365859.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee8898a56e.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee88f79421.jpg" alt="" />\r\n</p>\r\n<p>\r\n	每天醒来，面朝阳光，努力向上，相信日子会变得单纯而美好。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-28/544ee8a5e7b25.jpg" alt="" />\r\n</p>', '', 0),
	(8, 0, '<p>\r\n	总有一天，会有一个人，看你写过的所有状态，读完写的所有微博，看你从小到大的所有照片，甚至去别的地方寻找关于你的信息，试着听你听的歌，走你走过的地方，看你喜欢看的书，品尝你总是大呼好吃的东西……只是想弥补上，你的青春——他迟到的时光。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450a97411bbd.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【做梦都想】我梦想的冬天应该是这个样子的\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450a9a837315.gif" alt="" />\r\n</p>\r\n<p>\r\n	【真羡慕你还能向爸妈要钱】问爸妈要零花钱时你的状态。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450a9da8c104.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【笑到肚子疼啊】搞笑动物来袭<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" />&nbsp;哎哟，最后一个在干嘛了啦！？\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa430fb5a.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa4cc486f.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa549af31.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa618f4e3.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa7154501.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa7e49cf0.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa8865fcd.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aa990ca1a.gif" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aaa25df02.gif" alt="" />\r\n</p>\r\n<p>\r\n	【终于有话接了啊】虽说早睡早起身体好，可是晚睡晚起心情好啊\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aacc51104.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【哎呦，腰疼】00后的跆拳道比赛，我笑的腰疼 ！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450aaf29b72a.gif" alt="" />\r\n</p>\r\n<p>\r\n	【熊孩子全宇宙都一样】熊孩子不分国界，国外几张小盆友们令人费解的言行。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab478750d.jpg" alt="" /><img src="/Uploads/Editor/2014-10-29/5450ab5774ab5.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab61b5806.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab68053c6.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab6f67e15.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab75b467d.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab7c42511.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab8425fc7.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab8ad0ac0.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ab94de332.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【请看官们，对号入座喽】上班族各单位从业者表情一览表,神似啊!!!\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450abbdd0df2.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【只因你太美，我偷偷看了你一眼】街角偶遇一只喵，风霜尘土都掩盖不了她的美！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450abdf592f4.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【我也想有个胖子朋友，可是我是被欺负的那个啊。。。】论身边有个胖子朋友的重要性，简直不是一个等级的！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ac20a14d6.gif" alt="" />\r\n</p>\r\n<p>\r\n	【直接给我钱吧】别对我说天冷了照顾好自己穿多点衣服之类的话 要么就照顾我 要么就拿钱给我买衣服 要么别来烦我!!!\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450accd73f3d.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【我好幸福】你们该庆幸了，外国家长是这么盯孩子做作业的。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450acea497b3.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【哈哈哈哈哈哈】1.点开大图；2.按住鼠标左键往右拉；3.恭喜你，获得一只神奇草泥马~\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ad0e15c37.gif" alt="" />\r\n</p>\r\n<p>\r\n	【神技】这样处理橘子就不会弄得汁到处是，也不怕弄脏手啦~还能迅速吃到橘子~\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ad3690cb0.gif" alt="" />\r\n</p>\r\n<p>\r\n	【坐不下，真的坐不下啊】坐不下了坐不下了，哈哈哈，尼玛看一次笑尿一次！！<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450ad86f2e8e.gif" alt="" />\r\n</p>\r\n<p>\r\n	【就要很普通啊】其实，男女订婚酒席不需要满满一桌的山珍海味。一些每天都能见到，普通的不能再普通的东西也会让所有人感到满意。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450adab8c84b.jpg" alt="" />\r\n</p>\r\n<p>\r\n	【这书出自蓝线吧？】万能检讨书，不用改动任何东西。以备不时之需！你，值得拥有！<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/b6/doge_org.gif" title="[doge]" alt="[doge]" />\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-29/5450adda64b17.jpg" alt="" />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>', '', 0),
	(9, 0, '<p>\r\n	【要乖哦】你长大了，不能像小时候那么粘人了知道吗，乖~<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6e/panda_org.gif" title="[熊猫]" alt="[熊猫]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545184a7d75c1.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【主人真是讨厌啦】蓦然回首，人和狗之间最基本的信任都没有了。。。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545184ceadc0d.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【来来来，斗一斗】还记得这只猫吗？哈哈哈！原来是遇到对手了。。。 哈哈哈哈看一次笑一次！！<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" /><img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545184f0c096c.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【爱老婆】以关心老婆为荣，以忽视老婆为耻；以伺候老婆为荣，以麻烦老婆为耻；以赞美老婆为荣，以批评老婆为耻；以抢干家务为荣，以好逸恶劳为耻；以下班回家为荣，以夜不归宿为耻；以诚实专一为荣，以拈花惹草为耻；以遵守家法为荣，以违法乱纪为耻；以上缴工资为荣，以窝藏奖金为耻 ~~~\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/5451850d124d9.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【我得加油啊】一个在吃和睡之间努力挣扎的小朋友，吃货就是这样努力！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186273329c.gif" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/5451863293617.gif" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/5451863be4655.gif" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186439bd60.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【万年真理】不变的真理 中午不睡下午崩溃&nbsp;<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/f3/k_org.gif" title="[打哈欠]" alt="[打哈欠]" /><img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/7f/sleepya_org.gif" title="[困]" alt="[困]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/5451868b6aa45.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518691e6300.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518697e81da.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186a0077a2.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186a6275b2.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186accd060.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186b31e772.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186b970036.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545186bfa9f99.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【要做就要中午做】做坏事早晚都会被发现，所以中午做。\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/5451877b5e779.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【不好】昨天看见朋友发了一条“男人很累，出轨无罪”。然后底下一条评论“生孩子也很累啊，不要在意是谁的了，好吗？”\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545187b8f4166.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【我邪恶了】今天在幼儿园看见一个穿棉靴的女人，她二姐说：我以为她把裤子脱了呢！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545187d19c45d.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【嘿嘿，我爸做的】爸爸给小女儿做了一个LED灯万圣节变装，在黑暗中女儿就变成了一个火柴人！感觉简直萌萌哒！！好可爱！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545188181967f.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【你眨了下眼，我。。。】你眨了一下眼，他记了五百年。喜欢一个人的时候，望着他，眼神里是会有光的。。。<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6d/lovea_org.gif" title="[爱你]" alt="[爱你]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518866ac6e0.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【好难看啊】人有两种，一种好看的，一种难看的。我夹在中间，属于好难看的。。<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/70/88_org.gif" title="[拜拜]" alt="[拜拜]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545188860d0ae.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【我也跪】卧槽！这神一样的思维。。。彻底给跪了！<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545188a256f63.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	【谁家的孩子】想必司机当时都气疯了.........&nbsp;<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545188f32e338.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【我就不理你】主银早上忘记喂这只喵了，现在怎么安抚都没用。。。这演技，简直绝了！哈哈哈哈\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518992330ae.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【我跳我跳跳跳】狗狗想要跳上床去看宝宝，但无奈腿太短了跳不上。。。<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/19/heia_org.gif" title="[偷笑]" alt="[偷笑]" /><img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545189c61760f.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【好精彩好精彩】非常精彩的决斗小电影。。。。。笑死我了<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/545189d9be743.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【俺是实在人啊】“可以给我们示范一下吗？”“嘿嘿，可以啊。”“。。尼玛。。。”<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/cc/whh_org.gif" title="[哇哈哈]" alt="[哇哈哈]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518a08682e0.gif" alt="" /> \r\n</p>\r\n<p>\r\n	【来一捆】天气变冷啦，又可以揣小手了~<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/8c/hsa_org.gif" title="[花心]" alt="[花心]" />&nbsp;想抓走几只么？<img src="http://img.t.sinajs.cn/t4/appstyle/expression/ext/normal/6a/laugh.gif" title="[哈哈]" alt="[哈哈]" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518ab264ea5.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518ab8b2b1c.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518ac134c8a.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518ac7049fc.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518acceba92.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518ad2b7663.jpg" alt="" /> \r\n</p>\r\n<p>\r\n	妈知道我谈恋爱了，百般要挟下我不得不把男友的照片给她看。妈看着照片说：“你确定没骗我，这是你男友？长得这么帅怎么可能成为你男友。”我听后就不高兴：“妈，你女儿也不差啊！”妈说：“我知道你不差，我不是说你配不上他，我的意思是他没有男朋友？”妈，你是不是玩微博了？！\r\n</p>\r\n<p>\r\n	<img src="/Uploads/Editor/2014-10-30/54518aea57cf6.jpg" alt="" /> \r\n</p>', '', 0);
/*!40000 ALTER TABLE `lh_document_article` ENABLE KEYS */;


-- 导出  表 lehe.lh_document_download 结构
CREATE TABLE IF NOT EXISTS `lh_document_download` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文档ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容解析类型',
  `content` text NOT NULL COMMENT '下载详细描述',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '详情页显示模板',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文档模型下载表';

-- 正在导出表  lehe.lh_document_download 的数据：0 rows
DELETE FROM `lh_document_download`;
/*!40000 ALTER TABLE `lh_document_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_document_download` ENABLE KEYS */;


-- 导出  表 lehe.lh_file 结构
CREATE TABLE IF NOT EXISTS `lh_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '远程地址',
  `create_time` int(10) unsigned NOT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文件表';

-- 正在导出表  lehe.lh_file 的数据：0 rows
DELETE FROM `lh_file`;
/*!40000 ALTER TABLE `lh_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_file` ENABLE KEYS */;


-- 导出  表 lehe.lh_forum 结构
CREATE TABLE IF NOT EXISTS `lh_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '创建者',
  `title` text NOT NULL COMMENT '名称',
  `allow_user_group` varchar(500) NOT NULL COMMENT '允许用户组',
  `post_count` int(11) NOT NULL COMMENT '帖子总数',
  `status` int(11) NOT NULL COMMENT '数据状态，-1 已删除，0 被禁用，1 正常',
  `sort` int(11) NOT NULL COMMENT '排序',
  `logo` varchar(255) NOT NULL COMMENT 'logo图片',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='版块表';

-- 正在导出表  lehe.lh_forum 的数据：~0 rows (大约)
DELETE FROM `lh_forum`;
/*!40000 ALTER TABLE `lh_forum` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_forum` ENABLE KEYS */;


-- 导出  表 lehe.lh_forum_bookmark 结构
CREATE TABLE IF NOT EXISTS `lh_forum_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `post_id` int(11) NOT NULL COMMENT '帖子id',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='版块标记';

-- 正在导出表  lehe.lh_forum_bookmark 的数据：~0 rows (大约)
DELETE FROM `lh_forum_bookmark`;
/*!40000 ALTER TABLE `lh_forum_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_forum_bookmark` ENABLE KEYS */;


-- 导出  表 lehe.lh_forum_lzl_reply 结构
CREATE TABLE IF NOT EXISTS `lh_forum_lzl_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `post_id` int(11) NOT NULL COMMENT '帖子id',
  `to_f_reply_id` int(11) NOT NULL COMMENT '版块的id',
  `to_reply_id` int(11) NOT NULL COMMENT '回复id',
  `content` text NOT NULL COMMENT '内容',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `to_uid` int(11) NOT NULL COMMENT '回复的uid',
  `ctime` int(11) NOT NULL COMMENT '创建时间',
  `is_del` tinyint(4) NOT NULL COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='发帖人回复';

-- 正在导出表  lehe.lh_forum_lzl_reply 的数据：0 rows
DELETE FROM `lh_forum_lzl_reply`;
/*!40000 ALTER TABLE `lh_forum_lzl_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_forum_lzl_reply` ENABLE KEYS */;


-- 导出  表 lehe.lh_forum_post 结构
CREATE TABLE IF NOT EXISTS `lh_forum_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `forum_id` int(11) NOT NULL COMMENT '版块id',
  `title` text NOT NULL COMMENT '标题',
  `parse` int(11) NOT NULL COMMENT '解析',
  `content` text NOT NULL COMMENT '内容',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '数据状态，-1 已删除，0 被禁用，1 正常',
  `last_reply_time` int(11) NOT NULL COMMENT '最后回复时间',
  `view_count` int(11) NOT NULL DEFAULT '0' COMMENT '查看总数',
  `reply_count` int(11) NOT NULL DEFAULT '0' COMMENT '回复总数',
  `is_top` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否置顶，1是版块内置顶，2是全局置顶，0不是置顶',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帖子表';

-- 正在导出表  lehe.lh_forum_post 的数据：~0 rows (大约)
DELETE FROM `lh_forum_post`;
/*!40000 ALTER TABLE `lh_forum_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_forum_post` ENABLE KEYS */;


-- 导出  表 lehe.lh_forum_post_reply 结构
CREATE TABLE IF NOT EXISTS `lh_forum_post_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT 'uid',
  `post_id` int(11) NOT NULL COMMENT '帖子id',
  `parse` int(11) NOT NULL COMMENT '解析',
  `content` text NOT NULL COMMENT '内容',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `status` int(11) NOT NULL COMMENT '数据状态，-1 已删除，0 被禁用，1 正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帖子回复表';

-- 正在导出表  lehe.lh_forum_post_reply 的数据：~0 rows (大约)
DELETE FROM `lh_forum_post_reply`;
/*!40000 ALTER TABLE `lh_forum_post_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_forum_post_reply` ENABLE KEYS */;


-- 导出  表 lehe.lh_hooks 结构
CREATE TABLE IF NOT EXISTS `lh_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '数据状态，-1 已删除，0 被禁用，1 正常，2 未审核，3是草稿箱',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_hooks 的数据：12 rows
DELETE FROM `lh_hooks`;
/*!40000 ALTER TABLE `lh_hooks` DISABLE KEYS */;
INSERT INTO `lh_hooks` (`id`, `name`, `description`, `type`, `update_time`, `addons`, `status`) VALUES
	(1, 'pageHeader', '页面header钩子，一般用于加载插件CSS文件和代码', 1, 0, '', 1),
	(2, 'pageFooter', '页面footer钩子，一般用于加载插件JS文件和JS代码', 1, 0, 'ReturnTop,SuperLinks', 1),
	(3, 'documentEditForm', '添加编辑表单的 扩展内容钩子', 1, 0, 'Attachment', 1),
	(4, 'documentDetailAfter', '文档末尾显示', 1, 0, 'Attachment,SocialComment', 1),
	(5, 'documentDetailBefore', '页面内容前显示用钩子', 1, 0, '', 1),
	(6, 'documentSaveComplete', '保存文档数据后的扩展钩子', 2, 0, 'Attachment', 1),
	(7, 'documentEditFormContent', '添加编辑表单的内容显示钩子', 1, 0, 'Editor', 1),
	(8, 'adminArticleEdit', '后台内容编辑页编辑器', 1, 1378982734, 'EditorForAdmin', 1),
	(13, 'AdminIndex', '首页小格子个性化显示', 1, 1382596073, 'SiteStat,SystemInfo,DevTeam', 1),
	(14, 'topicComment', '评论提交方式扩展钩子。', 1, 1380163518, 'Editor', 1),
	(16, 'app_begin', '应用开始', 2, 1384481614, '', 1),
	(17, 'friendLink', '友情链接', 1, 1413523480, 'SuperLinks', 1);
/*!40000 ALTER TABLE `lh_hooks` ENABLE KEYS */;


-- 导出  表 lehe.lh_member 结构
CREATE TABLE IF NOT EXISTS `lh_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵称',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性别,0保密1男2女',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq号',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '会员状态',
  `email_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '邮箱激活状态：1是激活，0是未激活',
  `phone_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '手机认证状态：1是认证，0是未认证',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- 正在导出表  lehe.lh_member 的数据：13 rows
DELETE FROM `lh_member`;
/*!40000 ALTER TABLE `lh_member` DISABLE KEYS */;
INSERT INTO `lh_member` (`uid`, `nickname`, `sex`, `birthday`, `qq`, `score`, `login`, `reg_ip`, `reg_time`, `last_login_ip`, `last_login_time`, `status`, `email_status`, `phone_status`) VALUES
	(1, 'admin', 0, '0000-00-00', '', 290, 121, 0, 1407071022, 2130706433, 1446308411, 1, 0, 0),
	(2, 'administrator', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0),
	(3, 'root', 0, '0000-00-00', '', 10, 1, 0, 0, 1944532378, 1407198733, 1, 0, 0),
	(4, 'eryang', 0, '0000-00-00', '', 50, 40, 0, 0, 1944532378, 1408523951, 1, 0, 0),
	(5, '乐呵', 0, '0000-00-00', '', 50, 27, 0, 0, 1944532378, 1413350468, 1, 0, 0),
	(6, '乐呵乐呵', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0),
	(7, 'lehe', 0, '0000-00-00', '', 60, 10, 0, 0, 1944532378, 1414628384, 1, 0, 0),
	(8, 'lh', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0),
	(9, 'lehelehe', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0),
	(10, '匿名', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0),
	(11, 'niming', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0),
	(20, '123456', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0),
	(21, '1234567', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1, 0, 0);
/*!40000 ALTER TABLE `lh_member` ENABLE KEYS */;


-- 导出  表 lehe.lh_menu 结构
CREATE TABLE IF NOT EXISTS `lh_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文档ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同级有效）',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
  `group` varchar(50) DEFAULT '' COMMENT '分组',
  `is_dev` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否仅开发者模式可见',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_menu 的数据：137 rows
DELETE FROM `lh_menu`;
/*!40000 ALTER TABLE `lh_menu` DISABLE KEYS */;
INSERT INTO `lh_menu` (`id`, `title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `status`) VALUES
	(1, '首页', 0, 1, 'Index/index', 0, '', '', 0, 1),
	(2, '内容', 0, 2, 'Article/index', 0, '', '', 0, 1),
	(3, '文档列表', 2, 0, 'article/index', 1, '', '内容', 0, 1),
	(4, '新增', 3, 0, 'article/add', 0, '', '', 0, 1),
	(5, '编辑', 3, 0, 'article/edit', 0, '', '', 0, 1),
	(6, '改变状态', 3, 0, 'article/setStatus', 0, '', '', 0, 1),
	(7, '保存', 3, 0, 'article/update', 0, '', '', 0, 1),
	(8, '保存草稿', 3, 0, 'article/autoSave', 0, '', '', 0, 1),
	(9, '移动', 3, 0, 'article/move', 0, '', '', 0, 1),
	(10, '复制', 3, 0, 'article/copy', 0, '', '', 0, 1),
	(11, '粘贴', 3, 0, 'article/paste', 0, '', '', 0, 1),
	(12, '导入', 3, 0, 'article/batchOperate', 0, '', '', 0, 1),
	(13, '回收站', 2, 0, 'article/recycle', 1, '', '内容', 0, 1),
	(14, '还原', 13, 0, 'article/permit', 0, '', '', 0, 1),
	(15, '清空', 13, 0, 'article/clear', 0, '', '', 0, 1),
	(16, '用户', 0, 3, 'User/index', 0, '', '', 0, 1),
	(17, '用户信息', 16, 0, 'User/index', 0, '', '用户管理', 0, 1),
	(18, '新增用户', 17, 0, 'User/add', 0, '添加新用户', '', 0, 1),
	(19, '用户行为', 16, 0, 'User/action', 0, '', '行为管理', 0, 1),
	(20, '新增用户行为', 19, 0, 'User/addaction', 0, '', '', 0, 1),
	(21, '编辑用户行为', 19, 0, 'User/editaction', 0, '', '', 0, 1),
	(22, '保存用户行为', 19, 0, 'User/saveAction', 0, '"用户->用户行为"保存编辑和新增的用户行为', '', 0, 1),
	(23, '变更行为状态', 19, 0, 'User/setStatus', 0, '"用户->用户行为"中的启用,禁用和删除权限', '', 0, 1),
	(24, '禁用会员', 19, 0, 'User/changeStatus?method=forbidUser', 0, '"用户->用户信息"中的禁用', '', 0, 1),
	(25, '启用会员', 19, 0, 'User/changeStatus?method=resumeUser', 0, '"用户->用户信息"中的启用', '', 0, 1),
	(26, '删除会员', 19, 0, 'User/changeStatus?method=deleteUser', 0, '"用户->用户信息"中的删除', '', 0, 1),
	(27, '权限管理', 16, 0, 'AuthManager/index', 0, '', '用户管理', 0, 1),
	(28, '删除', 27, 0, 'AuthManager/changeStatus?method=deleteGroup', 0, '删除用户组', '', 0, 1),
	(29, '禁用', 27, 0, 'AuthManager/changeStatus?method=forbidGroup', 0, '禁用用户组', '', 0, 1),
	(30, '恢复', 27, 0, 'AuthManager/changeStatus?method=resumeGroup', 0, '恢复已禁用的用户组', '', 0, 1),
	(31, '新增', 27, 0, 'AuthManager/createGroup', 0, '创建新的用户组', '', 0, 1),
	(32, '编辑', 27, 0, 'AuthManager/editGroup', 0, '编辑用户组名称和描述', '', 0, 1),
	(33, '保存用户组', 27, 0, 'AuthManager/writeGroup', 0, '新增和编辑用户组的"保存"按钮', '', 0, 1),
	(34, '授权', 27, 0, 'AuthManager/group', 0, '"后台 \\ 用户 \\ 用户信息"列表页的"授权"操作按钮,用于设置用户所属用户组', '', 0, 1),
	(35, '访问授权', 27, 0, 'AuthManager/access', 0, '"后台 \\ 用户 \\ 权限管理"列表页的"访问授权"操作按钮', '', 0, 1),
	(36, '成员授权', 27, 0, 'AuthManager/user', 0, '"后台 \\ 用户 \\ 权限管理"列表页的"成员授权"操作按钮', '', 0, 1),
	(37, '解除授权', 27, 0, 'AuthManager/removeFromGroup', 0, '"成员授权"列表页内的解除授权操作按钮', '', 0, 1),
	(38, '保存成员授权', 27, 0, 'AuthManager/addToGroup', 0, '"用户信息"列表页"授权"时的"保存"按钮和"成员授权"里右上角的"添加"按钮)', '', 0, 1),
	(39, '分类授权', 27, 0, 'AuthManager/category', 0, '"后台 \\ 用户 \\ 权限管理"列表页的"分类授权"操作按钮', '', 0, 1),
	(40, '保存分类授权', 27, 0, 'AuthManager/addToCategory', 0, '"分类授权"页面的"保存"按钮', '', 0, 1),
	(41, '模型授权', 27, 0, 'AuthManager/modelauth', 0, '"后台 \\ 用户 \\ 权限管理"列表页的"模型授权"操作按钮', '', 0, 1),
	(42, '保存模型授权', 27, 0, 'AuthManager/addToModel', 0, '"分类授权"页面的"保存"按钮', '', 0, 1),
	(43, '扩展', 0, 7, 'Addons/index', 0, '', '', 0, 1),
	(44, '插件管理', 43, 1, 'Addons/index', 0, '', '扩展', 0, 1),
	(45, '创建', 44, 0, 'Addons/create', 0, '服务器上创建插件结构向导', '', 0, 1),
	(46, '检测创建', 44, 0, 'Addons/checkForm', 0, '检测插件是否可以创建', '', 0, 1),
	(47, '预览', 44, 0, 'Addons/preview', 0, '预览插件定义类文件', '', 0, 1),
	(48, '快速生成插件', 44, 0, 'Addons/build', 0, '开始生成插件结构', '', 0, 1),
	(49, '设置', 44, 0, 'Addons/config', 0, '设置插件配置', '', 0, 1),
	(50, '禁用', 44, 0, 'Addons/disable', 0, '禁用插件', '', 0, 1),
	(51, '启用', 44, 0, 'Addons/enable', 0, '启用插件', '', 0, 1),
	(52, '安装', 44, 0, 'Addons/install', 0, '安装插件', '', 0, 1),
	(53, '卸载', 44, 0, 'Addons/uninstall', 0, '卸载插件', '', 0, 1),
	(54, '更新配置', 44, 0, 'Addons/saveconfig', 0, '更新插件配置处理', '', 0, 1),
	(55, '插件后台列表', 44, 0, 'Addons/adminList', 0, '', '', 0, 1),
	(56, 'URL方式访问插件', 44, 0, 'Addons/execute', 0, '控制是否有权限通过url访问插件控制器方法', '', 0, 1),
	(57, '钩子管理', 43, 2, 'Addons/hooks', 0, '', '扩展', 0, 1),
	(58, '模型管理', 68, 3, 'Model/index', 0, '', '系统设置', 0, 0),
	(59, '新增', 58, 0, 'model/add', 0, '', '', 0, 1),
	(60, '编辑', 58, 0, 'model/edit', 0, '', '', 0, 1),
	(61, '改变状态', 58, 0, 'model/setStatus', 0, '', '', 0, 1),
	(62, '保存数据', 58, 0, 'model/update', 0, '', '', 0, 1),
	(63, '属性管理', 68, 0, 'Attribute/index', 1, '网站属性配置。', '', 0, 1),
	(64, '新增', 63, 0, 'Attribute/add', 0, '', '', 0, 1),
	(65, '编辑', 63, 0, 'Attribute/edit', 0, '', '', 0, 1),
	(66, '改变状态', 63, 0, 'Attribute/setStatus', 0, '', '', 0, 1),
	(67, '保存数据', 63, 0, 'Attribute/update', 0, '', '', 0, 1),
	(68, '系统', 0, 4, 'Config/group', 0, '', '', 0, 1),
	(69, '网站设置', 68, 1, 'Config/group', 0, '', '系统设置', 0, 1),
	(70, '配置管理', 68, 4, 'Config/index', 0, '', '系统设置', 0, 1),
	(71, '编辑', 70, 0, 'Config/edit', 0, '新增编辑和保存配置', '', 0, 1),
	(72, '删除', 70, 0, 'Config/del', 0, '删除配置', '', 0, 1),
	(73, '新增', 70, 0, 'Config/add', 0, '新增配置', '', 0, 1),
	(74, '保存', 70, 0, 'Config/save', 0, '保存配置', '', 0, 1),
	(75, '菜单管理', 68, 5, 'Menu/index', 0, '', '系统设置', 0, 1),
	(76, '导航管理', 68, 6, 'Channel/index', 0, '', '系统设置', 0, 1),
	(77, '新增', 76, 0, 'Channel/add', 0, '', '', 0, 1),
	(78, '编辑', 76, 0, 'Channel/edit', 0, '', '', 0, 1),
	(79, '删除', 76, 0, 'Channel/del', 0, '', '', 0, 1),
	(80, '分类管理', 68, 2, 'Category/index', 0, '', '系统设置', 0, 1),
	(81, '编辑', 80, 0, 'Category/edit', 0, '编辑和保存栏目分类', '', 0, 1),
	(82, '新增', 80, 0, 'Category/add', 0, '新增栏目分类', '', 0, 1),
	(83, '删除', 80, 0, 'Category/remove', 0, '删除栏目分类', '', 0, 1),
	(84, '移动', 80, 0, 'Category/operate/type/move', 0, '移动栏目分类', '', 0, 1),
	(85, '合并', 80, 0, 'Category/operate/type/merge', 0, '合并栏目分类', '', 0, 1),
	(86, '备份数据库', 68, 0, 'Database/index?type=export', 0, '', '数据备份', 0, 1),
	(87, '备份', 86, 0, 'Database/export', 0, '备份数据库', '', 0, 1),
	(88, '优化表', 86, 0, 'Database/optimize', 0, '优化数据表', '', 0, 1),
	(89, '修复表', 86, 0, 'Database/repair', 0, '修复数据表', '', 0, 1),
	(90, '还原数据库', 68, 0, 'Database/index?type=import', 0, '', '数据备份', 0, 1),
	(91, '恢复', 90, 0, 'Database/import', 0, '数据库恢复', '', 0, 1),
	(92, '删除', 90, 0, 'Database/del', 0, '删除备份文件', '', 0, 1),
	(93, '其他', 0, 5, 'other', 1, '', '', 0, 1),
	(96, '新增', 75, 0, 'Menu/add', 0, '', '系统设置', 0, 1),
	(98, '编辑', 75, 0, 'Menu/edit', 0, '', '', 0, 1),
	(106, '行为日志', 16, 0, 'Action/actionlog', 0, '', '行为管理', 0, 1),
	(108, '修改密码', 16, 0, 'User/updatePassword', 1, '', '', 0, 1),
	(109, '修改昵称', 16, 0, 'User/updateNickname', 1, '', '', 0, 1),
	(110, '查看行为日志', 106, 0, 'action/edit', 1, '', '', 0, 1),
	(112, '新增数据', 58, 0, 'think/add', 1, '', '', 0, 1),
	(113, '编辑数据', 58, 0, 'think/edit', 1, '', '', 0, 1),
	(114, '导入', 75, 0, 'Menu/import', 0, '', '', 0, 1),
	(115, '生成', 58, 0, 'Model/generate', 0, '', '', 0, 1),
	(116, '新增钩子', 57, 0, 'Addons/addHook', 0, '', '', 0, 1),
	(117, '编辑钩子', 57, 0, 'Addons/edithook', 0, '', '', 0, 1),
	(118, '文档排序', 3, 0, 'Article/sort', 1, '', '', 0, 1),
	(119, '排序', 70, 0, 'Config/sort', 1, '', '', 0, 1),
	(120, '排序', 75, 0, 'Menu/sort', 1, '', '', 0, 1),
	(121, '排序', 76, 0, 'Channel/sort', 1, '', '', 0, 1),
	(122, '数据列表', 58, 0, 'think/lists', 1, '', '', 0, 1),
	(123, '规则管理', 68, 0, 'Seo/index', 0, '', 'SEO规则', 0, 1),
	(124, '新增', 123, 0, 'Seo/add', 0, '', '', 0, 1),
	(125, '编辑', 123, 0, 'Seo/edit', 0, '', '', 0, 1),
	(126, '规则回收站', 68, 0, 'Seo/recycle', 0, '', 'SEO规则', 0, 1),
	(128, '我的文档', 2, 0, 'article/mydocument', 1, '', '', 0, 1),
	(129, '文档列表', 2, 0, 'article/index', 1, '', '内容', 0, 1),
	(130, '邮件测试', 68, 0, 'Test/email', 0, '', '测试', 0, 1),
	(131, '清除缓存', 68, 0, 'Cache/delcache', 0, '', '缓存', 0, 0),
	(132, '审核列表', 3, 0, 'Article/examine', 1, '', '', 0, 1),
	(133, '贴吧', 0, 4, 'Forum/index', 0, '', '', 0, 1),
	(134, '版块管理', 133, 1, 'Forum/forum', 0, '', '版块', 0, 1),
	(135, '新增版块', 134, 0, 'Forum/add', 0, '', '', 0, 1),
	(136, '编辑版块', 134, 0, 'Forum/edit', 0, '', '', 0, 1),
	(137, '版块排序', 134, 0, 'Forum/sort', 0, '', '', 0, 1),
	(138, '版块回收站', 133, 2, 'Forum/forumTrash', 0, '', '版块', 0, 1),
	(139, '清空', 138, 0, 'Forum/clear', 0, '', '', 0, 1),
	(140, '还原', 138, 0, 'Forum/permit', 0, '', '', 0, 1),
	(141, '帖子管理', 133, 3, 'Forum/post', 0, '', '帖子', 0, 1),
	(142, '编辑帖子', 141, 0, 'Forum/editPost', 0, '', '', 0, 1),
	(143, '帖子回收站', 133, 4, 'Forum/postTrash', 0, '', '帖子', 0, 1),
	(144, '还原', 143, 0, 'Forum/postPermit', 0, '', '', 0, 1),
	(145, '清空', 143, 0, 'Forum/postClear', 0, '', '', 0, 1),
	(146, '回复管理', 133, 5, 'Forum/reply', 0, '', '回复', 0, 1),
	(147, '编辑回复', 146, 0, 'Forum/editReply', 0, '', '', 0, 1),
	(148, '回复回收站', 133, 6, 'Forum/replyTrash', 0, '', '回复', 0, 1),
	(149, '还原', 148, 0, 'Forum/replyPermit', 0, '', '', 0, 1),
	(150, '清空', 148, 0, 'Forum/replyClear', 0, '', '', 0, 1);
/*!40000 ALTER TABLE `lh_menu` ENABLE KEYS */;


-- 导出  表 lehe.lh_model 结构
CREATE TABLE IF NOT EXISTS `lh_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '模型标识',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '模型名称',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '继承的模型',
  `relation` varchar(30) NOT NULL DEFAULT '' COMMENT '继承与被继承模型的关联字段',
  `need_pk` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新建表时是否需要主键字段',
  `field_sort` text NOT NULL COMMENT '表单字段排序',
  `field_group` varchar(255) NOT NULL DEFAULT '1:基础' COMMENT '字段分组',
  `attribute_list` text NOT NULL COMMENT '属性列表（表的字段）',
  `attribute_alias` varchar(255) NOT NULL DEFAULT '' COMMENT '属性别名定义',
  `template_list` varchar(100) NOT NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) NOT NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '编辑模板',
  `list_grid` text NOT NULL COMMENT '列表定义',
  `list_row` smallint(2) unsigned NOT NULL DEFAULT '10' COMMENT '列表数据长度',
  `search_key` varchar(50) NOT NULL DEFAULT '' COMMENT '默认搜索字段',
  `search_list` varchar(255) NOT NULL DEFAULT '' COMMENT '高级搜索的字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `engine_type` varchar(25) NOT NULL DEFAULT 'MyISAM' COMMENT '数据库引擎',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='文档模型表';

-- 正在导出表  lehe.lh_model 的数据：3 rows
DELETE FROM `lh_model`;
/*!40000 ALTER TABLE `lh_model` DISABLE KEYS */;
INSERT INTO `lh_model` (`id`, `name`, `title`, `extend`, `relation`, `need_pk`, `field_sort`, `field_group`, `attribute_list`, `attribute_alias`, `template_list`, `template_add`, `template_edit`, `list_grid`, `list_row`, `search_key`, `search_list`, `create_time`, `update_time`, `status`, `engine_type`) VALUES
	(1, 'document', '基础文档', 0, '', 1, '{"1":["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22"]}', '1:基础', '', '', '', '', '', 'id:编号\r\ntitle:标题:[TITLE]\r\ntype:类型\r\nupdate_time:最后更新\r\nstatus:状态\r\nview:浏览\r\nid:操作:[EDIT]|编辑,[DELETE]|删除', 0, '', '', 1383891233, 1384507827, 1, 'MyISAM'),
	(2, 'article', '文章', 1, '', 1, '{"1":["3","33","5","24","34","2"],"2":["9","13","19","10","12","16","17","26","20","14","11","25"]}', '1:基础,2:扩展', '', '', '', '', '', '', 0, '', '', 1383891243, 1387260622, 1, 'MyISAM'),
	(3, 'download', '下载', 1, '', 1, '{"1":["3","28","30","32","2","5","31"],"2":["13","10","27","9","12","16","17","19","11","20","14","29"]}', '1:基础,2:扩展', '', '', '', '', '', '', 0, '', '', 1383891252, 1387260449, 1, 'MyISAM');
/*!40000 ALTER TABLE `lh_model` ENABLE KEYS */;


-- 导出  表 lehe.lh_picture 结构
CREATE TABLE IF NOT EXISTS `lh_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片链接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_picture 的数据：7 rows
DELETE FROM `lh_picture`;
/*!40000 ALTER TABLE `lh_picture` DISABLE KEYS */;
INSERT INTO `lh_picture` (`id`, `path`, `url`, `md5`, `sha1`, `status`, `create_time`) VALUES
	(1, '/Uploads/Picture/2014-10-15/543dbd7236370.jpg', '', 'db2f185c953d9b853d35d72107cbaf64', '178c4a0d055a243406d5602503add876564161d3', 1, 1413332338),
	(2, '/Uploads/Picture/2014-10-15/543de51c82018.jpg', '', '3d10b05109cf0ce70c1bc033f9245c3a', 'e5bdc802b96066156c2fa43aef3e3881208f32bf', 1, 1413342492),
	(3, '/Uploads/Picture/2014-10-16/543f1e5407a28.jpg', '', 'a1e076d85a7286f9231f6fc17a6e20e9', 'af9ccc9bb2edfb93a40de7f1784e1070ac7f55d6', 1, 1413422675),
	(4, '/Uploads/Picture/2014-10-17/54406231c9fa0.jpg', '', '1b4dcaf9fb387bb8c896cad72fd0f65b', '8a3516c6ee287ae68dcf233421706452bb7e40d2', 1, 1413505585),
	(5, '/Uploads/Picture/2014-10-28/544ee27d8cbd1.jpg', '', 'ae13ace4f5af99a835ea8ffa87995cfd', '2310bcf6027b9d45458dd64cc975cae60d7b8ca6', 1, 1414455933),
	(6, '/Uploads/Picture/2014-10-29/5450a8c55ff93.jpg', '', '55154cafb482e88b9f70244feff2950f', '851efe8f096c33825c48bbf8e8df006a5007af42', 1, 1414572229),
	(7, '/Uploads/Picture/2014-10-30/54518449d568c.jpg', '', 'e2a1d641d43d9625c3b3da6d7eb8b37e', 'cd1c1de5a791d885e049dd75fb5bcc5ef3f7e7d9', 1, 1414628425);
/*!40000 ALTER TABLE `lh_picture` ENABLE KEYS */;


-- 导出  表 lehe.lh_seo 结构
CREATE TABLE IF NOT EXISTS `lh_seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `model` varchar(40) DEFAULT NULL COMMENT '模块',
  `controller` varchar(40) DEFAULT NULL COMMENT '控制器',
  `method` varchar(40) DEFAULT NULL COMMENT '方法',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态，-1 已删除，0 被禁用，1 正常，2 未审核',
  `seo_keywords` text NOT NULL COMMENT 'SEO关键字',
  `seo_description` text NOT NULL COMMENT 'SEO描述',
  `seo_title` text NOT NULL COMMENT 'SEO标题',
  `sort` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='seo规则表';

-- 正在导出表  lehe.lh_seo 的数据：~2 rows (大约)
DELETE FROM `lh_seo`;
/*!40000 ALTER TABLE `lh_seo` DISABLE KEYS */;
INSERT INTO `lh_seo` (`id`, `title`, `model`, `controller`, `method`, `status`, `seo_keywords`, `seo_description`, `seo_title`, `sort`) VALUES
	(1, '前台首页', 'Home', '', '', 1, '有趣,乐呵', '有趣的事', '有趣的事', 0),
	(2, '个人中心', 'Usercenter', '', '', 1, '{$user.nickname|op_t}的用户中心', '{$user.nickname|op_t}的用户中心', '{$user.nickname|op_t}的用户中心', 0);
/*!40000 ALTER TABLE `lh_seo` ENABLE KEYS */;


-- 导出  表 lehe.lh_super_links 结构
CREATE TABLE IF NOT EXISTS `lh_super_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '类别（1：图片，2：普通）',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '站点名称',
  `cover_id` int(10) NOT NULL COMMENT '图片ID',
  `link` char(140) NOT NULL DEFAULT '' COMMENT '链接地址',
  `level` int(3) unsigned NOT NULL DEFAULT '0' COMMENT '优先级',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态（0：禁用，1：正常）',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='友情连接表';

-- 正在导出表  lehe.lh_super_links 的数据：5 rows
DELETE FROM `lh_super_links`;
/*!40000 ALTER TABLE `lh_super_links` DISABLE KEYS */;
INSERT INTO `lh_super_links` (`id`, `type`, `title`, `cover_id`, `link`, `level`, `status`, `create_time`) VALUES
	(6, 2, '百度', 0, 'http://www.baidu.com', 0, 1, 1413524439),
	(5, 2, '网易', 0, 'http://www.163.com', 0, 1, 1413523783),
	(7, 2, '腾讯', 0, 'http://www.qq.com', 0, 1, 1413524458),
	(8, 2, '新浪', 0, 'http://www.sina.com.cn', 0, 1, 1413524501),
	(9, 2, '360导航', 0, 'http://hao.360.cn', 0, 1, 1413524531);
/*!40000 ALTER TABLE `lh_super_links` ENABLE KEYS */;


-- 导出  表 lehe.lh_ucenter_admin 结构
CREATE TABLE IF NOT EXISTS `lh_ucenter_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员用户ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理员状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- 正在导出表  lehe.lh_ucenter_admin 的数据：0 rows
DELETE FROM `lh_ucenter_admin`;
/*!40000 ALTER TABLE `lh_ucenter_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_ucenter_admin` ENABLE KEYS */;


-- 导出  表 lehe.lh_ucenter_app 结构
CREATE TABLE IF NOT EXISTS `lh_ucenter_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应用ID',
  `title` varchar(30) NOT NULL COMMENT '应用名称',
  `url` varchar(100) NOT NULL COMMENT '应用URL',
  `ip` char(15) NOT NULL COMMENT '应用IP',
  `auth_key` varchar(100) NOT NULL COMMENT '加密KEY',
  `sys_login` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '同步登陆',
  `allow_ip` varchar(255) NOT NULL COMMENT '允许访问的IP',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '应用状态',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='应用表';

-- 正在导出表  lehe.lh_ucenter_app 的数据：0 rows
DELETE FROM `lh_ucenter_app`;
/*!40000 ALTER TABLE `lh_ucenter_app` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_ucenter_app` ENABLE KEYS */;


-- 导出  表 lehe.lh_ucenter_member 结构
CREATE TABLE IF NOT EXISTS `lh_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` char(16) NOT NULL COMMENT '用户名',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` char(32) NOT NULL COMMENT '用户邮箱',
  `mobile` char(15) NOT NULL COMMENT '用户手机',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '用户状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 正在导出表  lehe.lh_ucenter_member 的数据：13 rows
DELETE FROM `lh_ucenter_member`;
/*!40000 ALTER TABLE `lh_ucenter_member` DISABLE KEYS */;
INSERT INTO `lh_ucenter_member` (`id`, `username`, `password`, `email`, `mobile`, `reg_time`, `reg_ip`, `last_login_time`, `last_login_ip`, `update_time`, `status`) VALUES
	(1, 'admin', 'dd580120692859a532007ee8e23a627b', 'admin@qq.com', '', 1407198334, 1944532378, 1446308411, 2130706433, 1407198334, 1),
	(2, 'administrator', 'dd580120692859a532007ee8e23a627b', 'administrator@lehe.so', '', 1407162312, 2130706433, 0, 0, 1407162312, 1),
	(3, 'root', 'dd580120692859a532007ee8e23a627b', 'root@lehe.so', '', 1407162327, 2130706433, 1407198733, 1944532378, 1407162327, 1),
	(4, 'eryang', 'dd580120692859a532007ee8e23a627b', 'eryang@lehe.so', '', 1407162346, 2130706433, 1408523951, 1944532378, 1407162346, 1),
	(5, '乐呵', 'dd580120692859a532007ee8e23a627b', 'lehe@lehe.s', '', 1407898846, 1944532378, 1413350468, 1944532378, 1407898846, 1),
	(6, '乐呵乐呵', 'dd580120692859a532007ee8e23a627b', 'lehelehe@lehe.so', '', 1407898888, 1944532378, 0, 0, 1407898888, 1),
	(7, 'lehe', 'dd580120692859a532007ee8e23a627b', '707069100@qq.com', '', 1407898931, 1944532378, 1414628384, 1944532378, 1407898931, 1),
	(8, 'lh', 'dd580120692859a532007ee8e23a627b', 'lh@lehe.so', '', 1407898948, 1944532378, 0, 0, 1407898948, 1),
	(9, 'lehelehe', 'dd580120692859a532007ee8e23a627b', 'leh@lehe.so', '', 1407898975, 1944532378, 0, 0, 1407898975, 1),
	(10, '匿名', 'dd580120692859a532007ee8e23a627b', 'niming@lehe.so', '', 1408351445, 1944532378, 0, 0, 1408351445, 1),
	(11, 'niming', 'dd580120692859a532007ee8e23a627b', 'niming@163.com', '', 1408351468, 1944532378, 0, 0, 1408351468, 1),
	(20, '123456', 'dd580120692859a532007ee8e23a627b', '123456@qq.com', '', 1415329132, 2130706433, 0, 0, 1415329132, 1),
	(21, '1234567', 'dd580120692859a532007ee8e23a627b', '1234567@qq.com', '', 1446308376, 2130706433, 0, 0, 1446308376, 1);
/*!40000 ALTER TABLE `lh_ucenter_member` ENABLE KEYS */;


-- 导出  表 lehe.lh_ucenter_setting 结构
CREATE TABLE IF NOT EXISTS `lh_ucenter_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '设置ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置类型（1-用户配置）',
  `value` text NOT NULL COMMENT '配置数据',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='设置表';

-- 正在导出表  lehe.lh_ucenter_setting 的数据：0 rows
DELETE FROM `lh_ucenter_setting`;
/*!40000 ALTER TABLE `lh_ucenter_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_ucenter_setting` ENABLE KEYS */;


-- 导出  表 lehe.lh_url 结构
CREATE TABLE IF NOT EXISTS `lh_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '链接唯一标识',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短网址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='链接表';

-- 正在导出表  lehe.lh_url 的数据：0 rows
DELETE FROM `lh_url`;
/*!40000 ALTER TABLE `lh_url` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_url` ENABLE KEYS */;


-- 导出  表 lehe.lh_userdata 结构
CREATE TABLE IF NOT EXISTS `lh_userdata` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(3) unsigned NOT NULL COMMENT '类型标识',
  `target_id` int(10) unsigned NOT NULL COMMENT '目标id',
  UNIQUE KEY `uid` (`uid`,`type`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 正在导出表  lehe.lh_userdata 的数据：0 rows
DELETE FROM `lh_userdata`;
/*!40000 ALTER TABLE `lh_userdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_userdata` ENABLE KEYS */;


-- 导出  表 lehe.lh_user_token 结构
CREATE TABLE IF NOT EXISTS `lh_user_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `token` varchar(255) NOT NULL COMMENT '标记',
  `time` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户登录标记表';

-- 正在导出表  lehe.lh_user_token 的数据：0 rows
DELETE FROM `lh_user_token`;
/*!40000 ALTER TABLE `lh_user_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `lh_user_token` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
