var Login = function () {

    var handleLogin = function() {

        $('.login-form').validate({
            errorElement: 'span', //容器默认输入错误消息
            errorClass: 'help-block', // 默认的输入错误消息类
            focusInvalid: false, //不关注最后一无效输入
            rules: {
                username: {
                    required: true,
                    minlength:2,
                    maxlength:18,
                    chinese:'只能包含中文、英文字母、数字、下划线、破折号'
                },
                password: {
                    required: true,
                    minlength:6,
                    maxlength:18
                },
                verify: {
                    required: true,
                    minlength:5,
                    maxlength:5,
                    chrnum:'只能输入数字和字母'
                }
            },

            messages: {
                username: {
                    required: '用户名必须填写',
                    minlength:'最少长度是2',
                    maxlength:'最大长度是18',
                    chinese:'只能包含中文、英文字母、数字、下划线、破折号'
                },
                password: {
                    required: '密码必须填写',
                    minlength:'最少长度是6',
                    maxlength:'最大长度是18'

                },
                verify: {
                    required: '验证码必须填写',
                    minlength:'最少长度是5',
                    maxlength:'最大长度是5',
                    chrnum_sign:'只能输入数字和字母'
                }
            },

            invalidHandler: function (event, validator) { //显示在表单提交错误提示
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) { // 标出错误的输入
                $(element)
                    .closest('.form-group').addClass('has-error'); // 设置错误类
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                form = $('.login-form');
                buttonLogin(form);
            }
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    var form = $('.login-form');
                    buttonLogin(form);
                }
                return false;
            }
        });
    }

    // ------------------------------------------------------------------------

    var  buttonLogin = function(form){
        //表单提交

        var action= form.get(0).action;
        $.post(action, form.serialize(), success, 'json');
        return false;
        function success(data){
            if(data.status){
                window.location.href = data.url;
            } else {
                if($('.alert-danger', $('.login-form')).is(":hidden")){
                    $('.alert-danger', $('.login-form')).show();
                    $('.login-form').find('.alert-danger').find('span').text(data.info);
                }else if($('.alert-danger', $('.login-form')).is(":visible")){
                    $('.login-form').find('.alert-danger').find('span').text(data.info);
                }
                //刷新验证码
                $('.verifyimg').click();
            }
        }

    }

    // ------------------------------------------------------------------------

    var  buttonVerifyImg = function(){


        //刷新验证码
        var verifyimg = $('.verifyimg').attr('src');
        $('.verifyimg').click(function(){
            if( verifyimg.indexOf('?')>0){
                $('.verifyimg').attr('src', verifyimg+'&random='+Math.random());
            }else{
                $('.verifyimg').attr('src', verifyimg.replace(/\?.*$/,'')+'?'+Math.random());
            }
        });

    }

    // ------------------------------------------------------------------------

    return {

        init: function () {
            buttonVerifyImg();
            handleLogin();
        }

    };

}();