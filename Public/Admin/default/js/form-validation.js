var FormValidation = function () {

    return {

        init: function () {

        },

        /*用户表单验证*/
        formValidationUser: function() {

            var form = $('#user_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    username: {
                        minlength: 1,
                        maxlength:16,
                        required: true
                    },
                    password: {
                        minlength: 6,
                        maxlength:30,
                        required: true
                    },
                    repassword: {
                        equalTo: '#password'
                    },
                    email: {
                        required: true,
                        email: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#user_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*昵称表单验证*/
        formValidationNickname: function() {

            var form = $('#nickname_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    password: {
                        minlength: 6,
                        maxlength:30,
                        required: true
                    },
                    nickname: {
                        minlength: 1,
                        maxlength:16,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#nickname_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------
        },

        // ------------------------------------------------------------------------

        /*密码表单验证*/
        formValidationPassword: function() {

            var form = $('#form_password');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    old: {
                        minlength: 6,
                        maxlength:30,
                        required: true
                    },
                    password: {
                        minlength: 6,
                        maxlength:30,
                        required: true
                    },
                    repassword: {
                        equalTo:'#password'
                    }

                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#form_password');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*行为表单验证*/
        formValidationAction: function() {

            var form = $('#form_action');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    name: {
                        minlength: 1,
                        maxlength:30,
                        alpha_en:true,
                        required: true
                    },
                    title: {
                        minlength: 1,
                        maxlength:80,
                        required: true
                    },
                    remark: {
                        minlength: 1,
                        maxlength:140,
                        required: true
                    }

                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#form_action');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*用户组表单验证*/
        formValidationGroup: function() {

            var form = $('#group_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        maxlength:20,
                        required: true
                    },
                    description: {
                        maxlength:80
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#group_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        /*用户授权用户组表单验证*/
        formValidationUserToGroup: function() {

            var form = $('#user_to_group_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {

                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#user_to_group_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        /*配置表单验证*/
        formValidationConfig: function() {

            var form = $('#config_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    name: {
                        minlength: 1,
                        maxlength:30,
                        required: true
                    },
                    title: {
                        minlength: 1,
                        maxlength:80,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#config_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        /*分类表单验证*/
        formValidationCategory: function() {

            var form = $('#category_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        maxlength:60,
                        required: true
                    },
                    name: {
                        minlength: 1,
                        maxlength:30,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#category_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*菜单表单验证*/
        formValidationMenu: function() {

            var form = $('#menu_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        maxlength:50,
                        required: true
                    },
                    url: {
                        minlength: 1,
                        maxlength:255,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#menu_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*模型表单验证*/
        formValidationModel: function() {

            var form = $('#model_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    name: {
                        minlength: 1,
                        maxlength:40,
                        required: true
                    },
                    title: {
                        minlength: 1,
                        maxlength:30,
                        required: true
                    },
                    url: {
                        minlength: 1,
                        maxlength:255,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#menu_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        /*导航表单验证*/
        formValidationChannel: function() {

            var form = $('#channel_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        maxlength:30,
                        required: true
                    },
                    name: {
                        minlength: 1,
                        maxlength:30,
                        required: true
                    },
                    url: {
                        minlength: 1,
                        maxlength:100,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#channel_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*钩子表单验证*/
        formValidationHooks: function() {

            var form = $('#form_hooks');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    name: {
                        minlength: 1,
                        maxlength:40,
                        required: true
                    },
                    description: {
                        minlength: 1,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#form_hooks');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*seo表单验证*/
        formValidationSeo: function() {

            var form = $('#seo_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        maxlength:50,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#seo_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        /*补丁表单验证*/
        formValidationPatch: function() {

            var form = $('#form_patch');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    sql: {
                        minlength: 1,
                        required: true
                    },
                    des:{
                        minlength: 1,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#form_patch');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        /*版块表单验证*/
        formValidationForum: function() {

            var form = $('#forum_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#forum_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },


        /*帖子表单验证*/
        formValidationPost: function() {

            var form = $('#post_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        required: true
                    },
                    content: {
                        minlength: 1,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#post_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        },

        /*帖子回复表单验证*/
        formValidationReply: function() {

            var form = $('#reply_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    content: {
                        minlength: 1,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#reply_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

        },

        // ------------------------------------------------------------------------

        /*友情链接表单验证*/
        formValidationSuperLink: function() {

            var form = $('#super_link_form');

            form.validate({
                errorElement: 'span', //默认错误消息的元素
                errorClass: 'help-block', // 默认错误消息的类
                focusInvalid: false, // 不自动移到最后一个错误输入
                ignore: '',
                rules: {
                    title: {
                        minlength: 1,
                        maxlength:30,
                        required: true
                    },
                    link: {
                        minlength: 1,
                        maxlength:100,
                        required: true
                    }
                },
                errorPlacement: function (error, element) { // 错误位置
                    error.insertAfter(element);
                },
                highlight: function (element) { // 标出错误的输入
                    $(element)
                        .closest('.form-group').addClass('has-error'); // 设置错误类
                },
                unhighlight: function (element) { // 验证通过
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // 设置成功类
                },

                submitHandler: function (form) {
                    form = $('#super_link_form');
                    //form.find('button:submit').prop('disabled',true);
                    Tools.form_validation_submit(form);
                }

            });

            // ------------------------------------------------------------------------

        }

        // ------------------------------------------------------------------------

    };

}();