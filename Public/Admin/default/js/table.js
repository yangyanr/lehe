var Table = function () {

    return {

        //主要模块初始化函数
        init: function () {

            //搜索条件选择
            $('#search_dropdown_menu li').find('a').each(function(){
                $(this).click(function(){
                    var text = $(this).text();
                    $('#sch_sort_txt').text(text).attr('data-value',$(this).attr('data-value'));
                })
            });

            // ------------------------------------------------------------------------

            //清空时间
            $('#clear_date_btn').click(function(){
                $('#clear_date').find('.date-picker').val('');
            });

            // ------------------------------------------------------------------------

            //搜索功能
            $('#search').click(function(){
                var url = $(this).attr('data-url');
                var status = $('#sch_sort_txt').attr('data-value');
                var query  = $('.search-form').find('input').serialize();
                query = query.replace(/(&|^)(\w*?\d*?\-*?_*?)*?=?((?=&)|(?=$))/g,'');
                query = query.replace(/^&/g,'');
                if(status != ''){
                    query = 'status=' + status + "&" + query;
                }
                if( url.indexOf('?')>0 ){
                    url += '&' + query;
                }else{
                    url += '?' + query;
                }
                window.location.href = url;
            });

            // ------------------------------------------------------------------------

            //回车自动提交
            $('.search-form input').keypress(function(e){
                if(e.keyCode===13){
                    $('#search').click();
                }
            });

            // ------------------------------------------------------------------------

            //搜索功能
            $('#search_config').click(function(){
                var url = $(this).attr('data-url');
                var query  = $('.search-form').find('input').serialize();
                query = query.replace(/(&|^)(\w*?\d*?\-*?_*?)*?=?((?=&)|(?=$))/g,'');
                query = query.replace(/^&/g,'');
                if( url.indexOf('?')>0 ){
                    url += '&' + query;
                }else{
                    url += '?' + query;
                }
                window.location.href = url;
            });

            // ------------------------------------------------------------------------

            //点击添加文档
            $('#document_add').click(function(){
                var url = $(this).attr('data-url');
                if(url != undefined && url != ''){
                    window.location.href = url;
                }
            });

            // ------------------------------------------------------------------------

            //点击排序
            $('#list_sort').click(function(){
                var url = $(this).attr('data-url');
                var ids = $('.ids:checked');
                var param = '';
                if(ids.length > 0){
                    var str = new Array();
                    ids.each(function(){
                        str.push($(this).val());
                    });
                    param = str.join(',');
                }

                if(url != undefined && url != ''){
                    window.location.href = url + '/ids/' + param;
                }
            });

            // ------------------------------------------------------------------------



        },

        // ------------------------------------------------------------------------

       initPickers :function () {
            //初始化日期选择器
            $('.date-picker').datetimepicker({
                rtl: App.isRTL(),
                format: 'yyyy-mm-dd',
                language:'zh-CN',
                minView:2,
                autoclose:true
            });
        },

        // ------------------------------------------------------------------------

        /*备份数据*/
        databaseExport :function () {

            var $form = $("#export-form"), $export = $("#export"), tables
            $optimize = $("#optimize"), $repair = $("#repair");

            $optimize.add($repair).click(function(){
                $.post($(this).attr('data-url'), $form.serialize(), function(data){
                    if(data.status){
                        Tools.updateAlert(data.info,'alert-success');
                    } else {
                        Tools.updateAlert(data.info,'alert-error');
                    }
                    setTimeout(function(){
                        $('#top_alert').find('button').click();
                    },2000);
                }, "json");
                return false;
            });

            // ------------------------------------------------------------------------

            $export.click(function(){
                $export.parent().children().addClass("disabled");
                $export.html("正在发送备份请求...");
                $.post(
                    $form.attr("action"),
                    $form.serialize(),
                    function(data){
                        if(data.status){
                            tables = data.tables;
                            $export.html(data.info + "，开始备份，请不要关闭本页面");
                            backup(data.tab);
                            window.onbeforeunload = function(){ return "正在备份数据库，请不要关闭" }
                        } else {
                            Tools.updateAlert(data.info,'alert-error');
                            $export.parent().children().removeClass("disabled");
                            $export.html("立即备份");
                            setTimeout(function(){
                                $('#top_alert').find('button').click();
                            },2000);
                        }
                    },
                    "json"
                );
                return false;
            });

            // ------------------------------------------------------------------------

            function backup(tab, status){
                status && showmsg(tab.id, "开始备份...(0%)");
                $.get($form.attr("action"), tab, function(data){
                    if(data.status){
                        showmsg(tab.id, data.info);
                        if(!$.isPlainObject(data.tab)){
                            $export.parent().children().removeClass("disabled");
                            $export.html("备份完成，点击重新备份");
                            window.onbeforeunload = function(){ return null }
                            return;
                        }
                        backup(data.tab, tab.id != data.tab.id);
                    } else {
                        Tools.updateAlert(data.info,'alert-error');
                        $export.parent().children().removeClass("disabled");
                        $export.html("立即备份");
                        setTimeout(function(){
                            $('#top_alert').find('button').click();
                        },2000);
                    }
                }, "json");

            }

            // ------------------------------------------------------------------------

            function showmsg(id, msg){
                $form.find("input[value=" + tables[id] + "]").closest("tr").find(".export-info").html(msg);
            }

            // ------------------------------------------------------------------------

        },

        /*还原数据*/
        databaseImport :function () {

            $(".db-import").click(function(){
                var self = this, status = ".";
                $.get(self.href, success, "json");
                window.onbeforeunload = function(){ return "正在还原数据库，请不要关闭" }
                return false;

                function success(data){
                    if(data.status){
                        if(data.gz){
                            data.info += status;
                            if(status.length === 5){
                                status = ".";
                            } else {
                                status += ".";
                            }
                        }
                        $(self).parent().prev().text(data.info);
                        if(data.part){
                            $.get(self.href,
                                {"part" : data.part, "start" : data.start},
                                success,
                                "json"
                            );
                        }  else {
                            window.onbeforeunload = function(){ return null; }
                        }
                    } else {
                        Tools.updateAlert(data.info,'alert-error');
                    }
                }
            });

            // ------------------------------------------------------------------------

        }

    // ------------------------------------------------------------------------

    };
}();