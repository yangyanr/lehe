var Tools = function () {

    return {

        //主要模块初始化函数
        init: function () {

            //ajax post submit请求
            $('.ajax-post').click(function(){
                var target,query,form;
                var target_form = $(this).attr('data-form');
                var that = this;
                var nead_confirm=false;
                if( ($(this).attr('type')=='submit') || (target = $(this).attr('data-url')) ){
                    form = $('.'+target_form);
                    if (form.get(0)==undefined){
                        return false;
                    }else if (form.get(0).nodeName=='FORM' ){
                        if ( $(this).hasClass('confirm') ) {
                            bootbox.confirm('确认要执行该操作吗?', function(result) {
                                if(result){
                                    if($(that).attr('data-url') !== undefined){
                                        target = $(that).attr('data-url');
                                    }else{
                                        target = form.get(0).action;
                                    }
                                    query = form.serialize();
                                    $(that).prop('disabled',true);
                                    $.post(target,query).success(function(data){
                                        if (data.status==1) {
                                            if (data.url) {
                                                Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                                            }else{
                                                Tools.updateAlert(data.info ,'alert-success');
                                            }
                                            setTimeout(function(){
                                                $(that).prop('disabled',false);
                                                if (data.url) {
                                                    location.href=data.url;
                                                }else if( $(that).hasClass('no-refresh')){
                                                    $('#top_alert').find('button').click();
                                                }else{
                                                    location.reload();
                                                }
                                            },2000);
                                        }else{
                                            Tools.updateAlert(data.info);
                                            setTimeout(function(){
                                                $(that).prop('disabled',false);
                                                if (data.url) {
                                                    location.href=data.url;
                                                }else{
                                                    $('#top_alert').find('button').click();
                                                }
                                            },2000);
                                        }
                                    });
                                }
                            });
                            return false;
                        }
                        if($(this).attr('data-url') !== undefined){
                            target = $(this).attr('data-url');
                        }else{
                            target = form.get(0).action;
                        }
                        query = form.serialize();
                    }else if( form.get(0).nodeName=='INPUT' || form.get(0).nodeName=='SELECT' || form.get(0).nodeName=='TEXTAREA') {
                        form.each(function(k,v){
                            if(v.type=='checkbox' && v.checked==true){
                                nead_confirm = true;
                            }
                        });
                        if ( $(this).hasClass('confirm') ) {
                            if(nead_confirm){
                                bootbox.confirm('确认要执行该操作吗?', function(result) {
                                    if(result){
                                        query = form.serialize();
                                        $(that).prop('disabled',true);
                                        $.post(target,query).success(function(data){
                                            if (data.status==1) {
                                                if (data.url) {
                                                    Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                                                }else{
                                                    Tools.updateAlert(data.info ,'alert-success');
                                                }
                                                setTimeout(function(){
                                                    $(that).prop('disabled',false);
                                                    if (data.url) {
                                                        location.href=data.url;
                                                    }else if( $(that).hasClass('no-refresh')){
                                                        $('#top_alert').find('button').click();
                                                    }else{
                                                        location.reload();
                                                    }
                                                },2000);
                                            }else{
                                                Tools.updateAlert(data.info);
                                                setTimeout(function(){
                                                    $(that).prop('disabled',false);
                                                    if (data.url) {
                                                        location.href=data.url;
                                                    }else{
                                                        $('#top_alert').find('button').click();
                                                    }
                                                },2000);
                                            }
                                        });
                                    }
                                });
                                return false;
                            }
                        }
                        query = form.serialize();
                    }else{
                        if ( $(this).hasClass('confirm') ) {
                            bootbox.confirm('确认要执行该操作吗?', function(result) {
                                if(result){
                                    query = form.find('input,select,textarea').serialize();
                                    $(that).prop('disabled',true);
                                    $.post(target,query).success(function(data){
                                        if (data.status==1) {
                                            if (data.url) {
                                                Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                                            }else{
                                                Tools.updateAlert(data.info ,'alert-success');
                                            }
                                            setTimeout(function(){
                                                $(that).prop('disabled',false);
                                                if (data.url) {
                                                    location.href=data.url;
                                                }else if( $(that).hasClass('no-refresh')){
                                                    $('#top-alert').find('button').click();
                                                }else{
                                                    location.reload();
                                                }
                                            },2000);
                                        }else{
                                            Tools.updateAlert(data.info);
                                            setTimeout(function(){
                                                $(that).prop('disabled',false);
                                                if (data.url) {
                                                    location.href=data.url;
                                                }else{
                                                    $('#top_alert').find('button').click();
                                                }
                                            },2000);
                                        }
                                    });
                                }
                            });
                            return false;
                        }
                        query = form.find('input,select,textarea').serialize();
                    }
                    $(that).prop('disabled',true);
                    $.post(target,query).success(function(data){
                        if (data.status==1) {
                            if (data.url) {
                                Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                            }else{
                                Tools.updateAlert(data.info ,'alert-success');
                            }
                            setTimeout(function(){
                                $(that).prop('disabled',false);
                                if (data.url) {
                                    location.href=data.url;
                                }else if( $(that).hasClass('no-refresh')){
                                    $('#top-alert').find('button').click();
                                }else{
                                    location.reload();
                                }
                            },2000);
                        }else{
                            Tools.updateAlert(data.info);
                            setTimeout(function(){
                                $(that).prop('disabled',false);
                                if (data.url) {
                                    location.href=data.url;
                                }else{
                                    $('#top_alert').find('button').click();
                                }
                            },2000);
                        }
                    });
                }
                return false;
            });

            // ------------------------------------------------------------------------

            //ajax get submit请求
            $('.ajax-get').click(function(){
                var target;
                var that = this;
                if ( $(this).hasClass('confirm') ) {
                    bootbox.confirm('确认要执行该操作吗?', function(result) {
                        if(result){
                            if ( (target = $(that).attr('href')) || (target = $(that).attr('data-url')) ) {
                                $.get(target).success(function(data){
                                    if (data.status==1) {
                                        if (data.url) {
                                            Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                                        }else{
                                            Tools.updateAlert(data.info,'alert-success');
                                        }
                                        setTimeout(function(){
                                            if (data.url) {
                                                location.href=data.url;
                                            }else if( $(that).hasClass('no-refresh')){
                                                $('#top_alert').find('button').click();
                                            }else{
                                                location.reload();
                                            }
                                        },2000);
                                    }else{
                                        Tools.updateAlert(data.info);
                                        setTimeout(function(){
                                            if (data.url) {
                                                location.href=data.url;
                                            }else{
                                                $('#top_alert').find('button').click();
                                            }
                                        },2000);
                                    }
                                });

                            }
                        }
                    });
                    return false;
                }
                if ( (target = $(this).attr('href')) || (target = $(this).attr('data-url')) ) {
                    $.get(target).success(function(data){
                        if (data.status==1) {
                            if (data.url) {
                                Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                            }else{
                                Tools.updateAlert(data.info,'alert-success');
                            }
                            setTimeout(function(){
                                if (data.url) {
                                    location.href=data.url;
                                }else if( $(that).hasClass('no-refresh')){
                                    $('#top_alert').find('button').click();
                                }else{
                                    location.reload();
                                }
                            },2000);
                        }else{
                            Tools.updateAlert(data.info);
                            setTimeout(function(){
                                if (data.url) {
                                    location.href=data.url;
                                }else{
                                    $('#top_alert').find('button').click();
                                }
                            },2000);
                        }
                    });

                }
                return false;
            });

            // ------------------------------------------------------------------------

            //复选框选中
            jQuery('#sample .group-checkable').change(function () {
                var set = jQuery(this).attr('data-set');
                var checked = jQuery(this).is(':checked');
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr('checked', true);
                        $(this).parents('tr').addClass('active');

                    } else {
                        $(this).attr('checked', false);
                        $(this).parents('tr').removeClass('active');
                    }
                });
                jQuery.uniform.update(set);
            });

            // ------------------------------------------------------------------------

            jQuery('#sample').on('change', 'tbody tr .checkboxes', function(){
                $(this).parents('tr').toggleClass('active');
            });

            // ------------------------------------------------------------------------

        },
        //弹出提示
        updateAlert: function (text,c) {
            /*顶部警告栏*/
            var top_alert = $('#top_alert');
            text = text||'default';
            c = c||false;
            if ( text!='default' ) {
                top_alert.find('#alert_content').text(text);
               // $('.alert', $('#top_alert')).show();
               $('#top_alert').show();
            }
            if ( c!=false ) {
                //top_alert.find('.alert').removeClass('alert-danger alert-warning alert-info alert-success').addClass(c);
                top_alert.removeClass('alert-danger alert-warning alert-info alert-success').addClass(c);
            }
        },

        // ------------------------------------------------------------------------

        //顶部导航高亮
        highlight_top_nav: function(url){

            $('.hor-menu').find('ul').find('a[href="'+url+'"]').parents('li').addClass('current');

        },

        // ------------------------------------------------------------------------

        //侧边栏导航高亮
        highlight_sidebar_nav: function(url){

            $('.page-sidebar-menu').find('a[href="'+url+'"]').parents('li').addClass('active');
            $('.page-sidebar-menu').find('a[href="'+url+'"]').parents('li').find('a > span.arrow').addClass('open');
            $('.page-sidebar-menu').find('a[href="'+url+'"]').parents().parents('li').addClass('active open');
        },

        // ------------------------------------------------------------------------

        /*表单排序*/
        form_sort: function () {

            //排序
            sort();

            //移至顶部
            $('#sort_top').click(function(){
                rest();
                $('#sort_div option:selected').prependTo('#sort_div select');
                sort();
            });

            // ------------------------------------------------------------------------

            //移至末尾
            $('#sort_bottom').click(function(){
                rest();
                $('#sort_div option:selected').appendTo('#sort_div select');
                sort();
            });

            // ------------------------------------------------------------------------

            //上移
            $('#sort_up').click(function(){
                rest();
                $('#sort_div option:selected').after($('#sort_div option:selected').prev());
                sort();
            });

            // ------------------------------------------------------------------------

            //下移
            $('#sort_down').click(function(){
                rest();
                $('#sort_div option:selected').before($('#sort_div option:selected').next());
                sort();
            });

            // ------------------------------------------------------------------------

            //排序
            function sort(){
                $('#sort_div option').text(function(){return ($(this).index()+1)+'.'+$(this).text()});
            }

            // ------------------------------------------------------------------------

            //重置所有option文字。
            function rest(){
                $('#sort_div option').text(function(){
                    return $(this).text().split('.')[1];
                });
            }

            // ------------------------------------------------------------------------

            //获取排序并提交
            $('#sort_confirm').click(function(){
                var arr = new Array();
                $('.ids').each(function(){
                    arr.push($(this).val());
                });
                $('input[name=ids]').val(arr.join(','));
                $.post(
                    $('form').attr('action'),
                    {
                        'ids' :  arr.join(',')
                    },
                    function(data){
                        if (data.status) {
                            Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                        }else{
                            Tools.updateAlert(data.info,'alert-success');
                        }
                        setTimeout(function(){
                            if (data.status) {
                                $('#sort_cancel').click();
                            }
                        },2000);
                    },
                    'json'
                );
            });

            // ------------------------------------------------------------------------

            //点击取消按钮
            $('#sort_cancel').click(function(){
                window.location.href = $(this).attr('data-url');
            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        //表单验证后提交
        form_validation_submit: function(form){
            var  target = form.get(0).action;
            var query = form.serialize();
            $.post(target,query).success(function(data){
                if (data.status==1) {
                    if (data.url) {
                        Tools.updateAlert(data.info + '，即将为你自动跳转...','alert-success');
                    }else{
                        Tools.updateAlert(data.info ,'alert-success');
                    }
                    setTimeout(function(){
                        if (data.url) {
                            location.href=data.url;
                        }else{
                            location.reload();
                        }
                    },2000);
                }else{
                    Tools.updateAlert(data.info);
                    setTimeout(function(){
                        if (data.url) {
                            location.href=data.url;
                        }else{
                            $('#top_alert').find('button').click();
                        }
                    },2000);
                }
            });
            return false;
        },

        // ------------------------------------------------------------------------

        /*访问授权*/
        group_rule: function (rules) {
            //节点
            $('.auth_rules').each(function(){
                if( $.inArray( parseInt(this.value,10),rules )>-1 ){
                    $(this).prop('checked',true);
                    $(this).parents('span').addClass('checked');
                }
                if(this.value==''){
                    $(this).closest('.child_row').remove();
                }
            });

            // ------------------------------------------------------------------------

            //全选节点
            $('.rules_all').on('change',function(){
                $(this).closest('.portlet').find('.portlet-body').find('input').prop('checked',this.checked);
                if(this.checked){
                    $(this).closest('.portlet').find('.portlet-body').find('span').addClass('checked');
                }else{
                    $(this).closest('.portlet').find('.portlet-body').find('span').removeClass('checked');
                }

            });

            // ------------------------------------------------------------------------

            //全选子节点
            $('.rules_row').on('change',function(){
                $(this).closest('.portlet-body').find('.child_row').find('input').prop('checked',this.checked);
                if(this.checked){
                    $(this).closest('.portlet-body').find('.child_row').find('span').addClass('checked');
                }else{
                    $(this).closest('.portlet-body').find('.child_row').find('span').removeClass('checked');
                }
            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*分类授权*/
        category_rule: function (auth_groups) {

            /* 分类展开收起 */
            $('.auth-category dd').prev().find('.fold i').addClass('fa fa-minus-square')
                .click(function(){
                    var self = $(this);
                    if(self.hasClass('fa fa-minus-square')){
                        self.closest('dt').next().slideUp('fast', function(){
                            self.removeClass('fa fa-minus-square').addClass('fa fa-plus-square');
                        });
                    } else {
                        self.closest('dt').next().slideDown('fast', function(){
                            self.removeClass('fa fa-plus-square').addClass('fa fa-minus-square');
                        });
                    }
                });

            // ------------------------------------------------------------------------

            $('.auth-category .cate_id').each(function(){
                if( $.inArray( parseInt(this.value,10),auth_groups )>-1 ){
                    $(this).prop('checked',true);
                    $(this).closest('span').addClass('checked');
                }
            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*用户授权到用户组授权*/
        user_to_group: function (group) {

            $('.auth_groups').each(function(){
                if( $.inArray( parseInt(this.value,10),group )>-1 ){
                    $(this).prop('checked',true);
                    $(this).closest('span').addClass('checked');
                }
            });

            // ------------------------------------------------------------------------

        },

        /*分类管理*/
        category_index: function () {

            /* 分类展开收起 */
            $('.auth-category dd').prev().find('.fold i').addClass('fa fa-minus-square')
                .click(function(){
                    var self = $(this);
                    if(self.hasClass('fa fa-minus-square')){
                        self.closest('dt').next().slideUp('fast', function(){
                            self.removeClass('fa fa-minus-square').addClass('fa fa-plus-square');
                        });
                    } else {
                        self.closest('dt').next().slideDown('fast', function(){
                            self.removeClass('fa fa-plus-square').addClass('fa fa-minus-square');
                        });
                    }
                });

            // ------------------------------------------------------------------------

            /* 三级分类删除新增按钮 */
            $('.auth-category dd dd .add-sub-cate').remove();

            // ------------------------------------------------------------------------

            /* 实时更新分类信息 */
            $('.auth-category')
                .on('submit', 'form', function(){
                    var self = $(this);
                    $.post(
                        self.attr('action'),
                        self.serialize(),
                        function(data){
                            /* 提示信息 */
                            var name = data.status ? 'success' : 'error', msg;
                            msg = self.find('.help-inline').addClass(name).text(data.info);
                            setTimeout(function(){
                                msg.fadeOut(function(){
                                    msg.text('').removeClass(name);
                                });
                            }, 2000);
                        },
                        'json'
                    );
                    return false;
                })
                .on('focus','input',function(){
                    $(this).data('param',$(this).closest('form').serialize());

                })
                .on('blur', 'input', function(){
                    if($(this).data('param')!=$(this).closest('form').serialize()){
                        $(this).closest('form').submit();
                    }
                });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*插件管理*/
        addons_index: function () {

            /* 分类展开收起 */
            function bindShow(radio_bind, selectors){
                $(radio_bind).click(function(){
                    $(selectors).toggleClass('hidden');
                })
            }

            // ------------------------------------------------------------------------

            //配置的动态
            bindShow('#has_config','.has_config');
            bindShow('#has_adminlist','.has_adminlist');

            // ------------------------------------------------------------------------

            //加载编辑代码窗口
            $('#preview').click(function(){
                var preview_url =  $(this).attr('data-url');
                var theme =  $(this).attr('data-theme');
                $.post(preview_url, $('#addons_form').serialize(),function(data){
                    $.thinkbox('<div id="preview_window" class="loading"><textarea></textarea></div>',{
                        afterShow:function(){
                            var codemirror_option = {
                                lineNumbers   :true,
                                matchBrackets :true,
                                mode          :'application/x-httpd-php',
                                indentUnit    :4,
                                gutter        :true,
                                fixedGutter   :true,
                                indentWithTabs:true,
                                readOnly	  :true,
                                lineWrapping  :true,
                                height		  :500,
                                enterMode     :'keep',
                                tabMode       :'shift',
                                theme:theme
                            };
                            var preview_window = $('#preview_window').removeClass('.loading').find('textarea');
                            var editor = CodeMirror.fromTextArea(preview_window[0], codemirror_option);
                            editor.setValue(data);
                            $(window).resize();
                        },

                        title:'预览插件主文件',
                        unload: true,
                        actions:['close'],
                        drag:true
                    });
                });
                return false;
            });

            // ------------------------------------------------------------------------

            $('.ajax-post_custom').click(function(){
                var target,query,form;
                var target_form = $(this).attr('data-form');
                var check_url = $(this).attr('data-url');
                //var check_url = "{:U('checkForm')}";
                $.ajax({
                    type: 'POST',
                    url: check_url,
                    dataType: 'json',
                    async: false,
                    data: $('#addons_form').serialize(),
                    success: function(data){
                        if(data.status){
                            if(($(this).attr('type')=='submit') || (target = $(this).attr('href')) || (target = $(this).attr('url'))){
                                form = $('.'+target_form);
                                if ( form.get(0).nodeName=='FORM' ){
                                    target = form.get(0).action;
                                    query = form.serialize();
                                }else if( form.get(0).nodeName=='INPUT' || form.get(0).nodeName=='SELECT' || form.get(0).nodeName=='TEXTAREA') {
                                    query = form.serialize();
                                }else{
                                    query = form.find('input,select,textarea').serialize();
                                }
                                $.post(target,query).success(function(data){
                                    if (data.status==1) {
                                        if (data.url) {
                                            Tools.updateAlert(data.info + '，页面即将自动跳转...','alert-success');
                                        }else{
                                            Tools.updateAlert(data.info + '，页面即将自动刷新...');
                                        }
                                        setTimeout(function(){
                                            if (data.url) {
                                                location.href=data.url;
                                            }else{
                                                location.reload();
                                            }
                                        },2000);
                                    }else{
                                        Tools.updateAlert(data.info);
                                    }
                                });
                            }
                        }else{
                            Tools.updateAlert(data.info);
                        }
                    }
                });

                return false;

            });

            // ------------------------------------------------------------------------

        },

        /*钩子排序*/
        hooks_form_sort: function () {

            $("#sortUl").dragsort({
                dragSelector:'li',
                placeHolderTemplate: '<li class="draging-place">&nbsp;</li>',
                dragEnd:function(){
                    updateVal();
                }
            });

            // ------------------------------------------------------------------------

            $('#sortUl li b').click(function(){
                $(this).parent().remove();
                updateVal();
            });

            // ------------------------------------------------------------------------

            // 更新排序后的隐藏域的值
            function updateVal() {
                var sortVal = [];
                $('#sortUl li').each(function(){
                    sortVal.push($('em',this).text());
                });
                $("input[name='addons']").val(sortVal.join(','));
            }

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        /*补丁管理*/
        patch_index: function () {

            /* 预览 */
            $('.view').click(function () {
                var preview_url = $(this).attr('data-url');
                var title = $(this).attr('data-title');
                var theme =  $(this).attr('data-theme');
                $.post(preview_url, {title: title}, function (data) {
                    $.thinkbox('<div id="preview_window" class="loading"><textarea></textarea></div>', {
                        afterShow: function () {
                            var codemirror_option = {
                                lineNumbers: true,
                                matchBrackets: true,
                                mode: "sql",
                                indentUnit: 4,
                                gutter: true,
                                fixedGutter: true,
                                indentWithTabs: true,
                                readOnly: true,
                                lineWrapping: true,
                                height: 500,
                                enterMode: "keep",
                                tabMode: "shift",
                                theme: theme
                            };
                            var preview_window = $("#preview_window").removeClass(".loading").find("textarea");
                            var editor = CodeMirror.fromTextArea(preview_window[0], codemirror_option);
                            editor.setValue(data);
                            $(window).resize();
                        },

                        title: '查看内容',
                        unload: true,
                        actions: ['close'],
                        drag: true
                    });
                });
                return false;
            });

            // ------------------------------------------------------------------------

        },

        /*版块表单*/
        forum_form: function (auth_groups) {

            //节点
            $('.user_group').each(function(){
                if( $.inArray( parseInt(this.value,10),auth_groups )>-1 ){
                    $(this).prop('checked',true);
                    $(this).parents('span').addClass('checked');
                }
                if(this.value==''){
                    $(this).closest('.child_row').remove();
                }
            });

            // ------------------------------------------------------------------------

        },

        // ------------------------------------------------------------------------

        //模型字段拖拽
        dragsort_model_file: function(){
            $(".needdragsort").dragsort({
                dragSelector:'li',
                placeHolderTemplate: '<li class="draging-place">&nbsp;</li>',
                dragBetween:true,	//允许拖动到任意地方
                dragEnd:function(){
                    var self = $(this);
                    self.find('input').attr('name', 'field_sort[' + self.closest('ul').data('group') + '][]');
                }
            });
        }

        // ------------------------------------------------------------------------

    };
}();