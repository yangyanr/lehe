/**
 核心脚本处理整个主题和核心功能
**/
var Layout = function() {

    var layoutImgPath = 'Admin/default/img/';

    var layoutCssPath = 'Admin/default/css/';

    var resBreakpointMd = App.getResponsiveBreakpoint('md');

    //* 开始:核心处理程序 *//
    // 这个函数处理响应布局屏幕尺寸大小或移动设备上旋转


    // 处理菜单栏链接 sidebar menu links
    var handleSidebarMenuActiveLink = function(mode, el) {
        var url = location.hash.toLowerCase();    

        var menu = $('.page-sidebar-menu');

        if (mode === 'click' || mode === 'set') {
            el = $(el);
        } else if (mode === 'match') {
            menu.find("li > a").each(function() {
                var path = $(this).attr("href").toLowerCase();       
                // url符合条件
                if (path.length > 1 && url.substr(1, path.length - 1) == path.substr(1)) {
                    el = $(this);
                    return; 
                }
            });
        }

        if (!el || el.size() == 0) {
            return;
        }

        if (el.attr('href').toLowerCase() === 'javascript:;' || el.attr('href').toLowerCase() === '#') {
            return;
        }        

        var slideSpeed = parseInt(menu.data("slide-speed"));
        var keepExpand = menu.data("keep-expanded");

        // 禁用活跃状态
        menu.find('li.active').removeClass('active');
        menu.find('li > a > .selected').remove();

        if (menu.hasClass('page-sidebar-menu-hover-submenu') === false) {
            menu.find('li.open').each(function(){
                if ($(this).children('.sub-menu').size() === 0) {
                    $(this).removeClass('open');
                    $(this).find('> a > .arrow.open').removeClass('open');
                }                             
            }); 
        } else {
             menu.find('li.open').removeClass('open');
        }

        el.parents('li').each(function () {
            $(this).addClass('active');
            $(this).find('> a > span.arrow').addClass('open');

            if ($(this).parent('ul.page-sidebar-menu').size() === 1) {
                $(this).find('> a').append('<span class="selected"></span>');
            }
            
            if ($(this).children('ul.sub-menu').size() === 1) {
                $(this).addClass('open');
            }
        });

        if (mode === 'click') {
            if (App.getViewPort().width < resBreakpointMd && $('.page-sidebar').hasClass("in")) { // 关闭菜单移动页面视图
                $('.page-header .responsive-toggler').click();
            }
        }
    };

    // ------------------------------------------------------------------------

    // 处理菜单栏 sidebar menu
    var handleSidebarMenu = function() {
        $('.page-sidebar').on('click', 'li > a', function(e) {

            if (App.getViewPort().width >= resBreakpointMd && $(this).parents('.page-sidebar-menu-hover-submenu').size() === 1) { //退出的悬浮栏菜单
                return;
            }

            if ($(this).next().hasClass('sub-menu') === false) {
                if (App.getViewPort().width < resBreakpointMd && $('.page-sidebar').hasClass("in")) { // 关闭菜单移动页面视图
                    $('.page-header .responsive-toggler').click();
                }
                return;
            }

            if ($(this).next().hasClass('sub-menu always-open')) {
                return;
            }

            var parent = $(this).parent().parent();
            var the = $(this);
            var menu = $('.page-sidebar-menu');
            var sub = $(this).next();

            var autoScroll = menu.data("auto-scroll");
            var slideSpeed = parseInt(menu.data("slide-speed"));
            var keepExpand = menu.data("keep-expanded");

            if (keepExpand !== true) {
                parent.children('li.open').children('a').children('.arrow').removeClass('open');
                parent.children('li.open').children('.sub-menu:not(.always-open)').slideUp(slideSpeed);
                parent.children('li.open').removeClass('open');
            }

            var slideOffeset = -200;

            if (sub.is(":visible")) {
                $('.arrow', $(this)).removeClass("open");
                $(this).parent().removeClass("open");
                sub.slideUp(slideSpeed, function() {
                    if (autoScroll === true && $('body').hasClass('page-sidebar-closed') === false) {
                        if ($('body').hasClass('page-sidebar-fixed')) {
                            menu.slimScroll({
                                'scrollTo': (the.position()).top
                            });
                        } else {
                            App.scrollTo(the, slideOffeset);
                        }
                    }
                });
            } else {
                $('.arrow', $(this)).addClass("open");
                $(this).parent().addClass("open");
                sub.slideDown(slideSpeed, function() {
                    if (autoScroll === true && $('body').hasClass('page-sidebar-closed') === false) {
                        if ($('body').hasClass('page-sidebar-fixed')) {
                            menu.slimScroll({
                                'scrollTo': (the.position()).top
                            });
                        } else {
                            App.scrollTo(the, slideOffeset);
                        }
                    }
                });
            }

            e.preventDefault();
        });

        // 在侧边栏菜单处理ajax链接 sidebar menu
        $('.page-sidebar').on('click', ' li > a.ajaxify', function(e) {
            e.preventDefault();
            App.scrollTop();

            var url = $(this).attr("href");
            var menuContainer = $('.page-sidebar ul');
            var pageContent = $('.page-content');
            var pageContentBody = $('.page-content .page-content-body');

            menuContainer.children('li.active').removeClass('active');
            menuContainer.children('arrow.open').removeClass('open');

            $(this).parents('li').each(function() {
                $(this).addClass('active');
                $(this).children('a > span.arrow').addClass('open');
            });
            $(this).parents('li').addClass('active');

            if (App.getViewPort().width < resBreakpointMd && $('.page-sidebar').hasClass("in")) { // 关闭菜单移动页面视图
                $('.page-header .responsive-toggler').click();
            }

            App.startPageLoading();

            var the = $(this);

            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function(res) {

                    if (the.parents('li.open').size() === 0) {
                        $('.page-sidebar-menu > li.open > a').click();
                    }

                    App.stopPageLoading();
                    pageContentBody.html(res);
                    Layout.fixContentHeight(); // 修复内容高度
                    App.initAjax(); // 初始化核心的东西
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    App.stopPageLoading();
                    pageContentBody.html('<h4>无法加载请求的内容</h4>');
                }
            });
        });

        // ------------------------------------------------------------------------

        // 在主要内容处理ajax链接 main content
        $('.page-content').on('click', '.ajaxify', function(e) {
            e.preventDefault();
            App.scrollTop();

            var url = $(this).attr("href");
            var pageContent = $('.page-content');
            var pageContentBody = $('.page-content .page-content-body');

            App.startPageLoading();

            if (App.getViewPort().width < resBreakpointMd && $('.page-sidebar').hasClass("in")) { //关闭菜单移动页面视图
                $('.page-header .responsive-toggler').click();
            }

            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function(res) {
                    App.stopPageLoading();
                    pageContentBody.html(res);
                    Layout.fixContentHeight(); // 修复内容高度
                    App.initAjax(); // 初始化核心的东西
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    pageContentBody.html('<h4>无法加载请求的内容</h4>');
                    App.stopPageLoading();
                }
            });
        });

        // 处理滚动时前在响应菜单切换点击标题移动视图是固定的
        $(document).on('click', '.page-header-fixed-mobile .responsive-toggler', function(){
            App.scrollTop();
        });      
    };

    // ------------------------------------------------------------------------

    //Helper函数计算栏高度固定栏布局
    var _calculateFixedSidebarViewportHeight = function() {
        var sidebarHeight = App.getViewPort().height - $('.page-header').outerHeight() - 30;
        if ($('body').hasClass("page-footer-fixed")) {
            sidebarHeight = sidebarHeight - $('.page-footer').outerHeight();
        }

        return sidebarHeight;
    };

    // ------------------------------------------------------------------------

    //处理固定侧栏
    var handleFixedSidebar = function() {
        var menu = $('.page-sidebar-menu');

        App.destroySlimScroll(menu);

        if ($('.page-sidebar-fixed').size() === 0) {
            return;
        }

        if (App.getViewPort().width >= resBreakpointMd) {
            menu.attr("data-height", _calculateFixedSidebarViewportHeight());
            App.initSlimScroll(menu);
        }
    };

    // ------------------------------------------------------------------------

    // 处理栏切换关闭/隐藏侧边栏
    var handleFixedSidebarHoverEffect = function () {
        var body = $('body');
        if (body.hasClass('page-sidebar-fixed')) {
            $('.page-sidebar').on('mouseenter', function () {
                if (body.hasClass('page-sidebar-closed')) {
                    $(this).find('.page-sidebar-menu').removeClass('page-sidebar-menu-closed');
                }
            }).on('mouseleave', function () {
                if (body.hasClass('page-sidebar-closed')) {
                    $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
                }
            });
        }
    };

    // ------------------------------------------------------------------------

    // Hanles sidebar toggler Hanles栏切换
    var handleSidebarToggler = function() {
        var body = $('body');
        if ($.cookie && $.cookie('sidebar_closed') === '1' && App.getViewPort().width >= resBreakpointMd) {
            $('body').addClass('page-sidebar-closed');
            $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
        }

        // 处理栏显示/隐藏
        $('body').on('click', '.sidebar-toggler', function(e) {
            var sidebar = $('.page-sidebar');
            var sidebarMenu = $('.page-sidebar-menu');
            $(".sidebar-search", sidebar).removeClass("open");

            if (body.hasClass("page-sidebar-closed")) {
                body.removeClass("page-sidebar-closed");
                sidebarMenu.removeClass("page-sidebar-menu-closed");
                if ($.cookie) {
                    $.cookie('sidebar_closed', '0');
                }
            } else {
                body.addClass("page-sidebar-closed");
                sidebarMenu.addClass("page-sidebar-menu-closed");
                if (body.hasClass("page-sidebar-fixed")) {
                    sidebarMenu.trigger("mouseleave");
                }
                if ($.cookie) {
                    $.cookie('sidebar_closed', '1');
                }
            }

            $(window).trigger('resize');
        });

        handleFixedSidebarHoverEffect();

        // 处理搜索栏
        $('.page-sidebar').on('click', '.sidebar-search .remove', function(e) {
            e.preventDefault();
            $('.sidebar-search').removeClass("open");
        });

        // 处理输入的搜索查询提交
        $('.page-sidebar .sidebar-search').on('keypress', 'input.form-control', function(e) {
            if (e.which == 13) {
                $('.sidebar-search').submit();
                return false; //
            }
        });

        // 处理搜索提交(标题搜索栏搜索和响应模式)
        $('.sidebar-search .submit').on('click', function(e) {
            e.preventDefault();
            if ($('body').hasClass("page-sidebar-closed")) {
                if ($('.sidebar-search').hasClass('open') === false) {
                    if ($('.page-sidebar-fixed').size() === 1) {
                        $('.page-sidebar .sidebar-toggler').click(); //触发栏切换按钮
                    }
                    $('.sidebar-search').addClass("open");
                } else {
                    $('.sidebar-search').submit();
                }
            } else {
                $('.sidebar-search').submit();
            }
        });

        // 处理在body点击关闭
        if ($('.sidebar-search').size() !== 0) {
            $('.sidebar-search .input-group').on('click', function(e) {
                e.stopPropagation();
            });

            $('body').on('click', function() {
                if ($('.sidebar-search').hasClass('open')) {
                    $('.sidebar-search').removeClass("open");
                }
            });
        }
    };

    // ------------------------------------------------------------------------

    // 处理横向菜单
    var handleHeader = function() {
        // 处理搜索框展开/折叠
        $('.page-header').on('click', '.search-form', function(e) {
            $(this).addClass("open");
            $(this).find('.form-control').focus();

            $('.page-header .search-form .form-control').on('blur', function(e) {
                $(this).closest('.search-form').removeClass("open");
                $(this).unbind("blur");
            });
        });

        // 处理菜单搜索表单输入
        $('.page-header').on('keypress', '.hor-menu .search-form .form-control', function(e) {
            if (e.which == 13) {
                $(this).closest('.search-form').submit();
                return false;
            }
        });

        // 处理标题搜索按钮点击
        $('.page-header').on('mousedown', '.search-form.open .submit', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.search-form').submit();
        });
    };

    // ------------------------------------------------------------------------

    // 处理去顶部按钮在页脚
    var handleGoTop = function() {
        var offset = 300;
        var duration = 500;

        if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) { // ios支持
            $(window).bind("touchend touchcancel touchleave", function(e) {
                if ($(this).scrollTop() > offset) {
                    $('.scroll-to-top').fadeIn(duration);
                } else {
                    $('.scroll-to-top').fadeOut(duration);
                }
            });
        } else { // 一般
            $(window).scroll(function() {
                if ($(this).scrollTop() > offset) {
                    $('.scroll-to-top').fadeIn(duration);
                } else {
                    $('.scroll-to-top').fadeOut(duration);
                }
            });
        }

        $('.scroll-to-top').click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, duration);
            return false;
        });
    };

    // ------------------------------------------------------------------------

    //* 结束:核心处理程序 *//

    return {

        // 主要init方法初始化布局
        // 重要! ! !:不要修改核心处理程序调用顺序

        initHeader: function() {
            handleHeader(); // 处理横向菜单
        },

        // ------------------------------------------------------------------------

        setSidebarMenuActiveLink: function(mode, el) {
            handleSidebarMenuActiveLink(mode, el);
        },

        // ------------------------------------------------------------------------

        initSidebar: function() {
            //布局处理程序
            handleFixedSidebar(); // 处理固定侧边栏菜单
            handleSidebarMenu(); // 处理主菜单
            handleSidebarToggler(); // 处理栏隐藏/显示

            if (App.isAngularJsApp()) {
                handleSidebarMenuActiveLink('match'); // init栏活动链接
            }

            App.addResizeHandler(handleFixedSidebar); // 初始化窗口大小固定的侧边栏
        },

        // ------------------------------------------------------------------------

        initContent: function() {
            return; 
        },

        // ------------------------------------------------------------------------

        initFooter: function() {
            handleGoTop(); //在页脚处理滚动到顶部功能
        },

        // ------------------------------------------------------------------------

        init: function () {            
            this.initHeader();
            this.initSidebar();
            this.initContent();
            this.initFooter();
        },

        // ------------------------------------------------------------------------

        //公共函数来解决这个侧边栏和内容相应的高度
        fixContentHeight: function() {
            return;
        },

        // ------------------------------------------------------------------------

        initFixedSidebarHoverEffect: function() {
            handleFixedSidebarHoverEffect();
        },

        // ------------------------------------------------------------------------

        initFixedSidebar: function() {
            handleFixedSidebar();
        },

        // ------------------------------------------------------------------------

        getLayoutImgPath: function() {
            return App.getAssetsPath() + layoutImgPath;
        },

        // ------------------------------------------------------------------------

        getLayoutCssPath: function() {
            return App.getAssetsPath() + layoutCssPath;
        }

        // ------------------------------------------------------------------------

    };

}();