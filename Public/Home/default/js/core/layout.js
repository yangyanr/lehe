/**
 核心脚本处理整个主题和核心功能
**/
var Layout = function () {

    var layoutImgPath = 'Home/default/img/';

    var layoutCssPath = 'Home/default/css/';

    var resBreakpointMd = App.getResponsiveBreakpoint('md');

    // ------------------------------------------------------------------------

    //* 开始:核心处理程序 *//
    // 这个函数处理响应布局屏幕尺寸大小或移动设备上旋转

    // 处理 头部
    var handleHeader = function () {        
        // 处理搜索框展开/折叠
        $('.page-header').on('click', '.search-form', function (e) {
            $(this).addClass("open");
            $(this).find('.form-control').focus();

            $('.page-header .search-form .form-control').on('blur', function (e) {
                $(this).closest('.search-form').removeClass("open");
                $(this).unbind("blur");
            });
        });

        // 处理 键盘搜索
        $('.page-header').on('keypress', '.hor-menu .search-form .form-control', function (e) {
            if (e.which == 13) {
                $(this).closest('.search-form').submit();
                return false;
            }
        });

        // 处理标题搜索按钮点击
        $('.page-header').on('mousedown', '.search-form.open .submit', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.search-form').submit();
        });

        // 处理滚动时前在响应菜单切换点击标题移动视图是固定的
        $('body').on('click', '.page-header-top-fixed .page-header-top .menu-toggler', function(){
            App.scrollTop();
        });     
    };

    // ------------------------------------------------------------------------

    // 处理主菜单
    var handleMainMenu = function () {

        // 处理菜单切换图标点击
        $(".page-header .menu-toggler").on("click", function(event) {
            if (App.getViewPort().width < resBreakpointMd) {
                var menu = $(".page-header .page-header-menu");
                if (menu.is(":visible")) {
                    menu.slideUp(300);
                } else {  
                    menu.slideDown(300);
                }

                if ($('body').hasClass('page-header-top-fixed')) {
                    App.scrollTop();
                }
            }
        });

        // 为移动设备处理子下拉菜单点击
        $(".hor-menu .dropdown-submenu > a").on("click", function(e) {
            if (App.getViewPort().width < resBreakpointMd) {
                if ($(this).next().hasClass('dropdown-menu')) {
                    e.stopPropagation();
                    if ($(this).parent().hasClass("open")) {
                        $(this).parent().removeClass("open");
                        $(this).next().hide();
                    } else {
                        $(this).parent().addClass("open");
                        $(this).next().show();
                    }
                }
            }
        });

        //桌面设备处理鼠标下拉菜单
        if (App.getViewPort().width >= resBreakpointMd) {
            $('.hor-menu [data-hover="megamenu-dropdown"]').not('.hover-initialized').each(function() {   
                $(this).dropdownHover(); 
                $(this).addClass('hover-initialized'); 
            });
        } 

        // 处理自动滚动到选定的子菜单节点在移动设备上
        $(document).on('click', '.hor-menu .menu-dropdown > a[data-hover="megamenu-dropdown"]', function() {
            if (App.getViewPort().width < resBreakpointMd) {
                App.scrollTo($(this));
            }
        });

        //  控制大型菜单的打开/关闭
        $(document).on('click', '.mega-menu-dropdown .dropdown-menu, .classic-menu-dropdown .dropdown-menu', function (e) {
            e.stopPropagation();
        });

        // 处理大型固定菜单(最小化)
        $(window).scroll(function() {                
            var offset = 75;
            if ($('body').hasClass('page-header-menu-fixed')) {
                if ($(window).scrollTop() > offset){
                    $(".page-header-menu").addClass("fixed");
                } else {
                    $(".page-header-menu").removeClass("fixed");  
                }
            }

            if ($('body').hasClass('page-header-top-fixed')) {
                if ($(window).scrollTop() > offset){
                    $(".page-header-top").addClass("fixed");
                } else {
                    $(".page-header-top").removeClass("fixed");  
                }
            }
        });
    };

    // ------------------------------------------------------------------------

    // 处理栏菜单链接
    var handleMainMenuActiveLink = function(mode, el) {
        var url = location.hash.toLowerCase();    

        var menu = $('.hor-menu');

        if (mode === 'click' || mode === 'set') {
            el = $(el);
        } else if (mode === 'match') {
            menu.find("li > a").each(function() {
                var path = $(this).attr("href").toLowerCase();       
                //  url符合条件
                if (path.length > 1 && url.substr(1, path.length - 1) == path.substr(1)) {
                    el = $(this);
                    return; 
                }
            });
        }

        if (!el || el.size() == 0) {
            return;
        }

        if (el.attr('href').toLowerCase() === 'javascript:;' || el.attr('href').toLowerCase() === '#') {
            return;
        }        

        // 禁用活跃状态
        menu.find('li.active').removeClass('active');
        menu.find('li > a > .selected').remove();
        menu.find('li.open').removeClass('open');

        el.parents('li').each(function () {
            $(this).addClass('active');

            if ($(this).parent('ul.navbar-nav').size() === 1) {
                $(this).find('> a').append('<span class="selected"></span>');
            }
        });
    };

    // ------------------------------------------------------------------------

    // 处理主菜单窗口大小
    var handleMainMenuOnResize = function() {
        // 桌面设备处理鼠标下拉菜单
        var width = App.getViewPort().width;
        var menu = $(".page-header-menu");
            
        if (width >= resBreakpointMd && menu.data('breakpoint') !== 'desktop') { 
            // 重置活跃状态
            $('.hor-menu [data-toggle="dropdown"].active').removeClass('open');

            menu.data('breakpoint', 'desktop');
            $('.hor-menu [data-hover="megamenu-dropdown"]').not('.hover-initialized').each(function() {   
                $(this).dropdownHover(); 
                $(this).addClass('hover-initialized'); 
            });
            $('.hor-menu .navbar-nav li.open').removeClass('open');
            $(".page-header-menu").css("display", "block");
        } else if (width < resBreakpointMd && menu.data('breakpoint') !== 'mobile') {
            // 活跃的状态设置为开放
            $('.hor-menu [data-toggle="dropdown"].active').addClass('open');
            
            menu.data('breakpoint', 'mobile');
            //  禁用 bootstrap dropdowns 悬浮
            $('.hor-menu [data-hover="megamenu-dropdown"].hover-initialized').each(function() {   
                $(this).unbind('hover');
                $(this).parent().unbind('hover').find('.dropdown-submenu').each(function() {
                    $(this).unbind('hover');
                });
                $(this).removeClass('hover-initialized');    
            });
        } else if (width < resBreakpointMd) {
            //$(".page-header-menu").css("display", "none");  
        }
    };

    // ------------------------------------------------------------------------

    var handleContentHeight = function() {
        var height;

        if ($('body').height() < App.getViewPort().height) {
            height = App.getViewPort().height -
                $('.page-header').outerHeight() - 
                ($('.page-container').outerHeight() - $('.page-content').outerHeight()) -
                $('.page-prefooter').outerHeight() - 
                $('.page-footer').outerHeight();

            $('.page-content').css('min-height', height);
        }
    };

    // ------------------------------------------------------------------------

    // 处理去顶部按钮在页脚
    var handleGoTop = function () {
        var offset = 100;
        var duration = 500;

        if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {  // ios支持
            $(window).bind("touchend touchcancel touchleave", function(e){
               if ($(this).scrollTop() > offset) {
                    $('.scroll-to-top').fadeIn(duration);
                } else {
                    $('.scroll-to-top').fadeOut(duration);
                }
            });
        } else {  // 其他
            $(window).scroll(function() {
                if ($(this).scrollTop() > offset) {
                    $('.scroll-to-top').fadeIn(duration);
                } else {
                    $('.scroll-to-top').fadeOut(duration);
                }
            });
        }
        
        $('.scroll-to-top').click(function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop: 0}, duration);
            return false;
        });
    };

    // ------------------------------------------------------------------------

    //*  *:* 核心处理程序结束

    return {
        
        // 主要init方法初始化布局
        // 重要! ! !:不要修改核心处理程序调用顺序。

        initHeader: function() {
            handleHeader(); // 处理横向菜单
            handleMainMenu(); // 处理菜单切换的移动
            App.addResizeHandler(handleMainMenuOnResize); //处理主菜单窗口大小

            if (App.isAngularJsApp()) {
                handleMainMenuActiveLink('match'); // init菜单活动链接
            }
        },

        // ------------------------------------------------------------------------

        initContent: function() {
            handleContentHeight(); //处理内容的高度
        },

        // ------------------------------------------------------------------------

        initFooter: function() {
            handleGoTop(); //在页脚处理滚动到顶部功能
        },

        // ------------------------------------------------------------------------

        init: function () {            
            this.initHeader();
            this.initContent();
            this.initFooter();
        },

        // ------------------------------------------------------------------------

        setMainMenuActiveLink: function(mode, el) {
            handleMainMenuActiveLink(mode, el);
        },

        // ------------------------------------------------------------------------

        closeMainMenu: function() {
            $('.hor-menu').find('li.open').removeClass('open');

            if (App.getViewPort().width < resBreakpointMd && $('.page-header-menu').is(":visible")) { // 关闭菜单移动页面视图
                $('.page-header .menu-toggler').click();
            }
        },

        // ------------------------------------------------------------------------

        getLayoutImgPath: function() {
            return App.getAssetsPath() + layoutImgPath;
        },

        // ------------------------------------------------------------------------

        getLayoutCssPath: function() {
            return App.getAssetsPath() + layoutCssPath;
        }

        // ------------------------------------------------------------------------
    };

    // ------------------------------------------------------------------------

}();