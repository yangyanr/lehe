/**
 核心脚本处理整个主题和核心功能
**/
var App = function() {

    // IE 模式
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

    var resizeHandlers = [];

    var assetsPath = '/Public/';

    var globalImgPath = 'Static/img/';

    var globalPluginsPath = 'Static/plugins/';

    var globalCssPath = 'Static/css/';

    // ------------------------------------------------------------------------

    // 主题布局颜色设置

    var brandColors = {
        'blue': '#89C4F4',
        'red': '#F3565D',
        'green': '#1bbc9b',
        'purple': '#9b59b6',
        'grey': '#95a5a6',
        'yellow': '#F8CB00'
    };

    // ------------------------------------------------------------------------

    // 初始化主要设置
    var handleInit = function() {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !!navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !!navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !!navigator.userAgent.match(/MSIE 10.0/);

        if (isIE10) {
            $('html').addClass('ie10'); // 检测IE10浏览器版本
        }

        if (isIE10 || isIE9 || isIE8) {
            $('html').addClass('ie'); // 检测IE10浏览器版本
        }
    };

    // ------------------------------------------------------------------------

    // App.addResponsiveHandler设定的回调函数
    var _runResizeHandlers = function() {
        // 初始化其他订阅元素
        for (var i = 0; i < resizeHandlers.length; i++) {
            var each = resizeHandlers[i];
            each.call();
        }
    };

    // ------------------------------------------------------------------------

    // 处理布局 检查窗口大小
    var handleOnResize = function() {
        var resize;
        if (isIE8) {
            var currheight;
            $(window).resize(function() {
                if (currheight == document.documentElement.clientHeight) {
                    return;
                }
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function() {
                    _runResizeHandlers();
                }, 50);
                currheight = document.documentElement.clientHeight; //
            });
        } else {
            $(window).resize(function() {
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function() {
                    _runResizeHandlers();
                }, 50);
            });
        }
    };

    // ------------------------------------------------------------------------

    // 处理 portlet 工具
    var handlePortletTools = function() {
        // 处理 portlet remove
        $('body').on('click', '.portlet > .portlet-title > .tools > a.remove', function(e) {
            e.preventDefault();
            var portlet = $(this).closest(".portlet");

            portlet.find('.portlet-title > .tools > .remove').tooltip('destroy');
            portlet.find('.portlet-title > .tools > .collapse, .portlet > .portlet-title > .tools > .expand').tooltip('destroy');

            portlet.remove();
        });

        // 处理 portlet collapse
        $('body').on('click', '.portlet > .portlet-title > .tools > .collapse, .portlet .portlet-title > .tools > .expand', function(e) {
            e.preventDefault();
            var el = $(this).closest(".portlet").children(".portlet-body");
            if ($(this).hasClass("collapse")) {
                $(this).removeClass("collapse").addClass("expand");
                el.slideUp(200);
            } else {
                $(this).removeClass("expand").addClass("collapse");
                el.slideDown(200);
            }
        });
    };

    // ------------------------------------------------------------------------

    // 处理  checkboxes & radios 用 jQuery Uniform plugin
    var handleUniform = function() {
        if (!$().uniform) {
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle, .make-switch, .icheck), input[type=radio]:not(.toggle, .star, .make-switch, .icheck)");
        if (test.size() > 0) {
            test.each(function() {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    };

    // ------------------------------------------------------------------------

    // 处理 checkboxes & radios 用 jQuery iCheck plugin
    var handleiCheck = function() {
        if (!$().iCheck) {
            return;
        }

        $('.icheck').each(function() {
            var checkboxClass = $(this).attr('data-checkbox') ? $(this).attr('data-checkbox') : 'icheckbox_minimal-grey';
            var radioClass = $(this).attr('data-radio') ? $(this).attr('data-radio') : 'iradio_minimal-grey';

            if (checkboxClass.indexOf('_line') > -1 || radioClass.indexOf('_line') > -1) {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass,
                    insert: '<div class="icheck_line-icon"></div>' + $(this).attr("data-label")
                });
            } else {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass
                });
            }
        });
    };

    // ------------------------------------------------------------------------

    // 处理 Bootstrap switches
    var handleBootstrapSwitch = function() {
        if (!$().bootstrapSwitch) {
            return;
        }
        $('.make-switch').bootstrapSwitch();
    };

    // ------------------------------------------------------------------------

    // 处理 Bootstrap confirmations
    var handleBootstrapConfirmation = function() {
        if (!$().confirmation) {
            return;
        }
        $('[data-toggle=confirmation]').confirmation({ container: 'body', btnOkClass: 'btn-xs btn-success', btnCancelClass: 'btn-xs btn-danger'});
    }

    // ------------------------------------------------------------------------
    
    // 处理 Bootstrap 手风琴效果
    var handleAccordions = function() {
        $('body').on('shown.bs.collapse', '.accordion.scrollable', function(e) {
            App.scrollTo($(e.target));
        });
    };

    // ------------------------------------------------------------------------

    // 处理 Bootstrap Tabs
    var handleTabs = function() {
        //激活标签如果标签id提供的URL
        if (location.hash) {
            var tabid = location.hash.substr(1);
            $('a[href="#' + tabid + '"]').parents('.tab-pane:hidden').each(function() {
                var tabid = $(this).attr("id");
                $('a[href="#' + tabid + '"]').click();
            });
            $('a[href="#' + tabid + '"]').click();
        }
    };

    // ------------------------------------------------------------------------

    // 处理 Bootstrap Modals
    var handleModals = function() {        
        // 修复可叠起堆放的模态问题: 当2个或更多modals opened, 关闭一个模态将删除 .modal-open 类
        $('body').on('hide.bs.modal', function() {
            if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') === false) {
                $('html').addClass('modal-open');
            } else if ($('.modal:visible').size() <= 1) {
                $('html').removeClass('modal-open');
            }
        });

        // 修复页面滚动条问题
        $('body').on('show.bs.modal', '.modal', function() {
            if ($(this).hasClass("modal-scroll")) {
                $('body').addClass("modal-open-noscroll");
            }
        });

        // 修复页面滚动条问题
        $('body').on('hide.bs.modal', '.modal', function() {
            $('body').removeClass("modal-open-noscroll");
        });

        // 删除ajax内容并删除缓存模式关闭
        $('body').on('hidden.bs.modal', '.modal:not(.modal-cached)', function () {
            $(this).removeData('bs.modal');
        });
    };

    // ------------------------------------------------------------------------

    // 处理 Bootstrap Tooltips
    var handleTooltips = function() {
        // 全局 tooltips
        $('.tooltips').tooltip();

        $('.portlet > .portlet-title > .tools > .remove').tooltip({
            container: 'body',
            title: '移除'
        });

        $('.portlet > .portlet-title > .tools > .collapse, .portlet > .portlet-title > .tools > .expand').tooltip({
            container: 'body',
            title: '伸缩 / 展开'
        });
    };

    // ------------------------------------------------------------------------

    // 处理 Bootstrap Dropdowns
    var handleDropdowns = function() {
        /*
         * 点击 Dropdowns
        */
        $('body').on('click', '.dropdown-menu.hold-on-click', function(e) {
            e.stopPropagation();
        });
    };

    // ------------------------------------------------------------------------

    var handleAlerts = function() {
        $('body').on('click', '[data-close="alert"]', function(e) {
            $(this).parent('.alert').hide();
            $(this).closest('.note').hide();
            e.preventDefault();
        });

        $('body').on('click', '[data-close="note"]', function(e) {
            $(this).closest('.note').hide();
            e.preventDefault();
        });

        $('body').on('click', '[data-remove="note"]', function(e) {
            $(this).closest('.note').remove();
            e.preventDefault();
        });
    };

    // ------------------------------------------------------------------------

    // 处理 Hower Dropdowns
    var handleDropdownHover = function() {
        $('[data-hover="dropdown"]').not('.hover-initialized').each(function() {
            $(this).dropdownHover();
            $(this).addClass('hover-initialized');
        });
    };

    // ------------------------------------------------------------------------

    // 处理 Bootstrap Popovers
    // 最后popep popover
    var lastPopedPopover;

    var handlePopovers = function() {
        $('.popovers').popover();

        //  关闭最后显示的 displayed popover

        $(document).on('click.bs.popover.data-api', function(e) {
            if (lastPopedPopover) {
                lastPopedPopover.popover('hide');
            }
        });
    };

    // ------------------------------------------------------------------------

    // 处理 scrollable contents 用 jQuery SlimScroll plugin
    var handleScrollers = function() {
        App.initSlimScroll('.scroller');
    };

    // ------------------------------------------------------------------------

    // 处理 Image Preview 用 jQuery Fancybox plugin
    var handleFancybox = function() {
        if (!jQuery.fancybox) {
            return;
        }

        if ($(".fancybox-button").size() > 0) {
            $(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
    };

    // ------------------------------------------------------------------------

    // 修复 IE8、IE9 input placeholder 问题
    var handleFixInputPlaceholderForIE = function() {
        //修复 ie7 、ie8 html5 placeholder attribute
        if (isIE8 || isIE9) { // ie8 、 ie9
            //  html5 placeholder inputs, inputs 如果有 placeholder-no-fix 类将被忽略(如: 密码 password fields)
            $('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function() {
                var input = $(this);

                if (input.val() === '' && input.attr("placeholder") !== '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function() {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function() {
                    if (input.val() === '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    };

    // ------------------------------------------------------------------------

    // 处理 Select2 Dropdowns
    var handleSelect2 = function() {
        if ($().select2) {
            $('.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }
    };

    // ------------------------------------------------------------------------

    // 处理 bootbox
    // var handleBootbox = function() {
    //     bootbox.setDefaults({locale: "zh_CN"});
    //};

    //* 核心处理完成 *//

    return {

        //主要函数 处理 主题
        init: function() {
            // 重要! ! !:不要修改核心处理程序调用顺序

            //核心处理程序
            handleInit(); // 初始化核心变量
            handleOnResize(); // 设置处理响应式 responsive

            //UI组件处理程序
            handleUniform(); //  custom radio & checkboxes
            handleiCheck(); //  custom icheck radio and checkboxes
            handleBootstrapSwitch(); //  bootstrap switch plugin
            handleScrollers(); //  slim scrolling contents
            handleFancybox(); //  fancy box
            handleSelect2(); //   Select2 dropdowns
            handlePortletTools(); // portlet action (collapse, remove)
            handleAlerts(); // closabled alerts
            handleDropdowns(); //  dropdowns
            handleTabs(); //  tabs
            handleTooltips(); //  bootstrap tooltips
            handlePopovers(); //  bootstrap popovers
            handleAccordions(); // accordions
            handleModals(); //  modals
            handleBootstrapConfirmation(); //  bootstrap confirmations

            // ie
            handleFixInputPlaceholderForIE(); //IE8 & IE9 input placeholder
            //handleBootbox();//  bootbox
        },

        // ------------------------------------------------------------------------

        //主要功能后启动核心javascript ajax完成
        initAjax: function() {
            handleUniform(); //  custom radio & checkboxes
            handleiCheck(); //  icheck radio and checkboxes
            handleBootstrapSwitch(); //  bootstrap switch plugin
            handleDropdownHover(); //  dropdown hover
            handleScrollers(); //  slim scrolling contents
            handleSelect2(); //  custom Select2 dropdowns
            handleFancybox(); //  fancy box
            handleDropdowns(); //  dropdowns
            handleTooltips(); //  bootstrap tooltips
            handlePopovers(); //  bootstrap popovers
            handleAccordions(); // accordions
            handleBootstrapConfirmation(); // bootstrap confirmations
        },

        // ------------------------------------------------------------------------

        //init主要组件
        initComponents: function() {
            this.initAjax();
        },

        // ------------------------------------------------------------------------

        //公共函数 最好打开 popover 需要点击关闭
        setLastPopedPopover: function(el) {
            lastPopedPopover = el;
        },

        // ------------------------------------------------------------------------

        //which
        addResizeHandler: function(func) {
            resizeHandlers.push(func);
        },

        // ------------------------------------------------------------------------

        // 处理 bootbox
        bootboxHandler: function() {
            bootbox.setDefaults({locale: "zh_CN"});
        },

        // ------------------------------------------------------------------------

        //调用 _runresizeHandlers
        runResizeHandlers: function() {
            _runResizeHandlers();
        },

        // ------------------------------------------------------------------------

        // 返回到顶部
        scrollTo: function(el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;

            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.page-header').height();
                }
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            $('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        // ------------------------------------------------------------------------

        initSlimScroll: function(el) {
            $(el).each(function() {
                if ($(this).attr("data-initialized")) {
                    return; // 退出
                }

                var height;

                if ($(this).attr("data-height")) {
                    height = $(this).attr("data-height");
                } else {
                    height = $(this).css('height');
                }

                $(this).slimScroll({
                    allowPageScroll: true, // 允许页面滚动元素滚动结束后
                    size: '7px',
                    color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
                    wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
                    railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
                    position: isRTL ? 'left' : 'right',
                    height: height,
                    alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                    railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                    disableFadeOut: true
                });

                $(this).attr("data-initialized", "1");
            });
        },

        // ------------------------------------------------------------------------

        destroySlimScroll: function(el) {
            $(el).each(function() {
                if ($(this).attr("data-initialized") === "1") { // 退出之前更新 height
                    $(this).removeAttr("data-initialized");
                    $(this).removeAttr("style");

                    var attrList = {};

                    // 存储自定义属性以后我们将重新分配
                    if ($(this).attr("data-handle-color")) {
                        attrList["data-handle-color"] = $(this).attr("data-handle-color");
                    }
                    if ($(this).attr("data-wrapper-class")) {
                        attrList["data-wrapper-class"] = $(this).attr("data-wrapper-class");
                    }
                    if ($(this).attr("data-rail-color")) {
                        attrList["data-rail-color"] = $(this).attr("data-rail-color");
                    }
                    if ($(this).attr("data-always-visible")) {
                        attrList["data-always-visible"] = $(this).attr("data-always-visible");
                    }
                    if ($(this).attr("data-rail-visible")) {
                        attrList["data-rail-visible"] = $(this).attr("data-rail-visible");
                    }

                    $(this).slimScroll({
                        wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
                        destroy: true
                    });

                    var the = $(this);

                    // 重新分配自定义属性
                    $.each(attrList, function(key, value) {
                        the.attr(key, value);
                    });

                }
            });
        },

        // ------------------------------------------------------------------------

        // 返回顶部
        scrollTop: function() {
            App.scrollTo();
        },

        // ------------------------------------------------------------------------

        // 函数块元素(显示加载)
        blockUI: function(options) {
            options = $.extend(true, {}, options);
            var html = '';
            if (options.animate) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '">' + '<div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' + '</div>';
            } else if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="' + this.getGlobalImgPath() + 'loading-spinner-grey.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : '加载中...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="' + this.getGlobalImgPath() + 'loading-spinner-grey.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : '加载中...') + '</span></div>';
            }

            if (options.target) { //加载中。。。
                var el = $(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY !== undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: '等待中...'
                    }
                });
            } else { //页面加屏蔽罩
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: '等待中...'
                    }
                });
            }
        },

        // ------------------------------------------------------------------------

        // 函数un-block元素(完成加载)
        unblockUI: function(target) {
            if (target) {
                $(target).unblock({
                    onUnblock: function() {
                        $(target).css('position', '');
                        $(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },

        // ------------------------------------------------------------------------

        startPageLoading: function(options) {
            if (options && options.animate) {
                $('.page-spinner-bar').remove();
                $('body').append('<div class="page-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
            } else {
                $('.page-loading').remove();
                $('body').append('<div class="page-loading"><img src="' + this.getGlobalImgPath() + 'loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + (options && options.message ? options.message : '加载中...') + '</span></div>');
            }
        },

        // ------------------------------------------------------------------------

        stopPageLoading: function() {
            $('.page-loading, .page-spinner-bar').remove();
        },

        // ------------------------------------------------------------------------

        alert: function(options) {

            options = $.extend(true, {
                container: "", // 弹出框(默认情况下放置在页面的面包屑)
                place: "append", // "append" 或者 "prepend" 在 container
                type: 'success', // 弹出框的类型
                message: "", // 弹出框的内容
                close: true, // 弹出框的关闭按钮
                reset: true, // 关闭所有弹出框
                focus: true, // 自动滚动显示弹出框
                closeInSeconds: 0, // 在定义秒后自动关闭
                icon: "" //图标
            }, options);

            var id = App.getUniqueID("Metronic_alert");

            var html = '<div id="' + id + '" class="Metronic-alerts alert alert-' + options.type + ' fade in">' + (options.close ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>' : '') + (options.icon !== "" ? '<i class="fa-lg fa fa-' + options.icon + '"></i>  ' : '') + options.message + '</div>';

            if (options.reset) {
                $('.Metronic-alerts').remove();
            }

            if (!options.container) {
                if ($('body').hasClass("page-container-bg-solid")) {
                    $('.page-title').after(html);
                } else {
                    if ($('.page-bar').size() > 0) {
                        $('.page-bar').after(html);
                    } else {
                        $('.page-breadcrumb').after(html);
                    }
                }
            } else {
                if (options.place == "append") {
                    $(options.container).append(html);
                } else {
                    $(options.container).prepend(html);
                }
            }

            if (options.focus) {
                App.scrollTo($('#' + id));
            }

            if (options.closeInSeconds > 0) {
                setTimeout(function() {
                    $('#' + id).remove();
                }, options.closeInSeconds * 1000);
            }

            return id;
        },

        // ------------------------------------------------------------------------

        //初始化统一的元素
        initUniform: function(els) {
            if (els) {
                $(els).each(function() {
                    if ($(this).parents(".checker").size() === 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
            } else {
                handleUniform();
            }
        },

        // ------------------------------------------------------------------------

        //函数来更新/同步 jquery uniform checkbox & radios
        updateUniform: function(els) {
            $.uniform.update(els); // update the uniform checkbox & radios UI 控件状态改变时
        },

        // ------------------------------------------------------------------------

        //公共函数初始化fancybox插件
        initFancybox: function() {
            handleFancybox();
        },

        // ------------------------------------------------------------------------

        //公共helper函数得到实际输入值(用于IE9和IE8由于placeholder属性不支持)
        getActualVal: function(el) {
            el = $(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }
            return el.val();
        },

        // ------------------------------------------------------------------------

        //paremeter的name从URL
        getURLParameter: function(paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        // 检查设备联系支持
        isTouchDevice: function() {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        // ------------------------------------------------------------------------

        // 根据http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/得到正确的窗口宽度
        getViewPort: function() {
            var e = window,
                a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }

            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        },

        // ------------------------------------------------------------------------

        getUniqueID: function(prefix) {
            return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
        },

        // ------------------------------------------------------------------------

        // 检查 IE8 模式
        isIE8: function() {
            return isIE8;
        },

        // ------------------------------------------------------------------------

        // 检查 IE9 模式
        isIE9: function() {
            return isIE9;
        },

        // ------------------------------------------------------------------------

        //检查 RTL 模式
        isRTL: function() {
            return isRTL;
        },

        // ------------------------------------------------------------------------

        // 检查 IE8 模式
        isAngularJsApp: function() {
            return (typeof angular == 'undefined') ? false : true;
        },

        // ------------------------------------------------------------------------

        getAssetsPath: function() {
            return assetsPath;
        },

        // ------------------------------------------------------------------------

        setAssetsPath: function(path) {
            assetsPath = path;
        },

        // ------------------------------------------------------------------------

        setGlobalImgPath: function(path) {
            globalImgPath = path;
        },

        // ------------------------------------------------------------------------

        getGlobalImgPath: function() {
            return assetsPath + globalImgPath;
        },

        // ------------------------------------------------------------------------

        setGlobalPluginsPath: function(path) {
            globalPluginsPath = path;
        },

        // ------------------------------------------------------------------------

        getGlobalPluginsPath: function() {
            return assetsPath + globalPluginsPath;
        },

        // ------------------------------------------------------------------------

        getGlobalCssPath: function() {
            return assetsPath + globalCssPath;
        },

        // ------------------------------------------------------------------------

        // 布局 颜色名称颜色代码
        getBrandColor: function(name) {
            if (brandColors[name]) {
                return brandColors[name];
            } else {
                return '';
            }
        },

        // ------------------------------------------------------------------------

        getResponsiveBreakpoint: function(size) {
            // bootstrap responsive  响应点
            var sizes = {
                'xs' : 480,     // 很小
                'sm' : 768,     // 小
                'md' : 992,     // 大
                'lg' : 1200     // 很大
            };

            return sizes[size] ? sizes[size] : 0; 
        }

        // ------------------------------------------------------------------------

    };

}();